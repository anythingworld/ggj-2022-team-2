using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayer : MonoBehaviour
{
    public Vector3 startingPosition;
    // Start is called before the first frame update
    public void Awake()
    {
        startingPosition = transform.position;
    }

    public void Update()
    {
        if (transform.position.y < -50f)
        {
            transform.position = startingPosition;
        }
        if (transform.position.y > 50f)
        {
            transform.position = startingPosition;

        }
    }
}
