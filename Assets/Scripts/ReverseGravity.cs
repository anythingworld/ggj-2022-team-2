using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseGravity : MonoBehaviour
{
    public float scalar = 1;
    private float gravity = 10;
    public void FixedUpdate()
    {
        if (TryGetComponent<Animator>(out var anim))
        {
            if (!anim.GetBool("Grounded"))
            {
                //GetComponent<CharacterController>().Move(Vector3.up * gravity * scalar);
                transform.GetComponent<Rigidbody>().AddForce(Vector3.up * gravity * scalar);
            }
        }
        else
        {
            if(TryGetComponent<Rigidbody>(out var rb))
            {
                rb.AddForce(Vector3.up * gravity * scalar);
            }

        }
       
 
    }
}
