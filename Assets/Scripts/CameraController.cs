using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    public GameObject topPlayer;
    public GameObject bottomPlayer;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    private GameObject focusedPlayer;
    // Update is called once per frame
    public AudioClip switchSound;
    public void Start()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
        focusedPlayer = topPlayer;
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {

            Debug.Log("keyed");
            if (focusedPlayer == null)
            {
                focusedPlayer = topPlayer;
            }
            else if (focusedPlayer == topPlayer)
            {
                GetComponent<AudioSource>().PlayOneShot(switchSound, 0.7f);
                Debug.Log("switching to bottom");
                focusedPlayer = bottomPlayer;
            }
            else if (focusedPlayer == bottomPlayer)
            {
                GetComponent<AudioSource>().PlayOneShot(switchSound, 0.7f);
                Debug.Log("switching to top");
                focusedPlayer = topPlayer;
            }
            Debug.Log(focusedPlayer.name);
        }


        float x = Mathf.Clamp(focusedPlayer.transform.position.x, xMin, xMax);
        float y = Mathf.Clamp(focusedPlayer.transform.position.y, yMin, yMax);
        gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
    }
}
