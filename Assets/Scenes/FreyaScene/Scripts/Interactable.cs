using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    void Interact();
    void Interact(GameObject interactingObject);
    void PickUp();
    void PickUp(Transform newParent);
    void PutDown();
    void PutDown(Vector3 position);

    bool CanBePickedUp();
}
