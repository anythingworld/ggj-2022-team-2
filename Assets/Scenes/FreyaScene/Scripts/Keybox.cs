using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Keybox : MonoBehaviour, IInteractable
{
    public GemInteractable gem;
    public AudioClip unlocked;
    public bool CanBePickedUp()
    {
        return false;
    }
    public void Awake()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
    }
    public void Interact()
    {

    }

    public void Interact(GameObject interactingObject)
    {
        if (interactingObject.GetComponent<KeyInteractable>())
        {
            if (TryGetComponent<AudioSource>(out var audioSource))
            {
                audioSource.PlayOneShot(unlocked, 0.7f);
            }
            if (this.TryGetComponent<ActivationColourChanger>(out var colourChanger))
            {
                colourChanger.ActivateObject();
            }
            interactingObject.GetComponent<IInteractable>().PutDown();
            interactingObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            interactingObject.transform.parent = this.transform;
            interactingObject.transform.localRotation = new Quaternion(-0.675590158f, 0, -0.737277389f, 0);
            interactingObject.transform.localPosition = new Vector3(0f, -0.12f, 0.46f);

            gem.gameObject.SetActive(true);
        }
    }

    public void PickUp()
    {
    }

    public void PickUp(Transform newParent)
    {
    }

    public void PutDown()
    {
    }

    public void PutDown(Vector3 position)
    {
    }
}
