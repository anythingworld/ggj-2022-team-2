using System.Collections;
using UnityEngine;
public class PlayerInventoryManager : MonoBehaviour
{
    public Animator parentAnimator;
    public Transform rightHandTransform;
    public Transform levelBack;
    //closest object that could be picked up
    public GameObject nearbyObject = null;
    //object being held
    public GameObject holdingObject = null;
    // Start is called before the first frame update

    public void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("InteractableObject")) return;

        //Debug.Log($"nearbyObject is no longer {nearbyObject.name}");
        if (nearbyObject == other.gameObject) nearbyObject = null;


    }
    public void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("InteractableObject")) return;
        //Debug.Log("On trigger enter");
        nearbyObject = other.gameObject;
        //Debug.Log($"nearbyObject is {nearbyObject.name}");
    }

    public void Update()
    {
        if (holdingObject) holdingObject.transform.position = rightHandTransform.transform.position;

        if (Input.GetKeyDown(KeyCode.E))
        {
            Vector3 putDownPosition = Vector3.zero;
            if(TryGetComponent<BoxCollider>(out var col))
            {
                putDownPosition = new Vector3(col.bounds.max.x, col.bounds.center.y, col.bounds.center.z);
            }
            else
            {
               putDownPosition  = new Vector3(transform.position.x + transform.forward.x * 2, transform.position.y, transform.position.z);
            }


            if (holdingObject != null)
            {
                //if picking up new object drop holding and pickup new holding
                holdingObject.GetComponent<IInteractable>().PutDown(putDownPosition);
                holdingObject = null;
            }
            else if (nearbyObject != null && nearbyObject.GetComponent<IInteractable>().CanBePickedUp())
            {           //if nearby object is not null and can pick up, pick up 
                var interactable = nearbyObject.GetComponent<IInteractable>();
                StartCoroutine(PickupWithDelay(2, nearbyObject));
            }


            else
            {
                parentAnimator.SetTrigger("Wave");
            }

        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (nearbyObject != null)
            {
                if (holdingObject == null)
                {
                    nearbyObject.GetComponent<IInteractable>().Interact();
                }
                else
                {

                    nearbyObject.GetComponent<IInteractable>().Interact(holdingObject);
                    holdingObject = null;
                }
            }

        }
    }
    public IEnumerator PickupWithDelay(int seconds, GameObject nearby)
    {
        parentAnimator.SetTrigger("Pickup");
        yield return new WaitForSeconds(0.38f);
        nearby.gameObject.GetComponent<IInteractable>().PickUp(transform);
        holdingObject = nearby;

    }
}
