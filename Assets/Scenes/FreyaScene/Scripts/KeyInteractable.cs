using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyInteractable : MonoBehaviour, IInteractable
{

    public Vector3 holdingAngle = Vector3.zero;
    public Vector3 holdingScale = Vector3.one;
    private Vector3 originalScale = Vector3.one;
    public bool scaleInHand = false;
    private Vector3 originalAngle = Vector3.zero;

    public AudioClip pickup;
    static AudioClip drop;

    public void Awake()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
    }
    public void Interact()
    {
    }
    public void Interact(GameObject interactionTarget)
    {
       

    }
    public void PickUp()
    {

    }

    public void PickUp(Transform newParent)
    {
        if (TryGetComponent<AudioSource>(out var audioSource))
        {
            audioSource.PlayOneShot(pickup,0.7f);
        }
        if (TryGetComponent<Animator>(out var animator))
        {
            animator.enabled = false;
        }
        if(scaleInHand)
        {
            originalScale = transform.localScale;
            transform.localScale = holdingScale;
        }

        transform.parent = newParent;
        transform.position = Vector3.zero;
    }

    public void PutDown()
    {
        transform.parent = null;
    }

    public void PutDown(Vector3 position)
    {
        //Debug.Log("putting down object");
        transform.parent = null;
        if (scaleInHand)
        {
            transform.localScale = originalScale;
        }

        if (TryGetComponent<Animator>(out var animator))
        {
            animator.enabled = true;
        }
        transform.position = position;
    }

    public bool CanBePickedUp()
    {
        return true;
    }
}
