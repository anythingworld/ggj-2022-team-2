using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationColourChanger : MonoBehaviour
{
    public Material mat;
    public Color startColor = Color.grey;
    public Color endColor = Color.white;
    public Color hdrEndColor = Color.white;
    public float transitionSpeed = 1;
    public bool returnToStartingColor = false;
    public void Start()
    {
        mat.SetColor("_BaseColor", startColor);
        mat.SetColor("_EmissionColor", Color.black);
    }
    public void ActivateObject()
    {
        StartCoroutine(LerpColour());
    }
    public IEnumerator LerpColour()
    {
        var elapsedTime = 0f;
        var lerpedColor = startColor;
        while (lerpedColor != endColor)
        {
            
            lerpedColor = Color.Lerp(startColor, endColor, elapsedTime*transitionSpeed);
            elapsedTime += Time.deltaTime;
            mat.SetColor("_BaseColor", lerpedColor);
            mat.SetColor("_EmissionColor", lerpedColor);
        }
        if (returnToStartingColor)
        {
            elapsedTime = 0f;
            lerpedColor = endColor;
            if (lerpedColor != endColor)
            {
                lerpedColor = Color.Lerp(endColor, startColor, elapsedTime * transitionSpeed);
                elapsedTime += Time.deltaTime;
                mat.SetColor("_BaseColor", lerpedColor);
            }
        }

        yield return null;
    }
}
