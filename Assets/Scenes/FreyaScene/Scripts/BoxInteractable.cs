using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxInteractable : MonoBehaviour, IInteractable
{
    public Vector3 holdingScale = Vector3.one;
    private Vector3 originalScale = Vector3.one;
    public BoxCollider boxCollider;
    public Rigidbody rb;
    public AudioClip pickupSound;
    public void Awake()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
    }
    public bool CanBePickedUp()
    {
        return true;
    }

    public void Interact()
    {
    }

    public void Interact(GameObject interactingObject)
    {
    }

    public void PickUp()
    {
        boxCollider.enabled = false;
        if (rb != null) rb.isKinematic = true;
    }
    public void PickUp(Transform newParent)
    {
        if (TryGetComponent<AudioSource>(out var audioSource))
        {
            audioSource.PlayOneShot(pickupSound);
        }
        boxCollider.enabled = false;
        if (rb != null) rb.isKinematic = true;
        originalScale = transform.localScale;
        transform.localScale = holdingScale;
        transform.parent = newParent;
        transform.position = Vector3.zero;

    }

    public void PutDown()
    {
        boxCollider.enabled = true;
        if (rb != null) rb.isKinematic = false;

        transform.parent = null;
        transform.localScale = originalScale;
    }

    public void PutDown(Vector3 position)
    {

        boxCollider.enabled = true;
        if (rb != null) rb.isKinematic = false;
        //Debug.Log("putting down object");
        transform.parent = null;
        transform.localScale = originalScale;
        transform.position = position;

        boxCollider.enabled = true;
    }

}
