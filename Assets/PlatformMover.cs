using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour
{
    public Transform start;
    public Transform end;
    private bool goingToEnd = true;
    public float platformSpeed = 2f;
    // Start is called before the first frame update
    public void Start()
    {
        transform.position = start.transform.position;
        goingToEnd = true;
    }
    // Update is called once per frame
    public void FixedUpdate()
    {
        if (goingToEnd)
        {
            transform.position = Vector3.MoveTowards(transform.position, end.transform.position, Time.deltaTime*platformSpeed);
            if(Vector3.Distance(transform.position, end.position) < 0.5f)
            {
                goingToEnd = false;
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, start.transform.position, Time.deltaTime*platformSpeed);
            if (Vector3.Distance(transform.position, start.position) < 0.5f)
            {
                goingToEnd = true;
            }
        }
    }
}
