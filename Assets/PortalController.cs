using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalController : MonoBehaviour
{
    public Material activatedMat;
    public GameObject portalEntryPlane;
    public int nextIndex;
    private bool activated=false;

    public AudioClip portal;
    // Start is called before the first frame update
    public void ActivatePortal()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
        portalEntryPlane.GetComponent<MeshRenderer>().material = activatedMat;
        activated = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "light")
        {
            if (activated)
            {
                GetComponent<AudioSource>().PlayOneShot(portal);
                SceneManager.LoadScene(nextIndex, LoadSceneMode.Single);
            }

        }
    }

}
