﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AnythingWorld.Examples
{
    public class MicControl : MonoBehaviour
    {
        public Image Speaking;
        public GameObject InputCanvas;

        public ConversationController ConvoController;

        private bool _isActive = false;

        private ThinkingAnimation _thinkingAnimation;
        private SpeakingAnimation _speakingAnimation;

        private float _thinkStartTime;

        private Image _micImage;
        private Button _micButton;
        private Image _speakButtonImage;
        private Button _speakButton;


        void Start()
        {
            _micImage = GetComponent<Image>();
            _micButton = GetComponent<Button>();

            _thinkingAnimation = GameObject.FindObjectOfType<ThinkingAnimation>();
            _speakingAnimation = GameObject.FindObjectOfType<SpeakingAnimation>();

            _speakButtonImage = _speakingAnimation.gameObject.GetComponent<Image>();
            _speakButton = _speakingAnimation.gameObject.GetComponent<Button>();

            MakInitialInactive();
        }

        public void MakInitialInactive()
        {
            Speaking.gameObject.SetActive(false);
            InputCanvas.SetActive(false);
        }

        public void ToggleTextInput()
        {
            bool newState = !InputCanvas.activeSelf;
            bool oldState = InputCanvas.activeSelf;
            InputCanvas.SetActive(newState);
            _speakingAnimation.gameObject.SetActive(false);
            _thinkingAnimation.gameObject.SetActive(false);
            _micButton.enabled = _micImage.enabled = oldState;
        }

        public void MicPress()
        {
            UIBlocker.Instance.CanvasOneBlocked = true;

            SetMicActiveNoWait(false);

            PerformMicPress();
        }

        public void OnApplicationPause(bool isPaused)
        {
            if (isPaused)
            {
                SetMicActiveNoWait(true);
            }
        }

        public void ThinkingPress()
        {
            UIBlocker.Instance.CanvasOneBlocked = true;
            SpeakingPress();
            StartCoroutine(WaitToStopThinking());
        }

        private bool _speakingExit = false;

        public void SpeakingPress()
        {
            UIBlocker.Instance.CanvasOneBlocked = true;

            if (_speakingExit == true)
                return;

            _speakingExit = true;

            StartCoroutine(DelayedSpeakingPressResponse());
        }

        private IEnumerator DelayedSpeakingPressResponse()
        {
            yield return new WaitForSeconds(0.2f);
            ConvoController.StopRecording();
            yield return new WaitForSeconds(0.2f);
            SetMicActiveNoWait(true);
            _speakingExit = false;
        }

        private void PerformMicPress()
        {


            if (_isActive)
            {
                ConvoController.StartRecording();
            }
            else
            {
                ConvoController.StopRecording();
            }
        }

        public void MicToThink()
        {
            InputCanvas.SetActive(false);
            _thinkStartTime = Time.time;
            _micButton.enabled = _micImage.enabled = false;
            _isActive = false;
            _speakingAnimation.StopSpeaking();
            Speaking.gameObject.SetActive(false);

            _speakButton.enabled = _speakButtonImage.enabled = false;

            _thinkingAnimation.StartThinking();

        }

        private void SetMicActiveNoWait(bool isActive)
        {
            bool oppEffect = isActive ? false : true;
            Speaking.gameObject.SetActive(oppEffect);
            _micButton.enabled = _micImage.enabled = isActive;
            _isActive = oppEffect;

            if (_isActive)
            {
                _speakingAnimation.StartSpeaking();
                _speakButton.enabled = _speakButtonImage.enabled = true;
            }
        }

        public void SetMicActive(bool isActive)
        {

            if (isActive)
            {
                StartCoroutine(WaitToStopThinking());
            }
            else
            {
                Speaking.gameObject.SetActive(true);
                _speakingAnimation.StartSpeaking();
                _speakButton.enabled = _speakButtonImage.enabled = true;
                InputCanvas.SetActive(false);
                _micButton.enabled = _micImage.enabled = false;
                _isActive = isActive;
            }
        }

        private IEnumerator WaitToStopThinking()
        {
            float timeThinking = Time.time - _thinkStartTime;
            float timeRound = Mathf.Ceil(timeThinking);
            float waitTime = timeRound - timeThinking;

            yield return new WaitForSeconds(waitTime);

            _thinkingAnimation.StopThinking();
            _speakButton.enabled = _speakButtonImage.enabled = false;

            Speaking.gameObject.SetActive(false);

            _micButton.enabled = _micImage.enabled = true;
            _isActive = true;

        }
    }
}

