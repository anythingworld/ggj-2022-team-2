﻿using UnityEngine;
using AnythingWorld.Utilities;
public class MenuHandler : MonoBehaviour
{
    public GameObject panelObject = null;
    public FlyCamera cameraController = null;
    // Start is called before the first frame update
    public void Start()
    {
        panelObject.SetActive(false);
        if (cameraController == null)
        {
            cameraController = Camera.main.gameObject.GetComponent<FlyCamera>();
        }
    }

    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            panelObject.SetActive(!panelObject.activeSelf);
            cameraController.TakingInput = !panelObject.activeSelf;
        }
    }
}
