using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AnythingWorld
{
    /// <summary>
    /// Switches materials in the AWObj hierarchy with a new material using the specified new shader.
    /// </summary>
    public class SwitchShaderScript : MonoBehaviour
    {
        [Tooltip("New shader to replace current shader")]
        public Shader newShader;
        public void ApplyCustomShader()
        {
            AWObj awObj = GetComponent<AWObj>();

            List<MeshRenderer> renderers = (awObj.awThing.GetComponentsInChildren<MeshRenderer>()).ToList();
            foreach (var renderer in renderers)
            {
                Material[] newSharedMaterials;
                List<Material> tempMaterialsList = new List<Material>();
                foreach(var mat in renderer.sharedMaterials)
                {
                    Material newMat = new Material(mat)
                    {
                        name = $"{renderer.sharedMaterial.name}_{newShader.name}_newMat",
                        shader = newShader
                    };

                    tempMaterialsList.Add(newMat);

                }

                newSharedMaterials = tempMaterialsList.ToArray();
                renderer.sharedMaterials = newSharedMaterials;


            }

        }
    }
}

