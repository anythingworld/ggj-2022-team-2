﻿using AnythingWorld;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace MeshStitcher
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ModelStitcher : MonoBehaviour
    {
        #region Fields
        public Shader stitchedShader = null;
        public Material overrideMaterial = null;
        private AWObj targetAWObj;
        #endregion

        public UnityEvent onFinishStitching;


        #region Unity Callbacks
        public void Start()
        {
            if (transform.parent.TryGetComponent<AWObj>(out var awObj))
            {
                if (awObj.ObjMade)
                {
                    Initialize();
                }
                else
                {
                    awObj.onObjectMadeSuccessfullyDelegate += Initialize;
                }

            }
            else
            {
                Initialize();
            }
        }
        public void OnEnable()
        {
            if (stitchedShader == null && overrideMaterial == null)
            {
                //Debug.Log("Applying default shader");
                stitchedShader = Shader.Find("Anything World/Simple Lit Stitched");
            }
        }
        #endregion

        public void Initialize()
        {
            StitchAllJoints();
            SetAllUnstitched();
            onFinishStitching.Invoke();
        }

        private void StitchAllJoints()
        {
            var joints = GetComponentsInChildren<Joint>(true);
            //Adds mesh stitcher to main body game object and stitches it to itself, no change in position.
            GameObject bodyGO = gameObject.transform.GetChild(0).gameObject;
            // if no meshfilter on first child, search for body instead
            // TODO: standardise this, seems a little risky to me - GM
            if (bodyGO.GetComponentInChildren<MeshFilter>() == null)
            {
                bodyGO = gameObject.transform.Find("body").gameObject;
            }

            if (bodyGO != null && bodyGO.GetComponentInChildren<MeshFilter>() != null)
            {
                InitializeMeshStitcher(bodyGO, bodyGO.GetComponentInChildren<MeshFilter>(), bodyGO.GetComponentInChildren<MeshFilter>(), stitchedShader, overrideMaterial, false);
            }
            else
            {
                Debug.LogWarning("Initialization of mesh stitcher for body failed");
                return;
            }


            //Iterates through meshes with joints and adds a mesh stitcher to each.
            foreach (var joint in joints)
            {
                if (joint.gameObject.tag != "AWStitcherDisable")
                {
                    var connectedTo = joint.connectedBody;
                    if (connectedTo != null)
                    {
                        MeshFilter stitchToThisMeshFilter = connectedTo.GetComponentInChildren<MeshFilter>(true);
                        MeshFilter addMeshStitcherTo = joint.gameObject.GetComponentInChildren<MeshFilter>(true);

                        if (stitchToThisMeshFilter != null && addMeshStitcherTo != null)
                        {
                            InitializeMeshStitcher(addMeshStitcherTo.gameObject, addMeshStitcherTo, stitchToThisMeshFilter, stitchedShader, overrideMaterial);
                        }
                    }
                    else
                    {
                        Debug.LogWarning($"Joint {joint.gameObject.name} is not connected to anything, default trying to connect to body.");
                        if (bodyGO.TryGetComponent<Rigidbody>(out Rigidbody rigidOut))
                        {
                            try
                            {
                                joint.connectedBody = rigidOut;
                                connectedTo = rigidOut;

                                MeshFilter stitchToThisMeshFilter = connectedTo.GetComponentInChildren<MeshFilter>(true);
                                MeshFilter addMeshStitcherTo = joint.gameObject.GetComponentInChildren<MeshFilter>(true);

                                if (stitchToThisMeshFilter != null && addMeshStitcherTo != null)
                                {
                                    InitializeMeshStitcher(addMeshStitcherTo.gameObject, addMeshStitcherTo, stitchToThisMeshFilter, stitchedShader, overrideMaterial);
                                }
                                else
                                {
                                    Debug.LogWarning("Missing mesh filter in child components");
                                }
                            }
                            catch
                            {
                                Debug.LogError($"Failed to stitch {joint.gameObject.name} to body");
                            }
                        }
                    }
                }
                else
                {
                    //if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Stitcher should not be applied to this joint.");
                    try
                    {
                        MeshRenderer meshRenderer = joint.gameObject.GetComponentInChildren<MeshRenderer>(true);

                        Material[] rendererMaterials = meshRenderer.sharedMaterials;
                        Shader simpleLit = Shader.Find("Anything World/Simple Lit");
                        if (simpleLit != null)
                        {
                            List<Material> materialOverrideList = new List<Material>();
                            foreach (var item in rendererMaterials.Select((value, i) => new { i, value }))
                            {
                                var mat = item.value;
                                var index = item.i;

                                Material newMat = new Material(simpleLit);
                                newMat.CopyPropertiesFromMaterial(mat);
                                newMat.name = $"{mat} - SimpleLit Override";
                                materialOverrideList.Add(newMat);
                            }

                            meshRenderer.materials = materialOverrideList.ToArray();


                        }
                        else
                        {
                            if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("No simpleLit shader found");
                        }

                    }
                    catch
                    {
                        if (AnythingSettings.Instance.showDebugMessages) Debug.LogError("Error switching meshes to Simple Lit shader");
                    }

                }
            }
        }
        private void SetAllUnstitched()
        {
            var unstitchedPartTags = GetComponentsInChildren<UnstitchedPartTag>();
            foreach (var partTag in unstitchedPartTags)
            {
                if (partTag.GetComponentInChildren<MeshRenderer>())
                {
                    foreach (var component in partTag.GetComponentsInChildren<MeshRenderer>(true))
                    {
                        MeshRenderer unstitchedMeshRenderer = component;

                        //Debug.Log($"Found unstitched MeshRenderer on {partTag.gameObject.name}");

                        try
                        {

                            Material[] rendererMaterials = unstitchedMeshRenderer.sharedMaterials;
                            Shader simpleLit = Shader.Find("Anything World/Simple Lit");
                            if (simpleLit != null)
                            {
                                List<Material> materialOverrideList = new List<Material>();
                                foreach (var item in rendererMaterials.Select((value, i) => new { i, value }))
                                {
                                    var mat = item.value;
                                    var index = item.i;

                                    Material newMat = new Material(simpleLit);
                                    newMat.CopyPropertiesFromMaterial(mat);
                                    newMat.name = $"{mat.name} - SimpleLit Override";
                                    materialOverrideList.Add(newMat);
                                    //Debug.Log($"Set {mat.name} to {newMat.name}");
                                }

                                unstitchedMeshRenderer.materials = materialOverrideList.ToArray();


                            }
                            else
                            {
                                if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("No simpleLit shader found");
                            }

                        }
                        catch
                        {
                            if (AnythingSettings.Instance.showDebugMessages) Debug.LogError("Error switching meshes to Simple Lit shader");
                        }
                    }





                }
            }
        }

        private static void InitializeMeshStitcher(GameObject mesh, MeshFilter source, MeshFilter target, Shader overrideShader = null, Material overrideMaterial = null, bool stitchComponent = true)
        {


            MeshStitcher newMeshStitcher = mesh.GetComponent<MeshStitcher>();
            if (newMeshStitcher == null)
            {
                newMeshStitcher = mesh.AddComponent<MeshStitcher>();
            }



            if (overrideShader)
            {
                newMeshStitcher.overrideShader = overrideShader;
            }
            if (overrideMaterial)
            {
                newMeshStitcher.overrideMaterial = overrideMaterial;
            }

            newMeshStitcher.Initialize(source, target, stitchComponent);
        }

    }
}
