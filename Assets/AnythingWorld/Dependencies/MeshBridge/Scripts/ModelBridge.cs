﻿using System.Collections.Generic;
using UnityEngine;

using AnythingWorld.Utilities;


public class ModelBridge : MonoBehaviour
{
    [SerializeField]
    public List<MeshBridge> meshBridges;
    public void OnEnable()
    {
    }

    public void AddMeshBridgeUnits()
    {
        meshBridges = new List<MeshBridge>();
        ConfigurableJoint[] joints = GetComponentsInChildren<ConfigurableJoint>();
        for (int i = 0; i < joints.Length; i++)
        {
            if (!joints[i].gameObject.GetComponent<MeshBridge>())
            {
                meshBridges.Add(joints[i].gameObject.AddComponent<MeshBridge>());
            }
        }
    }

    public void RemoveMeshBridgeUnits()
    {
        if (meshBridges == null) return;
        foreach (MeshBridge meshBridge in meshBridges)
        {
            AnythingSafeDestroy.SafeDestroy(meshBridge);
            AnythingSafeDestroy.ClearList<MeshBridge>(meshBridges);
        }
    }
}
