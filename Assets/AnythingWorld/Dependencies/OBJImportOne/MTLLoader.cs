﻿/*
 * Copyright (c) 2019 Dummiesman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
*/

using Dummiesman;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using AnythingWorld;
public class MTLLoader
{
    public List<string> SearchPaths = new List<string>() { "%FileName%_Textures", string.Empty };

    private FileInfo _objFileInfo = null;

    /// <summary>
    /// The texture loading function. Overridable for stream loading purposes.
    /// </summary>
    /// <param name="path">The path supplied by the OBJ file, converted to OS path seperation</param>
    /// <param name="isNormalMap">Whether the loader is requesting we convert this into a normal map</param>
    /// <returns>Texture2D if found, or NULL if missing</returns>
    public virtual Texture2D TextureLoadFunction(string path, bool isNormalMap)
    {
        //find it
        foreach (var searchPath in SearchPaths)
        {
            //replace varaibles and combine path
            string processedPath = (_objFileInfo != null) ? searchPath.Replace("%FileName%", Path.GetFileNameWithoutExtension(_objFileInfo.Name))
                                                          : searchPath;
            string filePath = Path.Combine(processedPath, path);

            //return if eists
            if (File.Exists(filePath))
            {
                var tex = ImageLoader.LoadTexture(filePath);

                if (isNormalMap)
                    tex = ImageUtils.ConvertToNormalMap(tex);

                return tex;
            }
        }

        //not found
        return null;
    }

    private int GetArgValueCount(string arg)
    {
        switch (arg)
        {
            case "-bm":
            case "-clamp":
            case "-blendu":
            case "-blendv":
            case "-imfchan":
            case "-texres":
                return 1;
            case "-mm":
                return 2;
            case "-o":
            case "-s":
            case "-t":
                return 3;
        }
        return -1;
    }

    private int GetTexNameIndex(string[] components)
    {
        for (int i = 1; i < components.Length; i++)
        {
            var cmpSkip = GetArgValueCount(components[i]);
            if (cmpSkip < 0)
            {
                return i;
            }
            i += cmpSkip;
        }
        return -1;
    }

    private float GetArgValue(string[] components, string arg, float fallback = 1f)
    {
        string argLower = arg.ToLower();
        for (int i = 1; i < components.Length - 1; i++)
        {
            var cmp = components[i].ToLower();
            if (argLower == cmp)
            {
                return OBJLoaderHelper.FastFloatParse(components[i + 1]);
            }
        }
        return fallback;
    }

    private string GetTexPathFromMapStatement(string processedLine, string[] splitLine)
    {
        int texNameCmpIdx = GetTexNameIndex(splitLine);
        if (texNameCmpIdx < 0)
        {
            Debug.LogError($"texNameCmpIdx < 0 on line {processedLine}. Texture not loaded.");
            return null;
        }

        int texNameIdx = processedLine.IndexOf(splitLine[texNameCmpIdx]);
        string texturePath = processedLine.Substring(texNameIdx);

        return texturePath;
    }

    public Dictionary<string, Material> Load(Stream input, Dictionary<string, Texture> textures)
    {

        //Debug.Log("MTLLoader :: Load :: " + input.Length);

        //reader = new StreamReader(input);
        StreamReader reader = new StreamReader(input);
        //var reader = new StringReader(inputReader.ReadToEnd());

        Dictionary<string, Material> mtlDict = new Dictionary<string, Material>();
        Material currentMaterial = null;

        string line;

        string mtl_file = "MATERIAL FILE" + "\n";
        string mtl_log = "MATERIAL LOADING LOG" + "\n";
        while ((line = reader.ReadLine()) != null)
        {
            mtl_file = mtl_file + line + "\n";
            if (string.IsNullOrWhiteSpace(line))
                continue;

            string processedLine = line.Clean();
            string[] splitLine = processedLine.Split(' ');

            //blank or comment
            if (splitLine.Length < 2 || processedLine[0] == '#')
                continue;


            //newmtl
            if (splitLine[0] == "newmtl")
            {
                string materialName = processedLine.Substring(7);

                var newMtl = new Material(Shader.Find("Anything World/Simple Lit")) { name = materialName };

                mtlDict[materialName] = newMtl;
                currentMaterial = newMtl;
                continue;
            }

            //anything past here requires a material instance
            if (currentMaterial == null)
                continue;

            //diffuse color
            if (splitLine[0] == "Kd" || splitLine[0] == "kd")
            {
                var currentColor = currentMaterial.GetColor("_BaseColor");
                var kdColor = OBJLoaderHelper.ColorFromStrArray(splitLine);
                mtl_log = mtl_log + "Base color (Kd): " + $"{kdColor.r} {kdColor.g} {kdColor.b} {currentColor.a}" + "\n";

                currentMaterial.SetColor("_BaseColor", new Color(kdColor.r, kdColor.g, kdColor.b, currentColor.a));
                //Debug.Log($"{kdColor.r} {kdColor.g} {kdColor.b}");
                continue;
            }

            // diffuse map
            if (splitLine[0] == "map_Kd" || splitLine[0] == "map_kd" || splitLine[0] == "map-Kd" || splitLine[0] == "map-kd")
            {
                string texturePath = GetTexPathFromMapStatement(processedLine, splitLine);
                //Debug.Log("MTLLOADER tex path:" + texturePath);
                if (texturePath == null)
                {
                    Debug.Log("map_Kd specified but no texturePath found");
                    continue;
                }
                else
                {

                }


                if (textures.ContainsKey(texturePath))
                {
                    Texture loadedTex;
                    textures.TryGetValue(texturePath, out loadedTex);


                    if (loadedTex != null)
                    {
                        Texture2D KdTexture = loadedTex as Texture2D;
                        //KdTexture.alphaIsTransparency = true;
                        if (KdTexture != null)
                        {
                            currentMaterial.SetTexture("_BaseMap", KdTexture);
                            mtl_log = mtl_log + "_BaseMap (map_Kd): " + texturePath + "\n";
                        }
                        else
                        {
                            Debug.Log("Error loading texture" + texturePath + " to texture 2D");
                            continue;
                        }

                        //WORK AROUND: set kd to white if texture present and is pitch black
                        // if things are turning white instead of black this is the culprit. apologies.
         
                        
                        if (currentMaterial.GetColor("_BaseColor") == Color.black)
                        {
                            if(AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Warning: Base color is black, overriding to white.");
                            currentMaterial.SetColor("_BaseColor", Color.white);
                            //if (AnythingSettings.Instance.showDebugMessages) Debug.LogError("Setting basecolor to white!");
                            //

                            // mtl_log = mtl_log + "Switched base color from black to white" + "\n";
                        }

                        
                        /*
                        if (KdTexture != null && (KdTexture.format == TextureFormat.DXT5 || KdTexture.format == TextureFormat.ARGB32))
                        {
                            OBJLoaderHelper.EnableMaterialTransparency(currentMaterial);
                            mtl_log = mtl_log + "Enabled transparency on base map"+ "\n";
                        }
                        else
                        {


                        }
                        */
                    }
                    else
                    {
                        Debug.Log("Texture key \"" + texturePath + "\" found in dictionary but texture was not loaded.");
                        mtl_log = mtl_log + "Error: Base map " + texturePath + " found, error loading" + "\n";
                    }

                }
                else
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Texture \"" + texturePath + "\" not found in dictionary");

                    mtl_log = mtl_log + "Error: Base map " + texturePath + " not found, not loaded." + "\n";

                    continue;
                }

                /*
                if (Path.GetExtension(texturePath).ToLower() == ".dds")
                {
                    currentMaterial.mainTextureScale = new Vector2(1f, -1f);
                }
                */
                continue;
            }

            // bump map
            if (splitLine[0] == "map_Bump" || splitLine[0] == "map_bump")
            {
                string texturePath = GetTexPathFromMapStatement(processedLine, splitLine);
                if (texturePath == null)
                {
                    continue; //invalid args or sth
                }

                if (textures.ContainsKey(texturePath))
                {
                    textures.TryGetValue(texturePath, out Texture loadedTex);
                    if (loadedTex != null)
                    {
                        Texture2D bumpTex = loadedTex as Texture2D;
                        if (bumpTex != null)
                        {
                            currentMaterial.SetTexture("_BumpMap", bumpTex);

                            float bumpScale = GetArgValue(splitLine, "-bm", 1.0f);
                            currentMaterial.SetFloat("_BumpScale", bumpScale);
                            currentMaterial.EnableKeyword("_NORMALMAP");


                            mtl_log = mtl_log + "Bump map (map_Bump): " + texturePath + "\n";
                        }
                        else
                        {
                            Debug.Log("Error loading texture" + texturePath + " to texture 2D");
                            continue;
                        }

                        if (bumpTex != null && (bumpTex.format == TextureFormat.DXT5 || bumpTex.format == TextureFormat.ARGB32))
                        {
                            OBJLoaderHelper.EnableMaterialTransparency(currentMaterial);
                        }

                    }
                    else
                    {
                        Debug.Log("Texture key \"" + texturePath + "\" found in dictionary but texture was not loaded.");
                        mtl_log = mtl_log + "Error: Bump map " + texturePath + " not loaded." + "\n";
                    }
                }
                else
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Bump map texture \"" + texturePath + "\" not found in dictionary");
                    mtl_log = mtl_log + "Error: Bump map " + texturePath + " not loaded." + "\n";
                    continue;
                }
                continue;
            }

            //specular color
            if (splitLine[0] == "Ks" || splitLine[0] == "ks")
            {
                Color specularColor = OBJLoaderHelper.ColorFromStrArray(splitLine);

                if (specularColor != null)
                {
                    currentMaterial.SetColor("_SpecColor", specularColor);

                    mtl_log = mtl_log + "Specular color (Ka): " + $"{specularColor.r} {specularColor.g} {specularColor.b} {specularColor.a}" + "\n";
                }
                
                continue;
            }

            //emission color
            if (splitLine[0] == "Ka" || splitLine[0] == "ka")
            {
                Color emissionColor = OBJLoaderHelper.ColorFromStrArray(splitLine, 0.05f);

                if (emissionColor != null)
                {
                    currentMaterial.EnableKeyword("_EmissionColor");
                    currentMaterial.SetColor("_EmissionColor", emissionColor);
                    mtl_log = mtl_log + "Emission color (Ka): " + $"{emissionColor.r} {emissionColor.g} {emissionColor.b} {emissionColor.a}" + "\n";
                }

                continue;
            }

            //ambient color map/emission map
            if (splitLine[0] == "map_Ka" || splitLine[0] == "map_ka")
            {
                string texturePath = GetTexPathFromMapStatement(processedLine, splitLine);
                if (texturePath == null)
                {
                    continue; //invalid args or sth
                }
                else
                {

                }

                if (textures.ContainsKey(texturePath))
                {
                    if(AnythingSettings.Instance.showDebugMessages) Debug.Log("Looking for texture \"" + texturePath + "\" in dictionary");
                    textures.TryGetValue(texturePath, out Texture loadedTex);

                    if (loadedTex != null)
                    {
                        Texture2D emissionTex = loadedTex as Texture2D;
                        if (emissionTex != null)
                        {
                            currentMaterial.SetTexture("_EmissionMap", emissionTex);
                            mtl_log = mtl_log + "Ambient Color/Emission Map (map_Ka): " + texturePath + "\n";

                        }
                        else
                        {
                            if(AnythingSettings.Instance.showDebugMessages) Debug.Log("Error loading texture" + texturePath + " to texture 2D");
                            mtl_log = mtl_log + "Error:  Emission map " + texturePath + " not loaded." + "\n";
                            continue;
                        }

                        if (emissionTex != null && (emissionTex.format == TextureFormat.DXT5 || emissionTex.format == TextureFormat.ARGB32))
                        {
                            OBJLoaderHelper.EnableMaterialTransparency(currentMaterial);
                        }

                    }
                    else
                    {
                        Debug.Log("Texture key \"" + texturePath + "\" found in dictionary but texture was not loaded.");
                        mtl_log = mtl_log + "Error:  Emission map " + texturePath + " not loaded." + "\n";
                    }
                }
                else
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Texture \"" + texturePath + "\" not found in dictionary");
                    mtl_log = mtl_log + "Error:  Emission map " + texturePath + " not loaded." + "\n";
                    continue;
                }
            }

            // alpha
            if (splitLine[0] == "d" || splitLine[0] == "D" || splitLine[0] == "Tr" || splitLine[0] == "tr")
            {
                //Debug.Log("transparent material found");

                
                float visibility = OBJLoaderHelper.FastFloatParse(splitLine[1]);

                //tr statement is just d inverted
                if (splitLine[0] == "Tr" || splitLine[0] == "tr")
                {
                    visibility = 1f - visibility;
                }


                //F_REMOVE
                if (visibility == 0)
                {
                    if(AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("VISIBILITY IN " + currentMaterial.name + ": " + visibility);
                }

                if (visibility < (1f - Mathf.Epsilon))
                {
                    if (AnythingWorld.Utilities.MaterialCacheUtil.
    MaterialCacheDict.TryGetValue("SIMPLE_LIT_TRANSPARENT", out var template))
                    {
                        var newMat = new Material(template);
                        newMat.name = currentMaterial.name;
                        mtlDict[currentMaterial.name] = newMat;
                        currentMaterial = newMat;

                    }


                    var currentColor = currentMaterial.GetColor("_BaseColor");
                    currentColor.a = visibility;
                    currentMaterial.SetColor("_BaseColor", currentColor);
                    mtl_log = mtl_log + "Visibility (tr/d): " + visibility.ToString() + "\n";
                    mtl_log = mtl_log + "Material tranparency: enabled" + "\n";
                }
                else
                {
                    mtl_log = mtl_log + "Visibility (tr/d): " + visibility.ToString() + "\n";
                    mtl_log = mtl_log + "Material tranparency: disabled" + "\n";
                }
                continue;
            }

            //glossiness
            if (splitLine[0] == "Ns" || splitLine[0] == "ns")
            {
                float glossiness = OBJLoaderHelper.FastFloatParse(splitLine[1]);
                glossiness = (glossiness / 1000f);
                currentMaterial.SetFloat("_Glossiness", glossiness);
                mtl_log = mtl_log + "Glossiness (ns)" + glossiness.ToString() + "\n";
            }
        }
        reader.Close();



        //F_REMOVE
        if(AnythingSettings.Instance.showDebugMessages) Debug.Log(mtl_log);
        mtl_log = "";

        if(AnythingSettings.Instance.showDebugMessages) Debug.Log(mtl_file);
        mtl_file = "";


        return mtlDict;
    }


    /*
    /// <summary>
    /// Loads a *.mtl file
    /// </summary>
    /// <param name="path">The path to the MTL file</param>
    /// <returns>Dictionary containing loaded materials</returns>ƒ
	public Dictionary<string, Material> Load(string path)
    {
        _objFileInfo = new FileInfo(path); //get file info
        SearchPaths.Add(_objFileInfo.Directory.FullName); //add root path to search dir

        using (var fs = new FileStream(path, FileMode.Open))
        {
            return Load(fs); //actually load
        }

    }
    */
}
