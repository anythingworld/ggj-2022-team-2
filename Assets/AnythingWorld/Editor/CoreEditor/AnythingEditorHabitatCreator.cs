﻿using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using AnythingWorld.Habitat;
using System;

namespace AnythingWorld.Editors
{
    /// <summary>
    /// Custom editor for the AWHabitatCreator.
    /// </summary>
    [System.Serializable]
    public class AnythingEditorHabitatCreator : AnythingEditor
    {
        #region Fields
        private SortedDictionary<string, HabitatDescription> _habitats;
        private string[] habitatNames;
        private int habitatIndex;
        private string habitatKey;
        private string habitatDescription;
        private string _habitatName;
        //private bool makeObjects = true;
        private AWHabitat AWHabitat;
        private Dictionary<string, List<GameObject>> madeThings;
        private Vector2 scrollPos;
        private GameObject awSetupInstance;
        public enum Orientation { Left, Right, Top, Bottom }
        private string[] defaultOption = new string[] { "Select Habitat" };
        #endregion

        #region Instancing
        [SerializeField]
        private static AnythingEditorHabitatCreator instance;
        public static AnythingEditorHabitatCreator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = ScriptableObject.CreateInstance<AnythingEditorHabitatCreator>();
                }
                return instance;
            }
        }
        #endregion

        #region Unity Callbacks
        private new void Awake()
        {
            Setup();
            HabitatSetup();
        }

        public string[] GetHabitatOptions()
        {
            if (habitatNames != null)
            {
                return habitatNames;
            }
            else
            {
                HabitatSetup();
            }
            return null;
        }
        public string[] GetHabitatWithDefault()
        {
            if (habitatNames != null)
            {
                string[] habitatList = GetHabitatOptions();
                string[] options = new string[defaultOption.Length + habitatList.Length];
                defaultOption.CopyTo(options, 0);
                habitatList.CopyTo(options, defaultOption.Length);

                return options;
            }
            else
            {
                HabitatSetup();
            }
            return null;
        }
        public void CreateHabitat(int index, bool populateHabitat)
        {
            try
            {
                if (newHabitatCoroutine != null)
                {
                    EditorCoroutineUtility.StopCoroutine(newHabitatCoroutine);
                }
                newHabitatCoroutine = EditorCoroutineUtility.StartCoroutine(MakeHabitat(index, populateHabitat), this);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        #endregion

        #region Private Methods
        private void HabitatSetup()
        {
            OBJECT_PADDING = 4;
            madeThings = new Dictionary<string, List<GameObject>>();
            _habitats = HabitatMap.GetHabitats();
            habitatNames = new string[_habitats.Count];
            int nameInd = 0;
            foreach (KeyValuePair<string, HabitatDescription> hKvp in _habitats)
            {
                habitatNames[nameInd] = hKvp.Value.Name;
                nameInd++;
            }

            habitatIndex = 0;
            ChangeDescriptions();
        }
        private void ChangeDescriptions()
        {
            if (_habitats == null)
                HabitatSetup();
            habitatKey = _habitats.ElementAt(habitatIndex).Key;
            habitatDescription = _habitats[habitatKey].Description;
            _habitatName = _habitats[habitatKey].Name;
        }
        private IEnumerator MakeHabitat(int index, bool makeObjects)
        {
            CheckAWSetup();
            ResetEverything();

            habitatKey = _habitats.ElementAt(habitatIndex).Key;
            habitatIndex = index;
            ChangeDescriptions();

            if (habitatKey != "testing" && makeObjects == true)
            {
                AnythingSetup.Instance.ShowLoading(true);
            }



            GameObject habitatCreator = new GameObject();
            habitatCreator.name = "Habitat Creator";
            AWHabitat = habitatCreator.AddComponent<AWHabitat>();


            AWHabitat.MakeHabitat(habitatKey);


            bool haveError = false;
            while (!AWHabitat.AWHabitatReady)
            {
                yield return new EditorWaitForSeconds(0.01f);
                if (AWHabitat.AWKeyInvalid)
                {
                    EditorUtility.DisplayDialog("API Key Invalid!", "We couldn't validate the key: " + AnythingSettings.Instance.apiKey + " \nPlease double check the API Key entry in AnythingWorld/Settings/AnythingSettings", "Ok");
                    haveError = true;
                }
                else if (AWHabitat.AWAppIdInvalid)
                {
                    EditorUtility.DisplayDialog("App Name Is Empty!", "We couldn't see an App Name! \nPlease double check the App Name entry in AnythingWorld/Settings/AnythingSettings", "Ok");
                    haveError = true;
                }
                if (haveError)
                {
                    AnythingSetup.Instance.ShowLoading(false);
                    SetUpFresh();
                    yield break;
                }


            }

            if (habitatKey != "testing" && makeObjects == true)
            {
                EditorCoroutineUtility.StartCoroutine(MakeHabitatObjects(AWHabitat.RandomObjects), this);
            }

        }
        private IEnumerator MakeHabitatObjects(string[] habitatObjects)
        {
            foreach (string objString in habitatObjects)
            {
                Creator.MakeObject(objString, true);
                yield return null;
            }
        }

        private void CheckAWSetup()
        {
            AnythingBase.CheckAWSetup();
        }
        #endregion
    }
}
