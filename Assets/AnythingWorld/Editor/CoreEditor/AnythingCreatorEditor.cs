﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
#endif
#if UNITY_EDITOR_WIN
#elif UNITY_EDITOR_OSX
using System.Runtime.InteropServices;
#endif

using AnythingWorld.DataContainers;
namespace AnythingWorld.Editors
{
    [ExecuteAlways]
    /// <summary>
    /// Custom editor for the AnythingCreator.
    /// </summary>
    public class AnythingCreatorEditor : AnythingEditor
    {
        private GameObject awSetupInstance;
        private string searchTerm;
        private int searchResultsCount = 0;
        private int navMenuSelected = 0;
        private float panelWidth;
        private float panelHeight;
        List<SearchResult> searchResults = null;
        List<SearchResult> animatedResults = null;
        private int habitatIndex;
        private int micIndex = 0;
        private string voiceInputString = "...";
        [SerializeField]
        private bool playModeEntered = false;
        [NonSerialized]
        bool pMReset = false;
        public bool autoGenerateFromVoice = true;
        private bool zoomedPanelActive = false;
        private bool populateHabitat = false;
        private string searchTermFieldString = "";
        private bool searchReturned = false;
        private bool searchStarted = false;
        private bool searchLoading = false;
        private bool errorWhileSearching = false;
        private bool showAnimatedOnly = false;
        private bool resetSearchField = false;
        private Rect windowPosition;

        private static AnythingSettings Settings
        {
            get
            {
                if (AnythingSettings.Instance == null)
                {
                    return null;
                }
                return AnythingSettings.Instance;
            }
        }


        private new void Awake()
        {
            if (searchResults == null) searchResults = new List<SearchResult>();
            Setup();
        }

        public void OnEnable()
        {
            StopSearch();
        }
        public static void ShowAPIWindow()
        {
            AnythingSignupLoginEditor.OpenFromScript();
        }
        /// <summary>
        /// Checks is AnythingSettings has API key present.
        /// </summary>
        /// <returns>Returns false if API key not present, true if present.</returns>
        private bool CheckAPI()
        {
            if (AnythingSettings.Instance.apiKey == "")
            {
                if (AnythingSettings.Instance == null)
                {
                    AnythingSettings.CreateInstance<AnythingSettings>();
                }

                return false;
            }
            else
            {
                return true;
            }
        }
        public void OnInspectorUpdate()
        {
            Repaint();
        }

        public void OnGUI()
        {
            #region Initialize GUI
            Voice.SubscribeOutputText(UpdateRecognisedString);
            Voice.SubscribeToListeningStatus(ToggleMicTex);
            Voice.SubscribeToListeningEnded(AutoCreateOnFinish);
            GetWindowSize();
            InitializeResources();
            #endregion

            #region Top Bar
            DrawWindowBanner(anythingWorldBanner);
            DrawNavToolbar();

            #endregion

            #region Window Content

            switch (navMenuSelected)
            {
                case 0:
                    CreatorPanel();
                    break;
                case 1:
                    VoicePanel();
                    break;
                case 2:
                    HabitatPanel();
                    break;
                default:
                    CreatorPanel();
                    break;
            }
            #endregion

            Repaint();
            UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
            UnityEditor.SceneView.RepaintAll();
        }

        #region Private Methods

        private void CreatorPanel()
        {
            if (!InitializeResources())
            {
                return;
            }
            if (zoomedPanelActive) GUI.enabled = false;
            EditorGUILayout.BeginVertical();
            //-------------------------------------//
            DrawBoldTextHeader("Create 3D Models");
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            showAnimatedOnly = GUILayout.Toggle(showAnimatedOnly, "ANIMATED ONLY", toggleStyle);
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Reset Grid", searchButtonStyle))
            {
                if (EditorUtility.DisplayDialog("Reset Grid", "Are you sure you want to reset grid?", "Reset", "Cancel"))
                {
                    AnythingCreator.Instance.LayoutGrid = new Utilities.GridUtility(10, 10);
                }

            }

            if (GUILayout.Button("Reset All", searchButtonStyle))
            {
                if (EditorUtility.DisplayDialog("Reset Creations", "Are you sure you want reset scene and remove all creations?", "Reset", "Cancel"))
                {
                    ResetEverything();
                }
            }
            GUILayout.EndHorizontal();

            //-------------------------------------//
            EditorGUILayout.BeginHorizontal();
            DrawSearchBar(!zoomedPanelActive);
            EditorGUILayout.EndHorizontal();
            //-------------------------------------//
            DrawResultsGrid();
            //-------------------------------------//
            EditorGUILayout.EndVertical();
        }
        private void VoicePanel()
        {
            EditorGUILayout.BeginVertical();
            //-------------------------------------//
            //DrawWindowBanner(AWBanner);
            DrawBoldTextHeader("Create Models With Voice");
            //GUILayout.FlexibleSpace();
            //-------------------------------------//
            EditorGUILayout.BeginHorizontal();
            bool micActive = DrawMicButton();
            EditorGUILayout.EndHorizontal();
            //-------------------------------------//
            EditorGUILayout.BeginHorizontal();
            DrawSoundWave(micActive);
            EditorGUILayout.EndHorizontal();
            //-------------------------------------//
            //GUILayout.FlexibleSpace();
            DrawSpeechPreview(voiceInputString);
            //GUILayout.FlexibleSpace();
            MicSelectionDropDown();
            //GUILayout.FlexibleSpace();
            //-------------------------------------//
            EditorGUILayout.EndVertical();
        }
        private void HabitatPanel()
        {
            EditorGUILayout.BeginVertical();
            DrawBoldTextHeader("Create Habitat");
            HabitatDropDownMenu();
            //HabitatPreview();
            EditorGUILayout.EndVertical();
        }

        #region Initialization
        /// <summary>
        /// Called when user opens up window from the menu bar.
        /// </summary>
        [MenuItem("Anything World/Anything Creator", false, 1)]
        private static void Init()
        {
            UnityEditor.EditorWindow window = GetWindow(typeof(AnythingCreatorEditor), false, "Anything Creator");
            window.position = new Rect(10, 10, 500, 500);
            window.Show();
        }

        /// <summary>
        /// Checks if instance of AWSetup exists in scene, creates if not found. Assigns to variable if found.
        /// </summary>
        private void CheckAWSetup()
        {
            if (!GameObject.FindGameObjectWithTag("AWSetup"))
            {
                Instantiate(AnythingSettings.Instance.anythingSetup);
            }
            awSetupInstance = GameObject.FindGameObjectWithTag("AWSetup");
        }

        private void CheckForPlayChange()
        {
            if (searchResults == null) searchResults = new List<SearchResult>();
            if (Application.isPlaying && pMReset == false)
            {
                zoomedPanelActive = false;
                searchTerm = "";
                searchResults.Clear();
                pMReset = true;
                playModeEntered = true;

            }
            if (!Application.isPlaying && playModeEntered == true)
            {
                playModeEntered = false;
                zoomedPanelActive = false;
                searchTerm = "";
                searchResults.Clear();
            }
        }

        public void GetWindowSize()
        {
            GetWindow<AnythingCreatorEditor>("position", false);
            windowPosition = position;
            panelWidth = position.width;
            panelHeight = position.height;
        }

        public void UpdateSearchResults(SearchResult[] thumbnailResults, AWThing[] jsonResults, bool failed)
        {
            errorWhileSearching = false;
            searchLoading = false;
            searchStarted = true;
            searchResults = new List<SearchResult>();
            animatedResults = new List<SearchResult>();

            if (thumbnailResults == null)
            {
                errorWhileSearching = true;
                searchReturned = false;
                return;
            }

            if (thumbnailResults.Length > 0)
            {
                searchReturned = true;
                searchResults = thumbnailResults.ToList<SearchResult>();
            }
            else
            {
                searchReturned = false;
            }
            foreach (SearchResult res in searchResults)
            {
                if (res.isAnimated)
                {
                    animatedResults.Add(res);
                }
            }
        }
        public void UpdateRecognisedString(string updateText)
        {
            voiceInputString = updateText;
        }
        #endregion

        #region Draw Elements

        /// <summary>
        /// Draws Anything world logo and Reset All button for the Anything Creator window.
        /// </summary>
        /// <param name="bannerIcon"> Anything world banner texture to display in top left.</param>
        /// <param name="horizontalPadding"></param>
        /// <param name="verticalPadding"></param>
        private void DrawWindowBanner(Texture bannerIcon, int horizontalPadding = 10, int verticalPadding = 10)
        {

            //GetWindow<AnythingCreatorEditor>("position");
            Rect globeRect = new Rect(10, 10, 64, 64);
            Rect bannerRect = new Rect(0, 0, position.width, globeRect.yMax + 10);
            GUI.DrawTexture(bannerRect, GreenGradientBanner);
            GUI.DrawTexture(globeRect, BlackAnythingGlobeLogo);



            Rect titleRect = new Rect(globeRect.xMax + 10, 21, position.width - globeRect.xMax, 40);
            GUIContent title = new GUIContent("ANYTHING CREATOR");
            GUI.Label(titleRect, title, BuildStyle(EditorStyles.label, PoppinsStyle.Bold, 22, TextAnchor.UpperLeft, Color.black));
            Rect versionRect = new Rect(globeRect.xMax + 10, titleRect.center.y + 10, position.width - globeRect.xMax, 20);
            GUIContent version = new GUIContent(AnythingSettings.PackageVersion);
            GUI.Label(titleRect, version, BuildStyle(EditorStyles.label, PoppinsStyle.Regular, 12, TextAnchor.LowerLeft, Color.black));


            //Mask banner in layouting
            GUILayoutUtility.GetRect(position.width, bannerRect.yMax, GUILayout.MinWidth(500));

            //var titleMask = new Rect(0, 0, position.width - 100, bannerRect.yMax);
            //var buttonArea = new Rect(titleMask.xMax-10, 0, 100, bannerRect.yMax);

        }

        /// <summary>
        /// Handles search input from user and calls request to API in anything creator.
        /// </summary>
        /// <param name="active">Is search bar active and can accept input</param>
        /// <returns></returns>
        private string DrawSearchBar(bool active)
        {
            if (active)
            {
                string previousSearch = searchTerm;

                if (Event.current.Equals(Event.KeyboardEvent("return")))
                {

                    if (CheckAPI() == false)
                    {
                        ShowAPIWindow();
                        searchTermFieldString = null;
                        return null;
                    }
                    else
                    {
                        searchTerm = searchTermFieldString;
                        DoSearch(searchTermFieldString);
                    }

                }

                if (resetSearchField)
                {

                    EditorGUILayout.TextField("", inputFieldStyle, GUILayout.Height(30));
                    searchTermFieldString = "";
                    resetSearchField = false;
                }
                else
                {
                    searchTermFieldString = EditorGUILayout.TextField(searchTermFieldString, inputFieldStyle, GUILayout.Height(30));
                }

                if (GUILayout.Button("×", resetSearchButtonStyle, GUILayout.MaxWidth(30)))
                {
                    StopSearch();
                    ResetPanel();
                };

                if (searchLoading) GUI.enabled = false;
                if (GUILayout.Button("Search", searchButtonStyle, GUILayout.MaxWidth(60)))
                {
                    if (CheckAPI() == false)
                    {
                        ShowAPIWindow();
                        searchTermFieldString = null;
                        return null;
                    }
                    else
                    {
                        searchTerm = searchTermFieldString;
                        DoSearch(searchTermFieldString);
                    }
                }
                GUI.enabled = true;
                return searchTerm;
            }
            else
            {
                GUILayout.Label(searchTerm, inputFieldStyle, GUILayout.Height(30));
                return searchTerm;
            }
        }
        private void StopSearch()
        {
            searchLoading = false;
            AnythingCreator.Instance.CancelCategorySearchCoroutine();
        }
        private void ResetPanel()
        {
            searchReturned = false;
            searchStarted = false;
            resetSearchField = true;
        }
        private void DoSearch(string termToSearch)
        {
            rotationAngle = 0;
            searchResults = new List<SearchResult>();
            if (termToSearch == "")
            {
                searchStarted = false;
                searchReturned = false;
            }
            else
            {
                searchLoading = true;
                errorWhileSearching = false;
                ///Call request category search, delegate UpdateSearchResult called once search finished.
                AnythingCreator.Instance.RequestCategorySearchResults(termToSearch, UpdateSearchResults);
            }
        }


        private Vector2 newScrollPosition;
        private bool copiedToKeyboard = false;
        private Rect copiedRect;
        private int copiedResult = 0;
        private void DrawResults(List<SearchResult> customSearchResults, float scaleMultiplier = 1)
        {
            float internalMultiplier = 1.5f;
            float buttonWidth = 100f * internalMultiplier * scaleMultiplier;
            float buttonHeight = 140f * internalMultiplier * scaleMultiplier;
            float verticalMargin = 5 * internalMultiplier;
            float horizontalMargin = 5 * internalMultiplier;
            float scrollBarAllowance = 6;
            //Scale font size
            resultLabelTitleStyle.fontSize = Mathf.RoundToInt(12 * scaleMultiplier * internalMultiplier);
            resultLabelStyle.fontSize = Mathf.RoundToInt(8 * scaleMultiplier * internalMultiplier);
            float buttonWidthWithMargin = buttonWidth + horizontalMargin;

            searchResultsCount = customSearchResults.Count;
            if (customSearchResults == null) { customSearchResults = new List<SearchResult>(); }


            float resultsPerLine = Mathf.Floor((panelWidth - horizontalMargin) / buttonWidthWithMargin);
            if (resultsPerLine == 0)
            {
                return;
            }
            int rows = (int)Math.Ceiling(searchResultsCount / resultsPerLine);



            //Caculate width of results block including horizontalMargin on either side
            float actualBlockWidth = (resultsPerLine * buttonWidthWithMargin) + horizontalMargin;
            //Free space that isn't block but too small for another result to be displayed
            float outerRemainder = panelWidth - (actualBlockWidth);
            //Margin either side
            float remainderMargin = outerRemainder / 2;


            int searchResultNumber = 0;


            Rect lastRect = GUILayoutUtility.GetLastRect();
            Rect gridArea = new Rect(0, lastRect.yMax, panelWidth + scrollBarAllowance, (buttonHeight * rows) + (verticalMargin * rows));
            Rect view = new Rect(0, lastRect.yMax, windowPosition.width, windowPosition.height - lastRect.yMax);
            newScrollPosition = GUI.BeginScrollView(view, newScrollPosition, gridArea);


            //Reset copiedToKeyboard once mouse leaves panel
            string labelText = "";
            if (copiedToKeyboard)
            {
                if (!copiedRect.Contains(Event.current.mousePosition))
                {
                    copiedToKeyboard = false;
                }
                labelText = "copied to keyboard";

            }


            for (int yPos = 0; yPos < rows; yPos++)
            {
                float rowCoord = lastRect.yMax + (yPos * buttonHeight) + (verticalMargin * yPos);
                for (int xPos = 0; xPos < resultsPerLine; xPos++)
                {
                    if (searchResultNumber < searchResultsCount)
                    {
                        float columnCoord = ((xPos * buttonWidthWithMargin) + horizontalMargin + (remainderMargin - scrollBarAllowance));
                        var thisResult = customSearchResults[searchResultNumber];

                        //Assign thumbnail
                        Texture2D displayThumbnail = thisResult.Thumbnail;
                        if (displayThumbnail == null)
                        {
                            displayThumbnail = defaultThumbnailImage;
                        }
                        //Define button position and content
                        GUIContent buttonContent = new GUIContent("");
                        Rect buttonRect = new Rect(columnCoord, rowCoord, buttonWidth, buttonHeight);
                        //Render button
                        Rect thumbnailBackgroundRect = new Rect(buttonRect.x, buttonRect.y, buttonRect.width, buttonRect.width);
                        GUI.DrawTexture(thumbnailBackgroundRect, thumbnailBackgroundActive);

                        GUI.DrawTexture(thumbnailBackgroundRect, thumbnailBackgroundActive);
                        if (GUI.Button(buttonRect, buttonContent, roundedThumbnailButton))
                        {
                            if (Event.current.button == 0)
                            {
                                if (copiedToKeyboard)
                                {
                                    copiedToKeyboard = false;
                                }
                                else
                                {
                                    AnythingCreator.Instance.MakeObject(thisResult.name);
                                }


                            }
                            else if (Event.current.button == 1)
                            {
                                TextEditor te = new TextEditor();
                                te.text = thisResult.name;
                                te.SelectAll();
                                te.Copy();
                                copiedResult = searchResultNumber;
                                copiedRect = buttonRect;
                                copiedToKeyboard = true;
                            }
                        }
                        GUI.DrawTexture(thumbnailBackgroundRect, displayThumbnail);
                        //Define label position
                        Rect labelRect = new Rect(buttonRect.x, thumbnailBackgroundRect.yMax, buttonRect.width, ((buttonRect.height - thumbnailBackgroundRect.height) / 2));

                        Rect sublabelRect = new Rect(labelRect.x, labelRect.y + labelRect.height, labelRect.width, labelRect.height);

                        if (copiedToKeyboard && copiedResult == searchResultNumber)
                        {
                            Rect guidRect = new Rect(labelRect.x, labelRect.y + (0.5f * labelRect.height), labelRect.width, labelRect.height);
                            //If this entry is the one copied to keyboard 
                            labelText = "GUID copied to keyboard";
                            GUIContent label = new GUIContent(labelText);
                            GUI.Label(guidRect, label, resultLabelStyle);
                        }
                        else
                        {
                            //If mouseover display both Title & GUID
                            if (buttonRect.Contains(Event.current.mousePosition))
                            {

                                GUIContent label = new GUIContent(thisResult.name, thisResult.name);
                                GUIContent subLabelContent = new GUIContent(thisResult.data.author);

                                //Draw big label
                                var sizeOfLabel = resultLabelTitleStyle.CalcSize(label);
                                if (labelRect.width < sizeOfLabel.x)
                                {
                                    var ratio = labelRect.width / sizeOfLabel.x;
                                    var oldFontSize = resultLabelTitleStyle.fontSize;
                                    resultLabelTitleStyle.fontSize = Mathf.RoundToInt((oldFontSize) * ratio) - 1;
                                    GUI.Label(labelRect, label, resultLabelTitleStyle);
                                    resultLabelTitleStyle.fontSize = oldFontSize;
                                }
                                else
                                {
                                    GUI.Label(labelRect, label, resultLabelTitleStyle);
                                }



                                //Draw small label
                                var sizeOfRenderedSublabelText = resultLabelStyle.CalcSize(subLabelContent);
                                if (sublabelRect.width < sizeOfRenderedSublabelText.x)
                                {
                                    var ratio = labelRect.width / sizeOfRenderedSublabelText.x;

                                    var oldFontSize = resultLabelStyle.fontSize;
                                    resultLabelStyle.fontSize = Mathf.RoundToInt((oldFontSize * ratio)) - 2;
                                    GUI.Label(sublabelRect, subLabelContent, resultLabelStyle);
                                    resultLabelStyle.fontSize = oldFontSize;

                                }
                                else
                                {
                                    GUI.Label(sublabelRect, subLabelContent, resultLabelStyle);
                                }
                            }
                            else
                            {

                                Rect centeredLabelRect = new Rect(labelRect.x, labelRect.y + (0.5f * labelRect.height), labelRect.width, labelRect.height);
                                //Else just display display name, eg. guid: cat#0000, display name: Cat
                                GUIContent label = new GUIContent(thisResult.DisplayName, thisResult.name);
                                var sizeOfLabel = resultLabelTitleStyle.CalcSize(label);

                                labelText = thisResult.DisplayName;
                                if (labelRect.width < sizeOfLabel.x)
                                {
                                    var ratio = labelRect.width / sizeOfLabel.x;
                                    var oldFontSize = resultLabelTitleStyle.fontSize;
                                    resultLabelTitleStyle.fontSize = Mathf.RoundToInt((oldFontSize - 1) * ratio);
                                    GUI.Label(centeredLabelRect, label, resultLabelTitleStyle);
                                    resultLabelTitleStyle.fontSize = oldFontSize;
                                }
                                else
                                {
                                    GUI.Label(centeredLabelRect, label, resultLabelTitleStyle);
                                }


                            }



                        }


                    }
                    else
                    {
                        GUI.EndScrollView();
                        return;
                    }

                    searchResultNumber++;
                }

            }
            GUI.EndScrollView();
        }

        private float resultThumbnailMultiplier = 1;
        private void DrawResultsGrid()
        {
            if (searchLoading)
            {
                DrawResultsLoading();
                return;
            }
            else if (errorWhileSearching)
            {
                DrawErrorWhileSearching();
                return;
            }
            else if (searchReturned == true)
            {
                if (searchReturned == true && !searchLoading)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(inputFieldStyle.margin.left);
                    if (showAnimatedOnly)
                    {
                        DrawCustomText($"{animatedResults.Count} animated results found for \"{searchTerm}\"", 12, TextAnchor.MiddleLeft);
                    }
                    else
                    {
                        DrawCustomText($"{searchResults.Count} results found for \"{searchTerm}\"", 12, TextAnchor.MiddleLeft);
                    }



                    //Scale slider
                    GUIStyle empty = new GUIStyle();
                    if (GUILayout.Button(gridIcon, empty, GUILayout.ExpandWidth(false), GUILayout.Height(20)))
                    {
                        resultThumbnailMultiplier = 1f;
                    }
                    resultThumbnailMultiplier = GUILayout.HorizontalSlider(resultThumbnailMultiplier, 0.5f, 2.5f, GUILayout.MaxWidth(100));
                    float roundedScale = (Mathf.RoundToInt(resultThumbnailMultiplier * 10)) / 10f;
                    GUILayout.Label($"{roundedScale.ToString()}x", GUILayout.ExpandWidth(false), GUILayout.Width(30));
                    GUILayout.Space(inputFieldStyle.margin.left);
                    GUILayout.EndHorizontal();
                    GUILayout.Space(inputFieldStyle.margin.bottom);
                }


                if (showAnimatedOnly)
                {
                    DrawResults(animatedResults, resultThumbnailMultiplier);
                }
                else
                {
                    DrawResults(searchResults, resultThumbnailMultiplier);
                }
                //EditorGUILayout.EndScrollView();
            }
            else if (searchStarted)
            {
                DrawResultsNotFound();
            }
            else
            {
                GUILayout.BeginVertical();
                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label(GreyAnythingGlobeLogo, new GUIStyle(), GUILayout.MaxWidth(100), GUILayout.MaxHeight(100));
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(20);
                GUILayout.BeginHorizontal();
                DrawCustomText("Try searching for a model... eg. \"cat\", \"flower\", \"car\"  ", 14, TextAnchor.MiddleCenter);
                GUILayout.EndHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.EndVertical();
            }
        }
        private float rotationAngle = 0;
        private double lastEditorTime = 0;
        Rect lastRect = Rect.zero;
        private void DrawResultsLoading()
        {
            if (Event.current.type == EventType.Repaint) lastRect = GUILayoutUtility.GetLastRect();

            //if(Event.current.type==EventType.Layout)

            if (position.height - lastRect.yMax < 300)
            {




                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                var temp = new GUIStyle();
                temp.alignment = TextAnchor.MiddleCenter;
                GUILayout.Label(new GUIContent("", LoadingCenter), temp, GUILayout.MinHeight(1), GUILayout.MaxHeight(300));

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Label("Searching...", BuildStyle(EditorStyles.label, PoppinsStyle.Bold, 20, TextAnchor.MiddleCenter));
                GUILayout.FlexibleSpace();
                GUILayout.Space(10);
            }
            else
            {


                if (Event.current.type == EventType.Repaint)
                {
                    var thisTime = EditorApplication.timeSinceStartup;

                    var spinningRect = new Rect((windowPosition.width / 2) - (255 / 2), (windowPosition.height / 2) - (255 / 2) + 120, 255, 255);
                    var dt = EditorApplication.timeSinceStartup - lastEditorTime;

                    var matrixBack = GUI.matrix;
                    rotationAngle += 50f * (float)dt;
                    GUIUtility.RotateAroundPivot(rotationAngle, spinningRect.center);
                    GUI.DrawTexture(spinningRect, loadingRing);
                    GUI.matrix = matrixBack;
                    GUI.DrawTexture(spinningRect, LoadingCenter);
                    lastEditorTime = thisTime;
                }




            }




        }




        private void DrawErrorWhileSearching()
        {
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader($"Error searching for that term, please try again.", 20, TextAnchor.MiddleCenter, true);
            GUILayout.FlexibleSpace();
        }
        private void DrawResultsNotFound()
        {
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader($"No results found for \"{searchTerm}\".", 20, TextAnchor.MiddleCenter, true);
            GUILayout.FlexibleSpace();
        }
        private void DrawNavToolbar()
        {
            GUILayout.BeginHorizontal();
            try
            {
                DrawNavButton(objectCreatorButton, 0, "Object creation window");
                DrawNavButton(voiceCreatorButton, 1, "Voice creation window");
                DrawNavButton(habitatCreatorButton, 2, "Habitat creation window");
            }
            catch { }
            GUILayout.EndHorizontal();
        }
        private void DrawNavButton(Texture2D texture, int buttonOption, string tooltip)
        {
            guiColor = GUI.backgroundColor;
            GUIStyle buttonStyle;
            if (navMenuSelected == buttonOption)
            {
                GUI.backgroundColor = Color.grey;
                buttonStyle = activeButtonStyle;
            }
            else
            {
                buttonStyle = defaultButtonStyle;
            }
            if (GUILayout.Button(new GUIContent("", texture, tooltip), buttonStyle, GUILayout.MinWidth(1)))
            {
                navMenuSelected = buttonOption;
            }
            GUI.backgroundColor = guiColor;
        }

        private bool DrawMicButton()
        {
            Texture2D activeMicTex = activeMicIcon;
            Texture2D restingMicTex = inactiveMicIcon;
            bool active = false;
            ToggleGUIBackgroundClear();
            GUILayout.FlexibleSpace();
            if (micButtonTex == null) micButtonTex = restingMicTex;
            if (GUILayout.Button(micButtonTex, micButtonStyle, GUILayout.MaxWidth(micButtonTex.width)))
            {
                if (!CheckAPI())
                {
                    ShowAPIWindow();
                    return false;
                }
                AnythingVoiceCreator.Instance.StartVoiceInput();
                Repaint();
            }
            GUILayout.FlexibleSpace();
            ToggleGUIBackgroundClear();
            if (micButtonTex == activeMicTex) active = true;
            return active;
        }

        public static void ToggleMicTex(bool listening)
        {
            if (Voice.ListenForVoice)
            {
                micButtonTex = activeMicIcon;
            }
            else
            {
                micButtonTex = inactiveMicIcon;
            }
        }

        private void DrawSoundWave(bool active)
        {
            GUILayout.FlexibleSpace();
            if (active == true)
            {
                GUILayout.Label(activeWaveform, centeredLabelStyle);
            }
            else
            {
                GUILayout.Label(inactiveWaveform, centeredLabelStyle);
            }
            GUILayout.FlexibleSpace();
            Voice.GetMaxLevel();
        }

        private void DrawSpeechPreview(string inputString)
        {
            GUILayout.Label("Output:", BuildStyle(centeredLabelStyle, PoppinsStyle.Regular, 14, TextAnchor.MiddleCenter));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            //GUILayout.Label(speechBubble);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            //-------------------------------------//
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            //GUILayout.Label(inputString, BuildStyle(centeredLabelStyle, PoppinsStyle.Regular, 12, TextAnchor.MiddleCenter));
            //inputString = "Make ten cows and eight ostriches and 20 cats";
            var fieldStyle = BuildStyle(EditorStyles.textArea, PoppinsStyle.Regular, 12, TextAnchor.UpperCenter);
            fieldStyle.wordWrap = true;
            fieldStyle.padding = new RectOffset(10, 10, 10, 10);
            fieldStyle.normal.background = RoundedDarkSquare;
            fieldStyle.font = POPPINS_REGULAR;
            fieldStyle.fontSize = 16;
            //fieldStyle.margin = new RectOffset(10, 10, 10, 10);
            GUILayout.BeginVertical();
   
            var calcedSize = fieldStyle.CalcSize(new GUIContent(inputString));
            float size = Math.Min(calcedSize.x, 300);
            GUILayout.Label(inputString, fieldStyle, GUILayout.MinWidth(10), GUILayout.MaxWidth(size));
  
            GUILayout.EndVertical();
            //GUI.enabled = true;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }

        private void MicSelectionDropDown()
        {

            string[] options = null;
            try
            {
                options = Voice.GetMicrophoneArray();


                if (options == null || options.Length == 0)
                {

                    GUILayout.FlexibleSpace();
                    GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.BeginVertical();
                    GUILayout.Label("Choose audio source:", BuildStyle(centeredLabelStyle, PoppinsStyle.Regular, 14, TextAnchor.MiddleCenter));
                    GUI.enabled = false;
                    micIndex = EditorGUILayout.Popup(0, new string[] { "No microphone detected." }, EditorStyles.popup, GUILayout.MinWidth(400), GUILayout.Height(40), GUILayout.ExpandWidth(false));
                    GUI.enabled = true;
                    GUILayout.EndVertical();
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                    GUILayout.Space(10);
                    //GUILayout.Label("No mic detected",BuildStyle(EditorStyles.label, PoppinsStyle.Bold, 16, TextAnchor.MiddleCenter));
                    Repaint();
                    UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
                    UnityEditor.SceneView.RepaintAll();
                    return;
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Error getting microphone list: {e}");
                return;
            }


            if (options != null && options.Length > 0)
            {
                var micDropdownStyle = new GUIStyle(EditorStyles.popup);
                micDropdownStyle.font = POPPINS_REGULAR;
                micDropdownStyle.fontSize = 12;
                micDropdownStyle.alignment = TextAnchor.MiddleCenter;
                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.BeginVertical();
                GUILayout.Label("Choose audio source:", BuildStyle(centeredLabelStyle, PoppinsStyle.Regular, 14, TextAnchor.MiddleCenter));
                micIndex = EditorGUILayout.Popup(micIndex, options, micDropdownStyle, GUILayout.MinWidth(400), GUILayout.Height(40), GUILayout.ExpandWidth(false));
                GUILayout.EndVertical();
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(10);
                if (micIndex < options.Length)
                {
                    Voice.SelectedInputMic = options[micIndex];
                }
                else
                {
                    Voice.SelectedInputMic = options[0];
                }

            }
            else
            {
                return;
            }
            Repaint();
            UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
            UnityEditor.SceneView.RepaintAll();


        }



        private void HabitatDropDownMenu()
        {
            string[] options = { };
            try
            {
                options = Habitat.GetHabitatWithDefault();
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Error getting habitat list: {e}");
                return;
            }
            habitatIndex = EditorGUILayout.Popup(habitatIndex, options, dropDownStyle, GUILayout.Height(50));
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            smallButtonStyle.normal.textColor = GREEN_COLOR;
            if (GUILayout.Button("Create", smallButtonStyle))
            {
                if (!CheckAPI())
                {
                    ShowAPIWindow();
                    return;
                }
                if (habitatIndex != 0)
                {
                    Habitat.CreateHabitat(habitatIndex - 1, populateHabitat);
                }
            }
            smallButtonStyle.normal.textColor = RED_COLOR;
            if (GUILayout.Button("Clear", smallButtonStyle))
            {
                habitatIndex = 0;
            }
            populateHabitat = GUILayout.Toggle(populateHabitat, "Populate Habitat", toggleStyle);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }

        #endregion

        #region Utility Functions
        public void AutoCreateOnFinish()
        {
            if (autoGenerateFromVoice == true)
            {
                Voice.TryCreate();
            }
        }
        #endregion


        #endregion
    }
}
