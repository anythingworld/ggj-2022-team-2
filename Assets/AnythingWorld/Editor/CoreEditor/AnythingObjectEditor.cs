﻿using UnityEditor;
using UnityEngine;

namespace AnythingWorld.Editors
{
    [CustomEditor(typeof(AnythingObject))]
    [CanEditMultipleObjects]
    public class AnythingObjectEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            AnythingObject anythingObject = (AnythingObject)target;
            if (GUILayout.Button("Refresh"))
            {
                anythingObject.objectCreated = false;
                anythingObject.CreateObject();
            }
        }
    }
}
