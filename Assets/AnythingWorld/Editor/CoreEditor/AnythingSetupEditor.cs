using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace AnythingWorld.Editors
{
    [CustomEditor(typeof(AnythingSetup))]
    public class AnythingSetupEditor : Editor
    {
        private Vector2 scrollPosition;
        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();
            scrollPosition =  GUILayout.BeginScrollView(scrollPosition);
            if (GUILayout.Button("Generate attributions from scene objects"))
            {
                AnythingSetup.Instance.GenerateAttributionsFromScene();
            }

            GUILayout.TextArea(AnythingSetup.ModelAttributionStringBlock);
            GUILayout.EndScrollView();
        }
    }
}

