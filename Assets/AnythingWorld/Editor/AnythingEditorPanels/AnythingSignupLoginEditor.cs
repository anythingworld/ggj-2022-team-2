﻿using System.Collections;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using System.Text.RegularExpressions;
using AnythingWorld.Utilities;
namespace AnythingWorld.Editors
{
    /// <summary>
    /// Editor panel showing login/signup for Anything API keys, 
    /// under Anything World/Generate API
    /// </summary>
    public class AnythingSignupLoginEditor : AnythingEditor
    {
        #region Signup Params
        private string signupPass = "";
        private string signupPassCheck = "";
        private string signupEmail = "";
        private string signupName = "";
        private bool signupTerms = false;
        #endregion

        #region Login Params
        private string loginPass = "";
        private string loginEmail = "";
        #endregion


        private enum APIWindowPages
        {
            Landing,
            Signup,
            Login,
            SignupLanding,
            LoginLanding,
            SignupHanging,
            LoginHanging
        }
        static APIWindowPages currentWindow;

        [InitializeOnLoadMethod]
        private static void Init()
        {
            AnythingCreator.openAPIWindowDelegate = ShowWindow;
        }
        [MenuItem("Anything World/Log In or Sign Up", false, 22)]
        static void OpenFromToolbar()
        {
            currentWindow = APIWindowPages.Login;
            ShowWindow();

        }

        public static void OpenFromScript()
        {
            currentWindow = APIWindowPages.Landing;
            ShowWindow();
        }

        public static void ShowWindow()
        {
            AnythingSignupLoginEditor window = ScriptableObject.CreateInstance(typeof(AnythingSignupLoginEditor)) as AnythingSignupLoginEditor;
            //AnythingAPIGen window = EditorWindow.GetWindow<AnythingAPIGen>();
            GUIContent windowContent = new GUIContent("AnythingWorld Account Login");
            window.titleContent = windowContent;
            window.ShowUtility();
        }
        private void OnGUI()
        {
            InitializeResources();

            switch (currentWindow)
            {
                case 0:
                    LandingWindow();
                    break;
                case APIWindowPages.Signup:
                    DrawNavToolbar();
                    SignUpWindow();
                    break;
                case APIWindowPages.Login:
                    DrawNavToolbar();
                    LoginWindow();

                    break;
                case APIWindowPages.LoginLanding:
                    LoginComplete();
                    break;
                case APIWindowPages.SignupLanding:
                    SignupComplete();
                    break;
                case APIWindowPages.LoginHanging:
                    LoginHanging();
                    break;
                case APIWindowPages.SignupHanging:
                    SignupHanging();
                    break;
                default:
                    LandingWindow();
                    break;
            }
        }

        private void LandingWindow()
        {
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader("No API Found", 40, new RectOffset(10,10,10,10));
            DrawCustomText("You need to provide an API key for the AnythingWorld platform before you can start creating models.", 12, true, TextAnchor.MiddleCenter);
            if (GUILayout.Button("Login or Signup", submitButtonStyle))
            {
#if UNITY_EDITOR
                currentWindow = APIWindowPages.Login;
#endif
            }
            if (GUILayout.Button("Manually enter API Key", submitButtonStyle))
            {

                AnythingSettingsPanelEditor.ShowWindow();

                AnythingSignupLoginEditor window = GetWindow<AnythingSignupLoginEditor>();
                window.Close();
            }

            GUILayout.FlexibleSpace();
            GUILayout.Space(10);
            GUILayout.EndVertical();
        }
        private void DrawNavToolbar()
        {
            GUILayout.BeginHorizontal();
            try
            {
                DrawNavButton(loginButton, APIWindowPages.Login, "Login");
                DrawNavButton(signupButton, APIWindowPages.Signup, "Sign Up");
            }
            catch
            {

            }
            GUILayout.EndHorizontal();
        }
        private void DrawNavButton(Texture2D texture, APIWindowPages page, string tooltip)
        {
            guiColor = GUI.backgroundColor;
            GUIStyle buttonStyle;
            if (currentWindow == page)
            {
                GUI.backgroundColor = Color.grey;
                buttonStyle = activeButtonStyle;
            }
            else
            {
                buttonStyle = defaultButtonStyle;
            }
            if (GUILayout.Button(new GUIContent("", texture, tooltip), buttonStyle, GUILayout.MinWidth(1)))
            {
                currentWindow = page;
            }
            GUI.backgroundColor = guiColor;
        }


        private enum SignupErrors
        {
            None,
            PasswordLetter,
            PasswordNumber,
            PasswordLength,
            PasswordMatch,
            InvalidEmail,
            RegisteredEmail,
            EmptyFields,
            EmptyPassword,
            EmptyEmail,
            Terms
        }
        private enum LoginErrors
        {
            None,
            EmptyField,
            UnregisteredEmail,
            WrongPassword,
            InvalidEmail
        }
        SignupErrors signupError;
        private void SignUpWindow()
        {


            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();

            int spacer = 5;
            int bigspacer = 10;
            int textSize = 14;
            DrawUILine(Color.gray);
            DrawBoldTextHeader("Sign Up For API");
            GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
            fieldStyle.font = POPPINS_REGULAR;

            DrawCustomText("Enter name: ", textSize);
            GUILayout.Space(spacer);
            signupName = GUILayout.TextField(signupName, fieldStyle);

            GUILayout.Space(bigspacer);

            DrawCustomText("Enter email: ", textSize);
            GUILayout.Space(spacer);
            signupEmail = GUILayout.TextField(signupEmail, fieldStyle);
            if (signupError == SignupErrors.InvalidEmail)
            {
                DrawCustomText("Invalid Email.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupError == SignupErrors.RegisteredEmail)
            {
                DrawCustomText("Email already registered.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else
            {
                DrawCustomText(".", 12, Color.clear);
            }

            GUILayout.Space(bigspacer);

            DrawCustomText("Type password: ", textSize);
            GUILayout.Space(spacer);
            signupPass = GUILayout.PasswordField(signupPass, "*"[0], 25, fieldStyle);

            GUILayout.Space(bigspacer);

            DrawCustomText("Retype password: ", textSize);
            GUILayout.Space(spacer);
            signupPassCheck = GUILayout.PasswordField(signupPassCheck, "*"[0], 25, fieldStyle);
            if (signupPass.Length > 0 && signupPassCheck.Length > 0 && signupPass != signupPassCheck)
            {
                DrawCustomText("Passwords do not match.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupPass.Length != signupPassCheck.Length)
            {
                DrawCustomText("Passwords do not match.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupError == SignupErrors.PasswordLetter)
            {
                //Debug.Log("Password letter");

                DrawCustomText("Password must contain letters.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupError == SignupErrors.PasswordNumber)
            {
                DrawCustomText("Password must contain numbers.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupError == SignupErrors.EmptyFields)
            {
                DrawCustomText("All fields must be filled.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (signupError == SignupErrors.Terms)
            {
                DrawCustomText("Please agree to terms and conditions.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else
            {
                DrawCustomText(".", 12, Color.clear);
            }
            GUILayout.Space(bigspacer);

            GUILayout.BeginHorizontal();
            GUIStyle temp = new GUIStyle(EditorStyles.label);
            temp.fontSize = 12;
            temp.alignment = TextAnchor.UpperLeft;
            temp.font = POPPINS_REGULAR;
            temp.padding.right = 0;



            GUILayout.FlexibleSpace();
            GUILayout.Label("I have read and agree with the terms in the", temp, GUILayout.ExpandWidth(false));
            temp.padding.left = 0;
            Color originalColor = temp.normal.textColor;
            temp.normal.textColor = GREEN_COLOR;
            if (GUILayout.Button(" user agreement", temp, GUILayout.ExpandWidth(false)))
            {
                Application.OpenURL("https://anything.world/user-agreement");
            }
            temp.normal.textColor = originalColor;
            GUILayout.Label(".", temp, GUILayout.ExpandWidth(false));
            GUILayout.Space(spacer);
            signupTerms = GUILayout.Toggle(signupTerms, "");


            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(bigspacer);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();


            GUI.enabled = !submitting;
            if (GUILayout.Button("Submit", submitButtonStyle))
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(SubmitSignup(), this);
#endif
            }
            GUI.enabled = true;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(bigspacer);


            GUILayout.EndVertical();
            GUILayout.Space(10);
            GUILayout.EndHorizontal();

        }
        private void LoginWindow()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            int spacer = 5;
            int bigspacer = 10;
            int textSize = 14;
            DrawUILine(Color.gray);
            DrawBoldTextHeader("Login");

            GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
            fieldStyle.font = POPPINS_REGULAR;


            DrawCustomText("Email ", textSize);
            GUILayout.Space(spacer);
            loginEmail = GUILayout.TextField(loginEmail, fieldStyle);

            if (loginError == LoginErrors.UnregisteredEmail)
            {
                DrawCustomText("No account with this email found.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (loginError == LoginErrors.InvalidEmail)
            {
                DrawCustomText("Invalid email.", 12, new Color(0.8f, 0.3f, 0.3f));
            }


            GUILayout.Space(bigspacer);

            DrawCustomText("Password ", textSize);
            GUILayout.Space(spacer);
            loginPass = GUILayout.PasswordField(loginPass, "*"[0], 25, fieldStyle);
            if (loginError == LoginErrors.WrongPassword)
            {
                DrawCustomText("Incorrect password.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            else if (loginError == LoginErrors.EmptyField)
            {
                DrawCustomText("All fields must be filled.", 12, new Color(0.8f, 0.3f, 0.3f));
            }
            GUILayout.Space(bigspacer);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Submit", submitButtonStyle))
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(SubmitLogin(), this);
#endif
            }

            if (Event.current.keyCode == KeyCode.Return)
            {
                // Debug.Log("returned");
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(SubmitLogin(), this);
#endif
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(bigspacer);

            GUILayout.EndVertical();
            GUILayout.Space(10);
            GUILayout.EndHorizontal();
        }
        private void LoginComplete()
        {
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader("Login Success!", 40);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Apply and Close", submitButtonStyle))
            {
#if UNITY_EDITOR
                ApplyLoginResponse();
#endif
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }
        private void SignupComplete()
        {
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader("Succesful signup!", 40);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Login and Apply API Key", submitButtonStyle))
            {
#if UNITY_EDITOR
                ApplyLoginResponse();
#endif
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }

        private void LoginHanging()
        {
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader("Logging in...", 40);
            DrawBoldTextHeader("Please wait.", 20);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }
        private void SignupHanging()
        {
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            DrawBoldTextHeader("Signing Up...", 40);
            DrawBoldTextHeader("Please wait.", 20);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }

        bool submitting = false;
        private IEnumerator SubmitSignup()
        {

            currentWindow = APIWindowPages.SignupHanging;

            signupError = SignupErrors.None;
            if (signupEmail.Length == 0 || signupPass.Length == 0 || signupPassCheck.Length == 0 || signupName.Length == 0)
            {
                //If a field is found to empty, set error and stop submission.
                signupError = SignupErrors.EmptyFields;
                yield break;
            }
            else if (signupTerms == false)
            {
                //If terms not accepted flag error and return to form.
                signupError = SignupErrors.Terms;
                yield break;
            }

            WWWForm form = new WWWForm();
            form.AddField("email", signupEmail);
            form.AddField("password", signupPass);
            form.AddField("passwordCheck", signupPassCheck);
            form.AddField("terms", "true");
            form.AddField("tier", "individual");
            form.AddField("fullName", signupName);

            UnityWebRequest www = UnityWebRequest.Post("https://subscription-website-server.herokuapp.com/users/register", form);
            www.timeout = 2;
            submitting = true;
            yield return www.SendWebRequest();

            if (CheckWebRequest.IsError(www))
            {
                currentWindow = APIWindowPages.Signup;
                ParseSignupError(www.downloadHandler.text);
                submitting = false;
                yield return null;
            }
            else
            {
                submitting = false;
                ParseSignupResponse(www.downloadHandler.text);
            }
        }

        private IEnumerator SubmitLogin()
        {
            currentWindow = APIWindowPages.LoginHanging;
            WWWForm form = new WWWForm();
            form.AddField("email", loginEmail);
            form.AddField("password", loginPass);

            UnityWebRequest www = UnityWebRequest.Post("https://subscription-website-server.herokuapp.com/users/login", form);
            www.timeout = 0;
            yield return www.SendWebRequest();
            if (CheckWebRequest.IsError(www))
            {

                ////ParseLoginError(www.downloadHandler.text);
                Debug.Log($"Error logging into Anything World: {www.downloadHandler.text}");
                //Debug.LogWarning(www.error);
                currentWindow = APIWindowPages.Login;
            }
            else
            {
                ParseLoginResponse(www.downloadHandler.text);
                currentWindow = APIWindowPages.LoginLanding;
            }
            yield return null;
        }

        private void ParseSignupError(string error)
        {


            string[] arr = error.Split('\"');

            string errorStr = arr[3];
            switch (errorStr)
            {
                case "please enter a valid email address":
                    signupError = SignupErrors.InvalidEmail;
                    break;
                case "password must include letters":
                    signupError = SignupErrors.PasswordLetter;
                    break;
                case "password must include numbers":
                    signupError = SignupErrors.PasswordNumber;
                    break;
                case "passwords don't match":
                    signupError = SignupErrors.PasswordMatch;
                    break;
                case "passwords must be longer than 5 characters":
                    signupError = SignupErrors.PasswordLength;
                    break;
                case "an account with this email already exists":
                    signupError = SignupErrors.RegisteredEmail;
                    break;
                case "not all fields have been entered":
                    signupError = SignupErrors.EmptyFields;
                    break;
                default:
                    Debug.Log("Error message not handled");
                    Debug.Log(error);
                    break;
            }
        }

        private LoginErrors loginError;

        private void HandleLoginError(string error)
        {
            
        }

        private void ParseLoginError(string error)
        {
            //Debug.Log(error);
            loginError = LoginErrors.None;
            string[] arr = error.Split('\"');
            string errorStr = arr[3];
            switch (errorStr)
            {
                case "not all fields have been entered":
                    loginError = LoginErrors.EmptyField;
                    break;
                case "no account with this email found":
                    loginError = LoginErrors.UnregisteredEmail;
                    break;
                case "invalid credentials":
                    loginError = LoginErrors.WrongPassword;
                    break;
                case "please enter a valid email address":
                    loginError = LoginErrors.InvalidEmail;
                    break;
            }
        }

        private void ParseSignupResponse(string text)
        {
            string cleanedText = Regex.Replace(text, @"[[\]]", "");
            string[] arr = cleanedText.Split(',');
            apiKey = (arr[5].ToString().Split(':'))[1].Trim('\"');
            fetchedEmail = (arr[1].ToString().Split(':')[1].Trim('\"'));
            currentWindow = APIWindowPages.SignupLanding;
        }

        private string fetchedEmail = "";
        private string apiKey = "";
        private void ParseLoginResponse(string text)
        {

            string cleanedText = Regex.Replace(text, @"[[\]]", "");
            string[] arr = cleanedText.Split(',');
            apiKey = (arr[3].ToString().Split(':'))[1].Trim('\"');
            fetchedEmail = (arr[5].ToString().Split(':')[1].Trim('\"'));
        }
        private void ApplyLoginResponse()
        {
            AnythingSettings.Instance.apiKey = apiKey;
            AnythingSettings.Instance.email = fetchedEmail;
            AnythingSignupLoginEditor window = GetWindow<AnythingSignupLoginEditor>();
            Undo.RecordObject(AnythingSettings.Instance, "Added API Key and Email to AnythingSettings");
            EditorUtility.SetDirty(AnythingSettings.Instance);
            window.Close();
        }

        protected struct LoginResponse
        {
            public string user;

        }
        public struct LoginUser
        {
            public string id;
            public string fullName;
            public string apiKey;
            public string tier;
            public string email;
            public string stripeCustomer;
        }
        IEnumerator WaitForResponse(UnityWebRequest webRequest)
        {
            float progress = 0;
            while (!webRequest.isDone)
            {
                //progressBar.value = webRequest.downloadProgress;
                progress = webRequest.downloadProgress;
                yield return new WaitForSeconds(0.2f);
                Debug.Log(progress);
            }
        }
    }
}



