using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using AnythingWorld.Utilities;


namespace AnythingWorld.Habitat
{
    [ExecuteAlways]
    // [System.Serializable]
    public class AWHabitat : MonoBehaviour
    {
        // TODO: environment loading for habitats

        private const int HABITAT_OBJECT_TOTAL = 25;
        private const int HABITAT_VARIETY_TOTAL = 5;

        private string habitatName;
        private AWThing[] _awThings;
        private List<string> _objectsCreated;
        private int _objectVariety;
        public bool _awObjNotFound = false;
        public bool _awKeyInvalid = false;
        public bool _awAppIdInvalid = false;
        private bool _awHabitatReady;
        public bool AWHabitatReady
        {
            get
            {
                return _awHabitatReady;
            }
        }

        private string[] _randomObjects;
        public string[] RandomObjects
        {
            get
            {
                return _randomObjects;
            }
        }
        public bool AWObjNotFound
        {
            get
            {
                return _awObjNotFound;
            }
            set
            {
                _awObjNotFound = value;
            }
        }
        public bool AWKeyInvalid
        {
            get
            {
                return _awKeyInvalid;
            }
            set
            {
                _awKeyInvalid = value;
            }
        }
        public bool AWAppIdInvalid
        {
            get
            {
                return _awAppIdInvalid;
            }
            set
            {
                _awAppIdInvalid = value;
            }
        }

        public void MakeHabitat(string habitatKey)
        {
            _awHabitatReady = false;
            habitatName = habitatKey;
            if (habitatKey != "testing")
            {
                StartCoroutine(RequestHabitatObjects());
                _objectsCreated = new List<string>();
                _objectVariety = 0;
            }

        }

        private IEnumerator RequestHabitatObjects()
        {



            string APICall = AnythingApiConfig.ApiUrlStem +"/anything?"+ habitatName + "=true" + "&key=" + AnythingSettings.Instance.apiKey + "&app=" + AnythingSettings.Instance.appName;

            if (AnythingSettings.DebugEnabled) Debug.Log(APICall);
            UnityWebRequest www = UnityWebRequest.Get(APICall);
            yield return www.SendWebRequest();


            if (CheckWebRequest.IsError(www))
            {
                if (www.error != "HTTP/1.1 502 Bad Gateway")
                {
                    AnythingSetup.Instance.ShowLoading(false);
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log(www.error);
                    ErrorResponse response = ErrorUtility.HandleErrorResponseAWDebug(www.downloadHandler.text);

                    if (response.code == "Model not found")
                    {
                        MakeEnvironment();
                        yield break;
                    }
                    else
                    {
                        yield break;
                    }
                }
                else
                {
                    Debug.Log($"Server error, please try the operation again.\n {www.error}");
                    yield break;
                }


            }
            else
            {

            }



            string result = www.downloadHandler.text;

            _awThings = JSONHelper.GetJSONArray<AWThing>(www.downloadHandler.text);
            int totalObjects = _awThings.Length;

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(MakeRandomHabitatCreatures(), this);
#endif
            }
            else
            {
                StartCoroutine(MakeRandomHabitatCreatures());
            }
        }

        private IEnumerator MakeRandomHabitatCreatures()
        {
            int createdObjects = 0;
            string randomObject;
            int randomIndex;
            _randomObjects = new string[HABITAT_OBJECT_TOTAL];
            while (createdObjects < HABITAT_OBJECT_TOTAL)
            {
                if (_objectVariety < HABITAT_VARIETY_TOTAL)
                {
                    randomIndex = UnityEngine.Random.Range(0, _awThings.Length);
                    randomObject = _awThings[randomIndex].name;
                    if (!_objectsCreated.Contains(randomObject))
                    {
                        _objectVariety++;
                        _objectsCreated.Add(randomObject);
                    }
                }
                else
                {
                    randomIndex = UnityEngine.Random.Range(0, _objectsCreated.Count);
                    randomObject = _objectsCreated[randomIndex];
                }


                _randomObjects[createdObjects] = randomObject;

                createdObjects++;
            }
            _awHabitatReady = true;
            MakeEnvironment();
            yield return null;
        }

        private void MakeEnvironment()
        {
            GameObject environmentObject = EnvironmentMap.EnvironmentPrefab(habitatName);
            GameObject environmentPrefab = Instantiate(environmentObject, environmentObject.transform.localPosition, environmentObject.transform.rotation) as GameObject;
            environmentPrefab.transform.parent = transform;

            // reset all AnythingObjects in environment
            AnythingObject[] anythingObjects = environmentPrefab.GetComponentsInChildren<AnythingObject>();
            foreach (AnythingObject anythingObject in anythingObjects)
            {
                anythingObject.objectCreated = false;
            }

            EnvironmentLights environLights = EnvironmentMap.EnvironmentLighting(habitatName);
            GameObject awLightGO = GameObject.FindGameObjectWithTag("AWSun");

            Material SkyBoxMaterial = Resources.Load("Materials/" + environLights.SkyBoxMaterial) as Material;
            RenderSettings.skybox = SkyBoxMaterial;

            //Debug.Log("change skybox to : " + environLights.SkyBoxMaterial);

            if (awLightGO != null)
            {
                Light awLight = awLightGO.GetComponent<Light>();
                awLight.color = environLights.LightColor;
                awLight.intensity = environLights.LightIntensity;
            }

            RenderSettings.fogColor = environLights.FogColor;
            RenderSettings.fogMode = environLights.FogType;
            if (environLights.FogType == FogMode.ExponentialSquared)
            {
                RenderSettings.fogDensity = environLights.FogDensity;
            }
            else if (environLights.FogType == FogMode.Linear)
            {
                RenderSettings.fogStartDistance = environLights.FogStart;
                RenderSettings.fogEndDistance = environLights.FogEnd;
            }
        }

    }
}
