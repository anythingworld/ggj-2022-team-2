﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace AnythingWorld.Utilities
{
    /// <summary>
    /// Automatically object destruction for editor and runtime with one call.
    /// </summary>
    public class AnythingSafeDestroy
    {

        /// <summary>
        /// Safe version of the Destroy call that will work during editor and runtime.
        /// Destroy does not usually work during editor mode as the call execution is different. 
        /// Destroy immediate is used at the end of the editor loop to replicate Destroy.
        /// </summary>
        /// <param name="_object"></param>
        /// <param name="_nullify"></param>
        public static void SafeDestroy(UnityEngine.Object _object, bool _nullify = true)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    try
                    {
                        UnityEngine.Object.DestroyImmediate(_object);
                        if (_nullify) _object = null;
                    }
                    catch(Exception e)
                    {
                        Debug.Log($"Exception thrown while trying to safe destroy object: {e}");
                    }

                };
#endif
            }
            else
            {
                UnityEngine.Object.Destroy(_object);
                if (_nullify) _object = null;
            }
        }


        public static void ClearList<T>(List<T> list)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    list.Clear();
                };
#endif
            }
            else
            {
                list.Clear();
            }
        }

        public static void DestroyListOfObjects(List<UnityEngine.Object> objects)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    foreach(UnityEngine.Object obj in objects)
                    {
                        UnityEngine.Object.DestroyImmediate(obj);
                    }
                };
#endif
            }
            else
            {
                foreach(UnityEngine.Object obj in objects)
                {
                    UnityEngine.Object.Destroy(obj);
                }
            }
        }
        public static void DestroyListOfObjects(UnityEngine.Object[] objects)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    foreach (UnityEngine.Object obj in objects)
                    {
                        UnityEngine.Object.DestroyImmediate(obj);
                    }
                };
#endif
            }
            else
            {
                foreach (UnityEngine.Object obj in objects)
                {
                    UnityEngine.Object.Destroy(obj);
                }
            }
        }
    }
}



