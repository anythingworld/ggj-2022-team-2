﻿using UnityEngine;

namespace AnythingWorld.Utilities
{
    public class CenterMeshPivot : MonoBehaviour
    {
        private const bool SHOW_DEBUG_CENTER_SPHERE = false;
        private GameObject pivotSphere;


        private GameObject newPivotObject;
        private float wheelRadius = 0;

        public PivotPosition centerPivot;

        public enum PivotPosition
        {
            TopPivot,
            CenterPivot,
            LowerPivot
        };
        public void CreateNewMeshPivot()
        {
            GameObject loadedObject = transform.GetChild(0).gameObject;
            GameObject meshObject = loadedObject.GetComponentInChildren<MeshFilter>().gameObject;


            newPivotObject = new GameObject("MeshPivot").gameObject;
            newPivotObject.transform.parent = loadedObject.transform;
            newPivotObject.transform.localPosition = Vector3.zero;



            MeshFilter wheelMesh = meshObject.GetComponent<MeshFilter>();
            wheelRadius = wheelMesh.sharedMesh.bounds.extents.y * loadedObject.transform.localScale.y;
            MeshRenderer wheelRenderer = meshObject.GetComponent<MeshRenderer>();
            Vector3 meshCenterPositionLocal = wheelMesh.sharedMesh.bounds.center;
            Vector3 meshCenterPositionWorld = meshObject.transform.TransformPoint(meshCenterPositionLocal);


            newPivotObject.transform.position = meshCenterPositionWorld;

            GameObject newWheel = Instantiate(meshObject);
            newWheel.transform.parent = loadedObject.transform;
            newWheel.transform.position = meshObject.transform.position;
            newWheel.transform.rotation = meshObject.transform.rotation;
            newWheel.transform.localScale = meshObject.transform.localScale;
            newWheel.GetComponent<MeshRenderer>().enabled = true;
            newWheel.transform.parent = newPivotObject.transform;


            wheelRenderer.enabled = false;
        }

      
        private void CenterPivotOnMesh()
        {
            CreateNewMeshPivot();

            /*
            // TODO: be careful! specific to one GameObject structure for wheels [ this -> parentholder -> rotator ]
            MeshFilter wheelMeshFilter = transform.GetComponentInChildren<MeshFilter>();
            if (wheelMeshFilter != null)
            {
                Mesh wheelMesh = wheelMeshFilter.mesh;

                Transform loadedObj = transform.GetChild(0);
                Transform defaultObj = loadedObj.GetChild(0);

                Vector3 center = wheelMesh.bounds.center; //  mesh bounding box center point in local space 
                
                
                Vector3 objCenterWorldSpace = transform.TransformPoint(center);   //  world space position of mesh bounding box center
                Vector3 relativePos = transform.InverseTransformPoint(center);


                pivotSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                pivotSphere.name = "wheelcenter";
                Destroy(pivotSphere.GetComponent<Collider>());
                pivotSphere.transform.parent = loadedObj;
                pivotSphere.transform.localPosition = Vector3.zero;
                // debug indicator of center
                if (!SHOW_DEBUG_CENTER_SPHERE)
                {
                    Destroy(pivotSphere.GetComponent<Renderer>());
                }


                wheelRadius = wheelMesh.bounds.extents.y * loadedObj.localScale.y;

                defaultObj.localPosition = center * -1;

                // obj is scaled, so adjust here accordingly
                float referenceScale = loadedObj.localScale.x;

                // Debug.Log($"{gameObject.name} referenceScale = {referenceScale}");

                transform.localPosition += objCenterWorldSpace * referenceScale;
                transform.localPosition -= transform.parent.position * referenceScale;

                return objCenterWorldSpace;

            }

            return Vector3.zero;
            */
        }


        public Vector3 PivotCenter
        {
            get
            {
                if (newPivotObject == null)
                {
                    CenterPivotOnMesh();
                }
                return newPivotObject.transform.position;
            }
        }

        public float Radius
        {
            get
            {
                if (newPivotObject == null)
                {
                    CenterPivotOnMesh();
                }

                return wheelRadius;
            }
        }
    }
}

