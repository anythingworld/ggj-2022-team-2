﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor.Experimental.SceneManagement;
#endif

using AnythingWorld.Utilities;
namespace AnythingWorld
{

    [ExecuteAlways]
    [Serializable]

    public class AnythingObject : MonoBehaviour
    {
        #region Fields
        public string objectName;
        public string objectGUID;
        public bool createInEditor = true;
        public bool hasBehaviour = false;
        public bool hasCollider = false;
        public bool randomYRotation = false;
        public bool globallyPosition = false;

        [SerializeField]
        public bool objectCreated = false;
        private AnythingCreator anythingCreator;
        private string lastObjectName;
        #endregion

        #region Unity Callbacks
        public void Awake()
        {
            anythingCreator = AnythingCreator.Instance;

#if UNITY_EDITOR
            if (PrefabStageUtility.GetCurrentPrefabStage() != null)
                objectCreated = false;
#endif
        }

        public void Start()
        {
            if (objectCreated)
                return;

            if (objectName != null && !objectCreated)
            {
                CreateObject();
            }
        }
        #endregion

        #region Private Methods
        private void CleanUp()
        {
            AWObj[] awObjs = GetComponentsInChildren<AWObj>();
            foreach (AWObj awObj in awObjs)
            {

                AnythingSafeDestroy.SafeDestroy(awObj.gameObject);

            }
        }

        public void CreateObject()
        {

            if (!objectCreated)
            {
                CleanUp();
                var createObjectTerm = objectName;
                if (objectGUID != "")
                {
                    createObjectTerm = objectName + "#" + objectGUID;
                }


                AnythingCreator.Instance.MakeObject(createObjectTerm, Vector3.zero, Quaternion.identity, Vector3.one, transform, hasBehaviour, hasCollider, globallyPosition);
                objectCreated = true;
                lastObjectName = objectName;



                if (randomYRotation)
                {
                    transform.eulerAngles = new Vector3(transform.rotation.x, UnityEngine.Random.Range(0, 360), transform.rotation.z);
                }
                transform.name = createObjectTerm;
            }

        }

        #endregion

    }

}
