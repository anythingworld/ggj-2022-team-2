﻿using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEngine.Networking;
using AnythingWorld.Utilities;
using AnythingWorld.Behaviours;
using AnythingWorld.Animation;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif

namespace AnythingWorld
{
    [ExecuteAlways]
    [Serializable]
    public class AWObj : MonoBehaviour
    {
        #region Fields

        [SerializeField, HideInInspector]
        public delegate void OnBehaviourRefreshDelegate();
        [SerializeField, HideInInspector]
        public OnBehaviourRefreshDelegate onBehaviourRefreshMethod;

        [SerializeField, HideInInspector]
        public delegate void OnObjectMadeSuccessfully();

        [SerializeField, HideInInspector]
        public OnObjectMadeSuccessfully onObjectMadeSuccessfullyDelegate;


        private string apiOverride = "";
        public bool SHOW_DEBUG_BOXES = false;

        private Dictionary<string, Texture> _objTextures;
        private Dictionary<string, string> _objParts;
        private string _materialName;
        private string _textureName;
        private string[] textureAddresses;
        private Texture[] textureArray;
        private int _objIndex;
        private Material _objMaterial;
        private AWThing requestedObject;
        private SketchfabThing _sketchfabRequest;
        private Shader _universalLitShader;
        private Shader _fishWobbleShader;
        private Shader _fishBigWobbleShader;
        private Shader _fishWriggleShader;
        private Shader _fishWriggleBigShader;
        private Shader _uniformWriggleShader;
        private Shader _uniformSlitherShader;
        private Shader _uniformSlitherVerticalShader;
        private Shader _quadrupedCrawlShader;
        private Material _anythingMaterial;
        private string _rawMaterial;

        [SerializeField, HideInInspector]
        private GameObject mappedPrefab;

        private bool isPolyObject;

        [SerializeField, HideInInspector]
        public string objName;

        [SerializeField, HideInInspector]
        private string objectCategory;

        [SerializeField, HideInInspector]
        private string prefabName;

        [SerializeField, HideInInspector]
        private string categoryBehaviourName;

        [SerializeField, HideInInspector]
        private string behaviour;

        [SerializeField, HideInInspector]
        private bool hasBehaviour;

        [SerializeField, HideInInspector]
        public AWBehaviourController behaviourController;

        [SerializeField, HideInInspector]
        public ErrorResponse objError;

        [SerializeField, HideInInspector]
        public bool objHasError = false;
        [SerializeField, HideInInspector]
        private bool objMade = false;

        [HideInInspector]
        public bool awObjNotFound = false;

        [HideInInspector]
        public bool awApiKeyInvalid = false;

        [HideInInspector]
        public bool awAppNameInvalid = false;

        private List<GameObject> objLoadedObjs;
        private List<Renderer> objRendererList;
        private List<MeshFilter> objMeshList;
        private Vector3 _awObjectPosition = Vector3.zero;
        private List<Rigidbody> prefabRigidbodies;
        private Collider[] prefabColliders;
        private Joint[] _prefabJoints;
        private AWBehaviour[] awBehaviours;
        private GameObject boundsShow;

        [SerializeField]
        private string creatorAttribution;
        private bool objHasCollider;

        [SerializeField, HideInInspector]
        private Vector2 _objectScale;

        [SerializeField, HideInInspector]
        private float boundsYOffset;

        [SerializeField, HideInInspector]
        public float BoundsYOffset
        {
            get
            {
                return boundsYOffset;
            }
        }

        public string CreatorAttribution
        {
            get
            {
                return creatorAttribution;
            }
        }

        [SerializeField, HideInInspector]
        private float objectScale;

        [SerializeField, HideInInspector]
        public float ObjectScale
        {
            get
            {
                return objectScale;
            }
        }

        private bool addDefaultBehaviour = true;

        [SerializeField, HideInInspector]
        public GameObject behaviourObject;

        [SerializeField, HideInInspector]
        public GameObject awThing
        {
            get
            {
                if (mappedPrefab == null)
                {
                    if (objMade)
                    {
                        AddCollider addedCollider = GetComponentInChildren<AddCollider>();
                        if (addedCollider != null)
                        {
                            mappedPrefab = GetComponentInChildren<AddCollider>().gameObject;
                        }
                        else
                        {
                            mappedPrefab = transform.gameObject;
                        }

                        return mappedPrefab;
                    }
                    Debug.LogError("No _mappedPrefab found yet, object may not have finished being made");
                    return null;
                }
                else
                {
                    return mappedPrefab;
                }
            }
        }

        [HideInInspector]
        public bool ObjMade
        {
            get
            {
                return objMade;
            }
            set
            {

                objMade = value;
                if (value == true)
                {
                    onObjectMadeSuccessfullyDelegate?.Invoke();
                }
            }
        }

        [HideInInspector]
        public bool AWObjNotFound
        {
            get
            {
                return awObjNotFound;
            }
            set
            {
                awObjNotFound = value;
            }
        }

        [HideInInspector]
        public bool AWKeyInvalid
        {
            get
            {
                return awApiKeyInvalid;
            }
            set
            {
                awApiKeyInvalid = value;
            }
        }

        [HideInInspector]
        public bool AWAppIdInvalid
        {
            get
            {
                return awAppNameInvalid;
            }
            set
            {
                awAppNameInvalid = value;
            }
        }

        [HideInInspector]

        #endregion Fields

        public bool HasBehaviour
        {
            get
            {
                return hasBehaviour;
            }
        }

        public void SetAPIOverride(string url)
        {
            apiOverride = url;
        }

        public void MakeAWObj(string objName, Vector3 objPos)
        {
            hasBehaviour = objHasCollider = true;
            _awObjectPosition = objPos;
            if (behaviourObject == null)
            {
                BuildBehaviourContainer();
            }
            MakeAWObj(objName);
        }

        public void MakeAWObj(string objName, bool hasBehaviour)
        {
            this.hasBehaviour = hasBehaviour;
            objHasCollider = true;
            MakeAWObj(objName);
        }

        public void MakeAWObj(string objName, bool hasBehaviour, bool hasCollider)
        {
            this.hasBehaviour = hasBehaviour;
            objHasCollider = hasCollider;
            MakeAWObj(objName);
        }

        public void MakeAWObj(string objName)
        {
            objMade = false;
            this.objName = objName.ToLower();
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(RequestObject(this.objName), this);
#endif
            }
            else
            {
                StartCoroutine(RequestObject(this.objName));
            }
        }

        public void ActivateRBs(bool shouldActivate)
        {
            if (prefabRigidbodies == null)
                GatherRBs();

            foreach (Rigidbody rb in prefabRigidbodies)
            {
                if ((shouldActivate && hasBehaviour) || !shouldActivate)
                {
                    if (rb.isKinematic)
                    {
                        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                    }
                    rb.isKinematic = shouldActivate ? false : true;

                }
                rb.velocity = rb.angularVelocity = Vector3.zero;
            }
            if ((objHasCollider) || (!objHasCollider && !shouldActivate))
            {
                foreach (Collider cl in prefabColliders)
                {
                    cl.enabled = shouldActivate;
                }
            }
            if (shouldActivate && !hasBehaviour)
                return;

            foreach (AWBehaviour awB in awBehaviours)
            {
                awB.enabled = shouldActivate;
            };
        }

        public string GetObjCategory()
        {
            return objectCategory;
        }

        public string GetObjName()
        {
            return prefabName;
        }

        public string GetObjCatBehaviour()
        {
            if (hasBehaviour)
            {
                return categoryBehaviourName;
            }
            else
            {
                return objectCategory;
            }
        }

        public void Awake()
        {
            hasBehaviour = objHasCollider = true;

            Init();
            _universalLitShader = Shader.Find("Anything World/Simple Lit");
            _fishWobbleShader = Shader.Find("Shader Graphs/Fish Animation");
            _fishWriggleShader = Shader.Find("Shader Graphs/Fish Vertical Animation");
            _fishWriggleBigShader = Shader.Find("Shader Graphs/Fish Vertical Big Animation");
            _uniformWriggleShader = Shader.Find("Shader Graphs/Wriggle Animation");
            _uniformSlitherShader = Shader.Find("Shader Graphs/Slither Animation");
            _uniformSlitherVerticalShader = Shader.Find("Shader Graphs/Slither Vertical Animation");
            _quadrupedCrawlShader = Shader.Find("Shader Graphs/Crawler Animation");
            _fishBigWobbleShader = Shader.Find("Shader Graphs/Fish Big Animation");
            _anythingMaterial = Resources.Load("Materials/AnythingMaterial") as Material;
        }

        private void Init()
        {
            _objParts = new Dictionary<string, string>();
            _objTextures = new Dictionary<string, Texture>();
            objRendererList = new List<Renderer>();
            objMeshList = new List<MeshFilter>();
            objLoadedObjs = new List<GameObject>();
            _objIndex = 0;
        }

        #region Request Object Pipeline


        /// <summary>
        /// Requests object from the server and if successful hands off to object loading processes.
        /// </summary>
        /// <param name="objName"></param>
        /// <returns></returns>
        private IEnumerator RequestObject(string objName)
        {

            if (String.IsNullOrEmpty(AnythingSettings.Instance.appName))
            {
                AnythingSettings.Instance.appName = "My App";
            }

            if (String.IsNullOrEmpty(AnythingSettings.Instance.apiKey))
            {
                objHasError = true;
                Debug.LogError("Error: No API key found in AnythingSettings.");
                yield break;
            }

            if (objName == "")
            {
                objError = ErrorUtility.CreateErrorResponse("Empty name input, cannot search.");
                awApiKeyInvalid = false;
                objHasError = true;
                yield break;
            }

            isPolyObject = false;
            char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
            objName = objName.Trim(charsToTrim);



            string encodedObjName = System.Uri.EscapeUriString(objName);

            string baseUrl = AnythingApiConfig.ApiUrlStem + "/anything?key=" + AnythingSettings.ApiKey;
            if (apiOverride != "")
            {
                baseUrl = apiOverride+ "/anything?key=" + AnythingSettings.ApiKey;
                //apiCall = $"{apiOverride}{objName}";
            }


            if (encodedObjName.Contains("#"))
            {
                encodedObjName = encodedObjName.Replace("#", "%23");
            }
            if (encodedObjName.Contains(" "))
            {
                encodedObjName = encodedObjName.Replace(" ", "%20");
            }
            string encodedAppName = System.Uri.EscapeUriString(AnythingSettings.AppName);

            if (encodedAppName != "" && encodedAppName != null)
            {
                baseUrl = baseUrl + "&app=" + encodedAppName;
            }
            string apiCall = baseUrl + "&name=" + encodedObjName;
/*            if (apiOverride != "")
            {
                apiCall = $"{apiOverride}{objName}";
            }*/
       
            if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Making request to server: {apiCall}");

            UnityWebRequest www = UnityWebRequest.Get(apiCall);
            www.timeout = 20; //1; // TODO restore original value after integrating Sketchfab
            yield return www.SendWebRequest();



            if (CheckWebRequest.IsError(www))
            {
                objError = ErrorUtility.HandleErrorResponseAWDebug(www.downloadHandler.text);
                awApiKeyInvalid = true;
                objHasError = true;
                yield break;
            }

            string result = www.downloadHandler.text;
            if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"JSON response for \"{objName}\" returned. \n {result}");

            if (result == "[]")
            {
                Debug.LogError($"Error with model request \"{objName}\" \n Access customer support at our discord: \" https://discord.gg/knJgyn936s \"");
                awObjNotFound = true;
                objHasError = true;
                yield break;
            }

            string trimmedResult = www.downloadHandler.text;
            trimmedResult = trimmedResult.TrimStart('[');
            trimmedResult = trimmedResult.TrimEnd(']');
            string objectJsonString = trimmedResult;


            try
            {
                requestedObject = JsonUtility.FromJson<AWThing>(objectJsonString);
            }
            catch (Exception e)
            {
                Debug.LogError($"{objName} JSON does not match AWThing container" + "\n" + e.ToString());
                //Debug.LogError($"Error loading response for {objName} into AWThing.");
                objHasError = true;
                yield break;
            }

            creatorAttribution = objName + " by " + requestedObject.author;
            if (requestedObject.behaviour == "static")
            {
                addDefaultBehaviour = false;
            }

            if (this == null)
            {
                objHasError = true;
                DestroyImmediate(awThing);
                yield break;
            }


            if (hasBehaviour)
            {
                behaviourController = gameObject.AddComponent<AWBehaviourController>();
                behaviourController.awObj = this;
            }
            CategoryMap.SetCategory(requestedObject.type, this.objName);
            BehaviourMap.SetBehaviour(requestedObject.behaviour, this.objName);
            BuildAbout();
        }


        /// <summary>
        /// Load material from JSON request.
        /// </summary>
        /// <returns>Coroutine</returns>
        private IEnumerator LoadMaterial()
        {
            string matLocation = "";
            matLocation = _materialName;
            UnityWebRequest www = UnityWebRequest.Get(matLocation);
            yield return www.SendWebRequest();
            if (CheckWebRequest.IsError(www))
            {
                Debug.LogWarning($"Error requesting material from material URL. \n URL: \"{matLocation}\"");
                ThrowMakerError($"Error loading material", $"Could not load material for {objName}");
                yield break;
            }
            _rawMaterial = www.downloadHandler.text;
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(LoadTexture(), this);
#endif
            }
            else
            {
                StartCoroutine(LoadTexture());
            }
        }

        /// <summary>
        /// Loads texture from JSON request.
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadTexture()
        {
            if (textureAddresses != null && textureAddresses.Length > 0)
            {
                List<Texture> textureList = new List<Texture>();
                int textureInx = 0;
                foreach (string textureAddress in textureAddresses)
                {
                    UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(textureAddress);
                    yield return webRequest.SendWebRequest();
                    string textureName = GetTextureName(textureAddress);
                    textureName = System.Uri.UnescapeDataString(textureName);


                    //Debug.Log($"Test texture name: {testTextureName}");
                    try
                    {
                        if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Loaded texture file called " + textureName);


                        if (CheckWebRequest.IsError(webRequest))
                        {
                            PrintAWDebugWarning($"Error loading texture on {objName}", $"Could not load texture file {textureName} on {objName}", textureAddress);
                            _objTextures.Add(textureName, null);
                            continue;
                        }
                        else
                        {
                            DownloadHandlerTexture textureHandler = new DownloadHandlerTexture();
                            Texture tex = DownloadHandlerTexture.GetContent(webRequest);
                            if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Loading texture: " + textureName);
                            _objTextures.Add(textureName, tex);
                            textureList.Add(tex);
                        }
                        textureInx++;
                    }
                    catch (MissingReferenceException e)
                    {
                        Debug.Log("Missing reference exception thrown while loading textures: " + e);
                        objHasError = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        Debug.Log("Other exception thrown: " + e);
                    }
                }
                textureArray = textureList.ToArray();
            }
            GetPrefabMap();
            yield return null;
        }


        internal string GetTextureName(string textureAddress)
        {
            try
            {
                if (textureAddress.Contains("?"))
                {
                    string[] splitAddress = textureAddress.Split('?');
                    string addressWithTexName = splitAddress[0];
                    string[] splitAddressWithTexName = addressWithTexName.Split('/');

                    string textureName = splitAddressWithTexName[splitAddressWithTexName.Length - 1];
                    return textureName;
                }
                else
                {
                    Debug.LogError($"No name parameter detected in texture URL: {textureAddress}");
                    return null;
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error: trying to load texture name from texture address:{textureAddress}");
                Debug.LogException(e);
                return null;
            }


        }

        /// <summary>
        /// Loads pref from category and behaviour.
        /// </summary>
        /// <remarks>
        /// Adds non-kinematic rigidbodies.
        /// Gets joints in prefab and sets to _prefabJoints.
        /// Gets colliders in prefab and sets to _prefabColliders.
        /// Gets AWBehaviours and sets to _awBehaviours.
        /// </remarks>
        private void GetPrefabMap()
        {
            try
            {
                GameObject prefabObj = PrefabMap.ThingPrefab(objectCategory, behaviour);
                //prefabObj.name = "PrefabObj";
                mappedPrefab = Instantiate(prefabObj) as GameObject;
                //_mappedPrefab.name = "MappedPrefab";
                prefabRigidbodies = new List<Rigidbody>();
                Rigidbody[] allRBs = mappedPrefab.GetComponentsInChildren<Rigidbody>();
                foreach (Rigidbody rb in allRBs)
                {
                    if (!rb.isKinematic)
                    {
                        prefabRigidbodies.Add(rb);
                    }
                }
                prefabColliders = mappedPrefab.GetComponentsInChildren<Collider>();
                _prefabJoints = mappedPrefab.GetComponentsInChildren<Joint>();
                awBehaviours = mappedPrefab.GetComponentsInChildren<AWBehaviour>();
                ActivateRBs(false);
                if (this != null && mappedPrefab != null)
                {
                    mappedPrefab.transform.parent = transform;
                }
                else
                {
                    if (AnythingSettings.DebugEnabled) Debug.LogWarning("Mapped prefab destroyed, exiting make process");
                    objHasError = true;
                    return;
                }


            }
            catch (MissingReferenceException e)
            {
                Debug.LogException(e);
                if (AnythingSettings.DebugEnabled) Debug.LogWarning("Missing reference Exception while getting prefab map: " + e);
                AnythingSafeDestroy.SafeDestroy(mappedPrefab);
                objHasError = true;
                return;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Exception while getting prefab map: " + e);
                AnythingSafeDestroy.SafeDestroy(mappedPrefab);
                objHasError = true;
                return;
            }


            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(LoadOBJParts(), this);
#endif
            }
            else
            {
                StartCoroutine(LoadOBJParts());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadOBJParts()
        {
            MTLLoader materialLoader = new MTLLoader();
            Dictionary<string, Material> objectMaterial = new Dictionary<string, Material>();
            MemoryStream matStream = new MemoryStream(Encoding.UTF8.GetBytes(_rawMaterial));
            if (matStream != null)
            {
                try
                {
                    objectMaterial = materialLoader.Load(matStream, _objTextures);
                }
                catch
                {
                    Debug.LogError("Error loading material stream.");
                }

            }

            //For each "part" in the "parts" section of the JSON
            foreach (KeyValuePair<string, string> kvp in _objParts)
            {
                string partObjFileAddress = kvp.Value;
                string partName = kvp.Key;
                //Request obj of part using part address.
                UnityWebRequest www = UnityWebRequest.Get(partObjFileAddress);
                yield return www.SendWebRequest();
                

                if (CheckWebRequest.IsError(www))
                {
                    ThrowMakerError("Error loading OBJ part", $"Could not load OBJ for part \"{partName}\"");
                    PrintAWDebugWarning(objError.code, objError.message, $"URL: {partObjFileAddress}");
                    yield break;
                }

                MeshRenderer mRenderer;
                GameObject loadedObj = null;
                try
                {

                    if (this == null)
                    {
                        objHasError = true;
                        yield break;
                    }
                    MemoryStream textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.downloadHandler.text));
                    OBJLoader loader = new OBJLoader();
                    loadedObj = loader.Load(textStream, objectMaterial);
                    loadedObj.name = "LoadedObj";
                    if (mappedPrefab.transform.FindDeepChild(partName))
                    {
                        loadedObj.transform.parent = mappedPrefab.transform.FindDeepChild(partName);
                    }
                    if (this == null)
                    {
                        objHasError = true;
                        yield break;
                    }
                    mRenderer = loadedObj.GetComponentInChildren(typeof(MeshRenderer)) as MeshRenderer;
                    if (mRenderer == null)
                    {
                        PrintAWDebugWarning($"Error loading {objName}", "Mesh renderer not present on loaded object.");
                        continue;
                    }
                    GameObject rendererGO = mRenderer.gameObject;
                    if (this == null)
                    {
                        objHasError = true;
                        yield break;
                    }
                    mRenderer.enabled = false;
                    objRendererList.Add(mRenderer.GetComponent<Renderer>());
                    objMeshList.Add(mRenderer.GetComponent<MeshFilter>());
                    objLoadedObjs.Add(loadedObj);
                }
                catch (NullReferenceException e)
                {
                    Debug.LogError("Null reference exception thrown when loading OBJ parts: " + e);
                    AnythingSafeDestroy.SafeDestroy(loadedObj);
                    objHasError = true;
                    yield break;
                }
                catch (MissingReferenceException e)
                {
                    Debug.LogError("Missing reference exception thrown while loading OBJ parts: " + e);
                    AnythingSafeDestroy.SafeDestroy(loadedObj);
                    objHasError = true;
                    yield break;
                }


                switch (behaviour)
                {
                    case "swim":
                        Shader swimShader = _fishWobbleShader;
                        // TODO: another way, large swimmable category?
                        if (objName.IndexOf("whale") != -1)
                            swimShader = _fishBigWobbleShader;
                        SwitchShaders(mRenderer, swimShader);
                        break;

                    case "wriggle":
                        SwitchShaders(mRenderer, _uniformWriggleShader);
                        break;

                    case "swim2":
                        SwitchShaders(mRenderer, _fishWriggleShader);
                        break;

                    case "swim3":
                        SwitchShaders(mRenderer, _fishWriggleBigShader);
                        break;

                    case "crawl":
                        SwitchShaders(mRenderer, _quadrupedCrawlShader);
                        break;

                    case "slither":
                        SwitchShaders(mRenderer, _uniformSlitherShader);
                        break;

                    case "slithervertical":
                        SwitchShaders(mRenderer, _uniformSlitherVerticalShader);
                        break;
                }

                _objIndex++;
                yield return null;
            }

            try
            {
                CenterAndSizeObjectToBounds();
                StartCoroutine(MarkObjectAsDone(0f));
            }
            catch (Exception e)
            {
                Debug.LogWarning("Exception thrown while finishing object: " + e.Message);
                objHasError = true;
                yield break;
            }

        }

        private IEnumerator MarkObjectAsDone(float doneDelay = 0.1f)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(doneDelay);
#endif
            }
            else
            {
                yield return new WaitForSeconds(doneDelay);
            }

            if (!isPolyObject)
            {
                ActivateRBs(true);
            }

            objMade = true;
            onObjectMadeSuccessfullyDelegate?.Invoke();

            if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Object {objName} marked as made");

            if (AnythingSettings.DebugEnabled) Debug.Log($"Adding creator attribution: {creatorAttribution}");
            AnythingSetup.Instance.AddModelAttribution(creatorAttribution);



            if (addDefaultBehaviour)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    EditorCoroutineUtility.StartCoroutineOwnerless(AddDefaultBehaviour());
#endif
                }
                else
                {
                    StartCoroutine(AddDefaultBehaviour());
                }
            }

            MakeRenderersVisible();

        }

        #endregion Request Object Pipeline

        #region Object Utils

        private void ThrowMakerError(string title, string message)
        {
            this.StopAllCoroutines();
            objHasError = true;
            objError = new ErrorResponse
            {
                code = title,
                message = message
            };
        }
        private void PrintAWDebugWarning(string title, string message, string url = "")
        {
            if (AnythingSettings.DebugEnabled)
            {
                Debug.LogWarning($"{title}: {message} \n {url}");
            }
        }

        /// <summary>
        /// Converts string to title case.
        /// </summary>
        /// <param name="stringToConvert">String to convert.</param>
        /// <returns>String with titlecase.</returns>
        private string ToTitleCase(string stringToConvert)
        {
            string firstChar = stringToConvert[0].ToString();
            return (stringToConvert.Length > 0 ? firstChar.ToUpper() + stringToConvert.Substring(1) : stringToConvert);
        }

        /// <summary>
        /// Converts string in format "1.23m" to float.
        /// </summary>
        /// <param name="dimension">String to convert.</param>
        /// <returns>float</returns>
        private float ParseDimension(string dimension)
        {
            string trimmedDim = dimension.Replace("m", "");

            float floatDim = 1f;
            float pFloat;

            if (float.TryParse(trimmedDim, out pFloat))
            {
                floatDim = pFloat;
            }

            return floatDim;
        }

        /// <summary>
        /// Creates dictionary of parts from the JSON request.
        /// </summary>
        private void BuildAbout()
        {
            objectCategory = CategoryMap.GetCategory(objName);

            prefabName = PrefabMap.PrefabName(objectCategory, behaviour);
            // get common elements
            AWThingLook awThingLook = requestedObject.model.other;
            _materialName = awThingLook.material;
            if (_textureName != null && _textureName.Length > 0)
            {
                _textureName = awThingLook.texture[0];
            }
            else
            {
                _textureName = null;
            }

            if (awThingLook.texture != null && awThingLook.texture.Length > 0)
            {
                textureAddresses = awThingLook.texture;
            }
            else
            {
                textureAddresses = null;
            }

            behaviour = requestedObject.behaviour;
            categoryBehaviourName = PrefabMap.PrefabName(objectCategory, behaviour);
            _objectScale = new Vector2(ParseDimension(requestedObject.scale.length), ParseDimension(requestedObject.scale.height));

            AWParts awThingParts = requestedObject.model.parts;

            string JSONRep = JsonUtility.ToJson(requestedObject.model.parts);

            switch (objectCategory)
            {
                case "multileg_flyer":
                    AWMultilegFlyerParts awMultilegFlyerParts = JsonUtility.FromJson<AWMultilegFlyerParts>(JSONRep);
                    BuildObjParts(awMultilegFlyerParts);
                    break;

                case "multileg_crawler":
                    AWMultilegCrawlerParts awMultilegCrawlerParts = JsonUtility.FromJson<AWMultilegCrawlerParts>(JSONRep);
                    BuildObjParts(awMultilegCrawlerParts);
                    break;

                case "multileg_crawler_big":
                    AWMultilegCrawlerParts awMultilegCrawlerBigParts = JsonUtility.FromJson<AWMultilegCrawlerParts>(JSONRep);
                    BuildObjParts(awMultilegCrawlerBigParts);
                    break;

                case "multileg_crawler_eight":
                    AWMultilegCrawlerEightParts awMultilegCrawlerEightParts = JsonUtility.FromJson<AWMultilegCrawlerEightParts>(JSONRep);
                    BuildObjParts(awMultilegCrawlerEightParts);
                    break;

                case "winged_standing":
                    AWWingedStandingParts awWingedStandingParts = JsonUtility.FromJson<AWWingedStandingParts>(JSONRep);
                    BuildObjParts(awWingedStandingParts);
                    break;

                case "winged_standing_small":
                    AWWingedStandingParts awWingedStandingSmallParts = JsonUtility.FromJson<AWWingedStandingParts>(JSONRep);
                    BuildObjParts(awWingedStandingSmallParts);
                    break;

                case "winged_flyer":
                    AWWingedFlyerParts awWingedFlyerParts = JsonUtility.FromJson<AWWingedFlyerParts>(JSONRep);
                    BuildObjParts(awWingedFlyerParts);
                    break;

                case "quadruped_standard":
                case "quadruped":
                    AWQuadParts awQuadParts = JsonUtility.FromJson<AWQuadParts>(JSONRep);
                    BuildObjParts(awQuadParts);
                    break;

                case "quadruped_ungulate":
                    AWQuadParts awQuadUngulateParts = JsonUtility.FromJson<AWQuadParts>(JSONRep);
                    BuildObjParts(awQuadUngulateParts);
                    break;

                case "quadruped_fat":
                    AWQuadFatParts awQuadFatParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                    BuildObjParts(awQuadFatParts);
                    break;

                case "quadruped_fat_small_generic":
                    AWQuadFatParts awQuadFatSmallParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                    BuildObjParts(awQuadFatSmallParts);
                    break;

                case "quadruped_fat_shortleg_generic":
                    AWQuadFatParts awQuadFatShortLegParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                    BuildObjParts(awQuadFatShortLegParts);
                    break;

                case "hopper":
                    AWHopperParts awHopperParts = JsonUtility.FromJson<AWHopperParts>(JSONRep);
                    BuildObjParts(awHopperParts);
                    break;

                case "vehicle_four_wheel":
                    AWVehicleFourWheelParts awVehicleFourParts = JsonUtility.FromJson<AWVehicleFourWheelParts>(JSONRep);
                    BuildObjParts(awVehicleFourParts);
                    break;

                case "vehicle_three_wheel":
                    AWVehicleThreeWheelParts awVehicleThreeParts = JsonUtility.FromJson<AWVehicleThreeWheelParts>(JSONRep);
                    BuildObjParts(awVehicleThreeParts);
                    break;

                case "vehicle_two_wheel":
                    AWVehicleTwoWheelParts awVehicleTwoParts = JsonUtility.FromJson<AWVehicleTwoWheelParts>(JSONRep);
                    BuildObjParts(awVehicleTwoParts);
                    break;

                case "vehicle_one_wheel":
                    AWVehicleOneWheelParts awVehicleOneParts = JsonUtility.FromJson<AWVehicleOneWheelParts>(JSONRep);
                    BuildObjParts(awVehicleOneParts);
                    break;

                case "vehicle_load":
                    AWVehicleLoadParts awVehicleLoadParts = JsonUtility.FromJson<AWVehicleLoadParts>(JSONRep);
                    BuildObjParts(awVehicleLoadParts);
                    break;

                case "vehicle_flyer":
                    AWVehicleFlyerParts awVehicleFlyerParts = JsonUtility.FromJson<AWVehicleFlyerParts>(JSONRep);
                    BuildObjParts(awVehicleFlyerParts);
                    break;

                case "vehicle_propeller":
                    AWVehiclePropellerParts awVehiclePropellerParts = JsonUtility.FromJson<AWVehiclePropellerParts>(JSONRep);
                    BuildObjParts(awVehiclePropellerParts);
                    break;

                case "biped":
                    AWBipedParts awBipedParts = JsonUtility.FromJson<AWBipedParts>(JSONRep);
                    BuildObjParts(awBipedParts);
                    break;

                case "uniform":
                case "vehicle_other":
                case "vehicle_uniform":
                    AWUniformParts awUniformParts = JsonUtility.FromJson<AWUniformParts>(JSONRep);
                    BuildObjParts(awUniformParts);
                    break;

                case "floater":
                    AWFloaterParts awFloaterParts = JsonUtility.FromJson<AWFloaterParts>(JSONRep);
                    BuildObjParts(awFloaterParts);
                    break;

                case "scenery":
                    AWSceneryParts aWSceneryParts = JsonUtility.FromJson<AWSceneryParts>(JSONRep);
                    BuildObjParts(aWSceneryParts);
                    break;

                default:
                    Debug.LogError("NO CATEGORY FOUND FOR OBJECT -> " + objectCategory + " -> ABORTING!");
                    break;
            }

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(LoadMaterial(), this);
#endif
            }
            else
            {
                StartCoroutine(LoadMaterial());
            }
        }

        /// <summary>
        /// Builds list of parts from AWParts parts class.
        /// </summary>
        /// <param name="parts">AWParts parts class.</param>
        private void BuildObjParts(AWParts parts)
        {
            Type type = parts.GetType();

            FieldInfo[] properties = type.GetFields();
            foreach (FieldInfo property in properties)
            {
                string propName = property.Name.ToString();
                string propValue = property.GetValue(parts).ToString();
                if (propValue.Length > 1)
                {
                    if (!_objParts.ContainsKey(propName))
                    {
                        _objParts.Add(propName, propValue);
                    }
                }
            }
        }

        /// <summary>
        /// Set all renderers to be enabled, showing the object as visible.
        /// </summary>
        private void MakeRenderersVisible()
        {
            foreach (MeshRenderer rend in objRendererList)
            {
                rend.enabled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sRenderer"></param>
        /// <param name="sShader"></param>
        private void SwitchShaders(Renderer sRenderer, Shader sShader)
        {
            Material[] _allMats = sRenderer.sharedMaterials;

            foreach (Material mat in _allMats)
            {
                mat.shader = sShader;
            }
        }

        private Vector3 GetObjectBounds()
        {
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
            foreach (Renderer objRenderer in objRendererList)
            {
                // Debug.Log("Object bounds: " + objRenderer.bounds);
                bounds.Encapsulate(objRenderer.bounds);
            }
            return bounds.size;
        }

        private Vector3 GetObjectOrigin()
        {
            Bounds totalBounds = new Bounds(Vector3.zero, Vector3.zero);
            // GameObject boundsCenterDebug;
            Mesh mMesh;
            foreach (MeshFilter mFilter in objMeshList)
            {
                mMesh = mFilter.sharedMesh;
                totalBounds.Encapsulate(mMesh.bounds);
                // Debug.Log("have bounds object : " + mMesh.bounds);
            }


            if (SHOW_DEBUG_BOXES)
            {
                boundsShow = GameObject.CreatePrimitive(PrimitiveType.Cube);
                boundsShow.GetComponent<BoxCollider>().enabled = false;
                boundsShow.transform.localScale = totalBounds.size;
                boundsShow.transform.position = totalBounds.center;
                boundsShow.transform.parent = transform;
                boundsShow.GetComponent<Renderer>().material = Resources.Load("Materials/DebugMaterial") as Material;
            }

            boundsYOffset = totalBounds.extents.y;

            return totalBounds.center;
        }

        private float GetObjectRelativeScale()
        {
            float maxDimension = _objectScale.x > _objectScale.y ? _objectScale.x : _objectScale.y;
            // default to 1 in case no scale value found
            if (maxDimension == 0)
            {
                maxDimension = 1;
            }
            else
            {
                // TODO: we're resizing here, revisit!
                float sqrDimension = (float)Math.Sqrt(maxDimension);
                float divDimension = (sqrDimension) / (sqrDimension / 1.5f);
                maxDimension /= divDimension;
            }

            // Debug.Log("maxDimension = " + maxDimension);

            return maxDimension;
        }

        private void CenterAndSizeObjectToBounds()
        {
            Vector3 objectBounds = GetObjectBounds();
            Vector3 objectOrigin = GetObjectOrigin();

            if (objectBounds == Vector3.zero)
            {
                objHasError = true;
                throw new Exception($"Object {objName} has zero bounds, cancelling creation.");
            }


            float max = objectBounds.x;
            if (max < objectBounds.y)
                max = objectBounds.y;
            if (max < objectBounds.z)
                max = objectBounds.z;

            objectScale = Mathf.Clamp(GetObjectRelativeScale(), 0.01f, 5f);


            float boundsScale = (objectScale * 10f) / max;

            if(float.IsNaN(boundsScale) || float.IsPositiveInfinity(boundsScale) || float.IsNegativeInfinity(boundsScale))
            {
                Debug.LogError($"Error while calculating bounds scale for {objName}, calculated bound was {boundsScale.ToString()}");
                objHasError = true;
                return;
            }

            boundsYOffset *= boundsScale;

            if (SHOW_DEBUG_BOXES)
            {
                Vector3 currBoundsScale = boundsShow.transform.localScale;
                currBoundsScale *= boundsScale;
                boundsShow.transform.localScale = currBoundsScale;
                boundsShow.transform.position -= objectOrigin;
            }

            foreach (GameObject loadedObj in objLoadedObjs)
            {
                loadedObj.transform.localScale = new Vector3(boundsScale, boundsScale, boundsScale);
                loadedObj.transform.position -= objectOrigin * boundsScale;
            }
        }

        private void GatherRBs()
        {
            prefabRigidbodies = new List<Rigidbody>();
            Rigidbody[] allRBs = gameObject.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rb in allRBs)
            {
                if (!rb.isKinematic)
                {
                    prefabRigidbodies.Add(rb);
                }
            }

            prefabColliders = gameObject.GetComponentsInChildren<Collider>();
            _prefabJoints = gameObject.GetComponentsInChildren<Joint>();
            awBehaviours = gameObject.GetComponentsInChildren<AWBehaviour>();

            Transform[] AWtransforms = gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform t in AWtransforms)
            {
                if (t != this.transform && t.tag != "AWThing")
                {
                    t.localPosition = Vector3.zero;
                }
            }
        }


        #endregion Object Utils

        #region Behaviour Handling

        /// <summary>
        /// Adds behaviour once has finished being made.
        /// </summary>
        /// <param name="AWbehaviourName">String name of AWBehaviour script to be added.</param>
        public void AddBehaviourAsync(System.Type behaviourClassType)
        {
            if (behaviourObject == null) { BuildBehaviourContainer(); }
            try
            {
                AWBehaviour behaviour = (AWBehaviour)behaviourObject.AddComponent(behaviourClassType);
                behaviour.enabled = false;
                behaviour.AWObjController = this;
                addDefaultBehaviour = false;


                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    EditorCoroutineUtility.StartCoroutine(ActivateBehaviourWhenMade(behaviour), this);
#endif
                }
                else
                {
                    StartCoroutine(ActivateBehaviourWhenMade(behaviour));
                }

            }
            catch (Exception e)
            {
                Debug.LogError($"Exception thrown while adding {behaviourClassType.ToString()}: {e.Message}");
            }

            onBehaviourRefreshMethod.Invoke();

        }

        public T AddBehaviour<T>(bool ClearExistingBehaviours = true) where T : AWBehaviour
        {
            List<AWBehaviour> existingBehaviours = new List<AWBehaviour>();
            T behaviour = null;

            if (behaviourObject == null)
            {
                BuildBehaviourContainer();
                behaviour = behaviourObject.AddComponent<T>();
                behaviour.AWObjController = this;
                behaviour.enabled = false;
                addDefaultBehaviour = false;

                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    EditorCoroutineUtility.StartCoroutine(ActivateBehaviourWhenMade<T>(behaviour), this);
#endif
                }
                else
                {
                    StartCoroutine(ActivateBehaviourWhenMade(behaviour));
                }
            }
            else
            {
                if (ClearExistingBehaviours)
                {
                    existingBehaviours = GetExistingBehaviours();
                }

                if (gameObject.TryGetComponent<T>(out T temp))
                {
                    behaviour = temp;
                    existingBehaviours.Remove(temp);
                }
                else
                {
                    behaviour = behaviourObject.AddComponent<T>();
                    behaviour.AWObjController = this;
                    behaviour.enabled = false;
                    addDefaultBehaviour = false;

                    if (Application.isEditor && !Application.isPlaying)
                    {
#if UNITY_EDITOR
                        EditorCoroutineUtility.StartCoroutineOwnerless(ActivateBehaviourWhenMade<T>(behaviour));
#endif
                    }
                    else
                    {
                        StartCoroutine(ActivateBehaviourWhenMade<T>(behaviour));
                    }
                }
                if (ClearExistingBehaviours)
                {
                    DisableExistingBehaviours(existingBehaviours);
                }
            }
            try
            {
                onBehaviourRefreshMethod.Invoke();
            }
            catch
            {
                if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Failed to invoke behaviour refresh delegate when adding behaviour.");
            }

            return behaviour;

        }

        /// <summary>
        /// Adds default behaviour once OBJ spawned.
        /// </summary>
        private IEnumerator AddDefaultBehaviour()
        {
            //Debug.Log("AWObj.AddDefaultBehaviour: " + gameObject.name);
            if (addDefaultBehaviour)
            {
                if (behaviourObject == null)
                {
                    BuildBehaviourContainer();
                }
                while (!objMade)
                {
                    if (Application.isEditor && !Application.isPlaying)
                    {
#if UNITY_EDITOR
                        yield return new EditorWaitForSeconds(1);
#endif
                    }
                    else
                    {
                        yield return new WaitForSeconds(1);
                    }
                }

                bool isGroupMember = GroupMap.ThingHasAGroup(behaviour, objectCategory);
                // don't add defualt behvaiours to group members for now as these work differently - GM
                if (hasBehaviour && !isGroupMember)
                {
                    // Debug.Log($"_categoryBehaviourName = {_categoryBehaviourName}");
                    // _behaviour alone cannot always determine default behaviour, so _categoryBehaviourName checks first - GM
                    if (categoryBehaviourName == "vehicle_uniform__drive" || categoryBehaviourName == "vehicle_uniform")
                    {
                        AddBehaviour<RandomMovement>();
                    }
                    else
                    {
                        if (behaviour == "drive") AddBehaviour<VehicleDriveMovement>();
                        else if (objectCategory == "hopper" || behaviour == "hop") AddBehaviour<HoppingMovement>();
                        else if (behaviour == "fly") AddBehaviour<VehicleFlyerRandomMovement>();
                        else AddBehaviour<RandomMovement>();
                    }
                }
            }
        }

        /// <summary>
        /// Adds behaviour to AWObj without removing other behaviours.
        /// </summary>
        /// <param name="behaviourName">string name of behaviour script</param>
        /// <returns></returns>
        private IEnumerator AddBehaviourIfMade(string behaviourName)
        {
            addDefaultBehaviour = false;
            while (!objMade)
            {
                yield return new WaitForSeconds(1);
            }
            if (behaviourController != null)
            {
                behaviourController.AddBehaviourScript(behaviourName);
            }
            else
            {
                Debug.LogError("No controller added, cannot add behaviour " + behaviourName + " to " + gameObject.name);
            }
        }

        private IEnumerator AddBehaviourIfMade<T>(T behaviourReference) where T : AWBehaviour
        {
            addDefaultBehaviour = false;
            while (!objMade)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(1);
#endif
                }
                else
                {
                    yield return new WaitForSeconds(1);
                }
            }

            if (behaviourController != null)
            {
                behaviourReference = (T)behaviourController.AddBehaviourScript<T>();
            }
            else
            {
                Debug.LogError("No controller added, cannot add behaviour" + typeof(T).ToString() + " to " + gameObject.name); ;
            }
            yield return behaviourReference;
        }

        private IEnumerator ActivateBehaviourWhenMade<T>(T _behaviourRef) where T : AWBehaviour
        {
            //Debug.Log("AWObj.ActivateBehaviourWhenMade: " + gameObject.name);
            while (!objMade)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(1);
#endif
                }
                else
                {
                    yield return new WaitForSeconds(1);
                }
            }
            _behaviourRef.enabled = true;
            _behaviourRef.ReadyToGo = true;
            _behaviourRef.InitializeBehaviour();
        }

        private IEnumerator ActivateBehaviourWhenMade(AWBehaviour behaviourReference)
        {
            while (!objMade)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(1);
#endif
                }
                else
                {
                    yield return new WaitForSeconds(1);
                }
            }
            behaviourReference.ReadyToGo = true;
            behaviourReference.enabled = true;
            behaviourReference.InitializeBehaviour();
            behaviourReference.AWObjController = this;
        }

        /// <summary>
        /// Removes other behaviours and adds input behaviour only.
        /// </summary>
        /// <param name="behaviourName">string name of behaviour script.</param>
        /// <returns></returns>
        private IEnumerator SetBehaviourIfMade(string behaviourName)
        {
            while (!objMade)
            {
                yield return new WaitForSeconds(1);
            }
            behaviourController.RemoveActiveBehaviours();
            behaviourController.AddBehaviourScript(behaviourName);
        }

        public void RemoveExistingBehaviours()
        {
            if (behaviourObject != null)
            {
                foreach (var behaviour in behaviourObject.GetComponents<AWBehaviour>())
                {
                    behaviour.RemoveAWAnimator();
                    AnythingSafeDestroy.SafeDestroy(behaviour, false);
                }
            }
        }

        private List<AWBehaviour> GetExistingBehaviours()
        {
            List<AWBehaviour> behaviourList = new List<AWBehaviour>();
            if (behaviourObject != null)
            {
                foreach (var behaviour in behaviourObject.GetComponents<AWBehaviour>())
                {
                    behaviourList.Add(behaviour);
                }
            }
            return behaviourList;
        }

        private void DisableExistingBehaviours(List<AWBehaviour> behavioursToDisable)
        {
            if (behaviourObject != null)
            {
                foreach (var behaviour in behavioursToDisable)
                {
                    behaviour.enabled = false;
                }
            }
        }

        private void BuildBehaviourContainer()
        {
            behaviourObject = new GameObject();
            behaviourObject.name = "Behaviours";
            behaviourObject.transform.parent = transform;
        }
        private void OnDestroy()
        {
            StopAllCoroutines();
            AnythingSafeDestroy.ClearList(objLoadedObjs);
        }
        #endregion Behaviour Handling
    }
}

