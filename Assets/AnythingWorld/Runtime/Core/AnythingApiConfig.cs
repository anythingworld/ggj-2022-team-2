using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnythingApiConfig
{
    public static string ApiUrlStem
    {
        get
        {
            return AW_API_STEM;
        }
    }
    private const string AW_API_STEM = "https://anything-world-api-production-2hs6k2d56a-nw.a.run.app";
}
