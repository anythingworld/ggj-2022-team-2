﻿

#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using System.Linq;
using UnityEngine.Networking;
using AnythingWorld.Utilities;
using AnythingWorld.Animation;
using AnythingWorld.DataContainers;

namespace AnythingWorld
{
    [ExecuteAlways, Serializable]
    /// <summary>
    /// Script containing the main functionality for creating and managing Anything World objects.
    /// </summary>
    public class AnythingCreator : AnythingBase
    {
        #region Made Things Dict
        public Dictionary<string, List<GameObject>> madeThings = null;
        public Dictionary<string, List<GameObject>> MadeThings
        {
            get
            {
                return madeThings;
            }
        }
        #endregion

        #region Scene Ledger
        [SerializeField, HideInInspector]
        private SceneLedger sceneLedger = null;
        public SceneLedger SceneLedger
        {
            get
            {
                if (sceneLedger == null)
                {
                    sceneLedger = new SceneLedger();
                }
                return sceneLedger;
            }
        }
        #endregion

        public delegate void SearchCompleteDelegate(SearchResult[] thumbnailResults, AWThing[] jsonResults, bool failed = false);
        SearchCompleteDelegate searchDelegate;

        [SerializeField]
        public static List<GameObject> objectsInScene;

        public Queue anythingQueue;
#if UNITY_EDITOR
        private EditorCoroutine makeObjectEditorRoutine;
#endif
        private Coroutine makeObjectRoutine;
        private string behaviour;
        private GameObject groupControllerObject;
        private FlockManager groupController;
        private bool makeObjectRoutineRunning = false;
        public bool MakeObjectRoutineRunning
        {
            get
            {
                return makeObjectRoutineRunning;
            }
        }


        #region AnythingCreator Instance
        [SerializeField]
        private static AnythingCreator instance;
        public static AnythingCreator Instance
        {
            get
            {
                if (instance == null && CheckAWSetup())
                {
                    instance = GameObject.FindObjectOfType<AnythingCreator>();
                    if (instance != null)
                    {
                        instance.Init();
                        return instance;
                    }
                    else
                    {
                        GameObject anythingCreatorGO = new GameObject();
                        anythingCreatorGO.name = "Anything Creator";
                        AnythingCreator anythingCreator = anythingCreatorGO.AddComponent<AnythingCreator>();
                        instance = anythingCreator;
                        instance.Init();
                    }
                }
                return instance;
            }
        }
        #endregion

        public AnythingSettings AWSettings
        {
            get
            {
                if (AnythingSettings.Instance == null)
                {
                    AnythingSettings.CreateInstance<AnythingSettings>();
                }
                return AnythingSettings.Instance;
            }

        }

        #region Grid Instance
        [SerializeField, HideInInspector]
        private GridUtility layoutGrid = null;
        public GridUtility LayoutGrid
        {
            get
            {
                if (layoutGrid == null)
                {
                    layoutGrid = new GridUtility(10, 10);
#if UNITY_EDITOR
                    EditorApplication.playModeStateChanged -= layoutGrid.SerializeChanges;
                    EditorApplication.playModeStateChanged += layoutGrid.SerializeChanges;
#endif
                }
                return layoutGrid;
            }
            set
            {
                layoutGrid = value;
            }
        }
        #endregion
        public void Start()
        {
#if UNITY_EDITOR
            //This must be done or the delegate subscription will not persist through playmode and the seriaizatin will break.
            if (layoutGrid != null)
            {
                EditorApplication.playModeStateChanged -= layoutGrid.SerializeChanges;
                EditorApplication.playModeStateChanged += layoutGrid.SerializeChanges;
            }
#endif
        }
        
        public void Init()
        {
            madeThings = new Dictionary<string, List<GameObject>>();
            anythingQueue = new Queue();
            AnythingWorld.Utilities.MaterialCacheUtil.CacheAndGetMaterials();
        }

        #region Adjust and Clones
        public void AdjustGameObjects(string objName, int quantity)
        {
            ThingGroup thingGroup = SearchSceneLedger(objName);

            if (thingGroup != null)
            {
                thingGroup.Adjust(quantity);
            }


        }
        public void AdjustGameObjects(ThingGroup thingGroup, int quantity)
        {
            if (thingGroup != null)
            {
                thingGroup.Adjust(quantity);
            }
        }

        #endregion

        #region MakeObject

        public List<AWObj> MakeManyObjects(string objName, int quantity, bool hasBehaviour = true)
        {
            if (quantity < 0) return null;

            List<AWObj> madeObjects = new List<AWObj>();



            for (int i = 0; i < quantity; i++)
            {
                GameObject goRef = new GameObject();
                AWObj awRef = goRef.AddComponent<AWObj>();

                AddToQueue(objName.ToLower(), true, hasBehaviour, true, false, Vector3.zero, Quaternion.identity, Vector3.one, null, goRef);
                madeObjects.Add(awRef);
            }
            return madeObjects;
        }

        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// <example> For example:
        /// <code>
        /// AWObj catAWObj = AnythingCreator.Instance.MakeObject(cat);
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <returns>Reference to <c>AWObj</c> on the new object.</returns>
        public AWObj MakeObject(string objName, bool hasBehaviour = true, bool hasCollider = true)
        {
            // Game object is created ahead of asynchronous make process so that we can pass a reference to the AWObj back to the user at maketime.
            GameObject goRef = new GameObject();
            // AWObj cannot be instantiated 
            AWObj awRef = goRef.AddComponent<AWObj>();
            // Returns reference to caller.
            AddToQueue(objName.ToLower(), true, hasBehaviour, hasCollider, true, transform.localPosition, transform.localRotation, transform.localScale, null, goRef);
            return awRef;
        }


        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// <para>
        /// Takes a parent transform that the request object will be parented to.
        /// </para>
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="parentTransform">Transform to parent object to.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <param name="positionGlobally">Will object transform be applied globally or locally (Default: locally)</param>
        /// <returns>Reference to <c>AWObj</c> on the new object.</returns>
        public AWObj MakeObject(string objName, Transform parentTransform, bool hasBehaviour = true, bool hasCollider = true, bool positionGlobally = false)
        {
            GameObject goRef = new GameObject();
            // dummy transform to pass in zero values easily
            Transform transform = goRef.transform;
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, positionGlobally, transform.localPosition, transform.localRotation, transform.localScale, parentTransform, goRef);
            return awRef;
        }




        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// <para>
        /// Allows object to be parented to an object, and a given transform applied locally or globally.
        /// </para>
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="transform">Transform to be applied to object after creation.</param>
        /// <param name="parentTransform">Transform that new object will be parented to.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <param name="positionGlobally">Will object transform be applied globally or locally (Default: locally)</param>
        /// <returns>Reference to <c>AWObj</c> on the new object.</returns>
        public AWObj MakeObject(string objName, Transform transform, Transform parentTransform, bool hasBehaviour = true, bool hasCollider = true, bool positionGlobally = false)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, positionGlobally, transform.localPosition, transform.localRotation, transform.localScale, parentTransform, goRef);
            return awRef;
        }
        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="objectPos"><c>Vector3</c> position to be applied to new object.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <returns>Reference to <c>AWObj</c> on the new object.</returns>
        public AWObj MakeObject(string objName, Vector3 objectPos, bool hasBehaviour = true, bool hasCollider = true)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, false, objectPos, Quaternion.identity, Vector3.one, null, goRef);
            return awRef;
        }
        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="objectPos"><c>Vector3</c> position to be applied to new object.</param>
        /// <param name="objectRot"><c>Quaternion</c> rotation to be applied to new object.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <returns>Reference to <c>AWObj</c> on the new object.</returns>
        public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, bool hasBehaviour = true, bool hasCollider = true)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, false, objectPos, objectRot, Vector3.one, null, goRef);
            return awRef;
        }
        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="objectPos"><c>Vector3</c> position to be applied to new object.</param>
        /// <param name="objectRot"><c>Quaternion</c> rotation to be applied to new object.</param>
        /// <param name="objectScale"><c>Vector3</c> scale to be applied to new object.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <returns></returns>
        public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, bool hasBehaviour = true, bool hasCollider = true)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, false, objectPos, objectRot, objectScale, null, goRef);
            return awRef;
        }

        /// <summary>
        /// Adds an object of name <c>objName</c> to the AnythingCreator make queue.
        /// </summary>
        /// <param name="objName">Name of object to be requested from server.</param>
        /// <param name="objectPos"><c>Vector3</c> position to be applied to new object.</param>
        /// <param name="objectRot"><c>Quaternion</c> rotation to be applied to new object.</param>
        /// <param name="objectScale"><c>Vector3</c> scale to be applied to new object.</param>
        /// <param name="parentTransform">Transform that new object will be parented to.</param>
        /// <param name="hasBehaviour">Will object recieve default behaviour and animations.</param>
        /// <param name="hasCollider">Will object recieve colliders.</param>
        /// <param name="positionGlobally">Will object transform be applied globally or locally (Default: locally)</param>
        /// <returns></returns>
        public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, Transform parentTransform, bool hasBehaviour = true, bool hasCollider = true, bool positionGlobally = false)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();
            AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, positionGlobally, objectPos, objectRot, objectScale, parentTransform, goRef);
            return awRef;
        }


        #endregion

        Coroutine categorySearchCoroutine = null;
        #region Search
        public void RequestCategorySearchResults(string searchTerm, SearchCompleteDelegate delegateFunc)
        {
            try
            {
                categorySearchCoroutine = StartCoroutine(CategorySearch(searchTerm, delegateFunc));
            }
            catch (Exception e)
            {
                List<SearchResult> emptyList = new List<SearchResult>();
                searchDelegate += delegateFunc;
                searchDelegate(null, null);
                searchDelegate -= delegateFunc;

                Debug.LogError("Exception thrown while attempting category search");
                Debug.LogError(e);
            }

        }

        public void CancelCategorySearchCoroutine()
        {
            if (categorySearchCoroutine != null)
            {
                StopCoroutine(categorySearchCoroutine);
            }
        }

        /// <summary>
        /// IEnumerator <c>CategorySearch</c> requests a list of search results and generates a list of <see cref="SearchResult"/> objects.
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="delegateFunc">Function to be called once search results have loaded.</param>
        /// <returns></returns>
        public IEnumerator CategorySearch(string searchTerm, SearchCompleteDelegate delegateFunc)
        {
            if (searchTerm == "")
            {
                List<SearchResult> emptyList = new List<SearchResult>();
                searchDelegate += delegateFunc;
                searchDelegate(emptyList.ToArray(), null);
                searchDelegate -= delegateFunc;
                yield break;
            }

            searchDelegate += delegateFunc;

            #region Get Result JSON Array
            string api_key = AnythingSettings.Instance.apiKey;
            string appName = AnythingSettings.Instance.appName;

            if (String.IsNullOrEmpty(appName))
            {
                appName = AnythingSettings.Instance.appName = "My App";
            }
            if (String.IsNullOrEmpty(api_key))
            {
                Debug.LogError("Please enter an API Key in AnythingSettings!");
                yield break;
            }

            // Trim our request string
            char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
            searchTerm = searchTerm.Trim(charsToTrim);
            // lowercase our request string
            searchTerm = searchTerm.ToLower();
            // Make API call string
            string APICall = AnythingApiConfig.ApiUrlStem + "/anything?key=" + AnythingSettings.ApiKey + "&search=" + searchTerm;
            if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Requesting search results for \"{searchTerm}\". \n URL: \"{APICall}\"");


            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            UnityWebRequest www = UnityWebRequest.Get(APICall);
            yield return www.SendWebRequest();



            if (CheckWebRequest.IsError(www))
            {
                //Debug.LogError(www.downloadHandler.text);
                string errorResponseString = www.downloadHandler.text;

                //Debug.Log(www.downloadHandler.error);

                //Debug.Log(www.error);

                ErrorResponse errorResponse = null;
                try
                {
                    errorResponse = JsonUtility.FromJson<ErrorResponse>(errorResponseString);
                }
                catch
                {
                    searchDelegate += delegateFunc;
                    searchDelegate(null, null);
                    searchDelegate -= delegateFunc;
                    Debug.LogWarning($"Unexpected error returned from server: \n {errorResponseString}");
                    yield break;
                }

                if (errorResponse.code == "Model not found")
                {
                    searchDelegate += delegateFunc;
                    searchDelegate(new SearchResult[0], null);
                    searchDelegate -= delegateFunc;
                    if (AnythingSettings.DebugEnabled) Debug.LogWarning($"{errorResponse.code}: {errorResponse.message}");
                    yield break;
                }
                else
                {
                    searchDelegate += delegateFunc;
                    searchDelegate(new SearchResult[0], null);
                    searchDelegate -= delegateFunc;
                    Debug.LogWarning($"{errorResponse.code}: {errorResponse.message}");
                    yield break;
                }

            }



            //Convert response to json format
            string result = www.downloadHandler.text;
            result = FixJson(result);



            if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Response returned for for \"{searchTerm}\". \n {result.Substring(0, 500)}...");
            string text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");
            List<SearchResult> searchResultList = new List<SearchResult>();

            AWThing[] resultsArray;
            try
            {
                resultsArray = JSONHelper.FromJson<AWThing>(result);
            }
            catch (Exception e)
            {
                Debug.LogError("Problem generating array: " + e.Message);
                yield break;
            }
            #endregion

            SearchResult[] searchResultArray = new SearchResult[resultsArray.Length];
            for (int i = 0; i < searchResultArray.Length; i++)
            {
                try
                {
                    searchResultArray[i] = new SearchResult(resultsArray[i]);
                }
                catch
                {
                    Debug.Log($"Error setting value at index {i}");
                }

            }
            yield return GetThumbnailTextures(searchResultArray);

            //Turn JSON into AWThing data format.
            searchDelegate(searchResultArray, resultsArray);

            //Unsubscribe search delegate
            searchDelegate -= delegateFunc;
        }
        #endregion

        private IEnumerator GetThumbnailTextures(SearchResult[] resultsList)
        {
            var requests = new List<UnityWebRequestAsyncOperation>(resultsList.Length);


            // Start all requests
            foreach (var result in resultsList)
            {
                string url = null;
                if (result.data.thumbnails.aw_thumbnail_transparent != null)
                {
                    url = result.data.thumbnails.aw_thumbnail_transparent;
                }
                else
                {
                    url = result.data.model.other.aw_thumbnail;
                }

                var www = UnityWebRequestTexture.GetTexture(url);
                // starts the request but doesn't wait for it for now
                requests.Add(www.SendWebRequest());

            }

            // Now wait for all requests parallel
            yield return new WaitUntil(() => AllRequestsDone(requests));



            // Now evaluate all results
            HandleAllRequestsWhenFinished(requests, resultsList);


            foreach (var request in requests)
            {
                try
                {
                    request.webRequest.Dispose();
                }
                catch(Exception e)
                {
                    Debug.LogWarning("Problem disposing of async web request");
                    Debug.LogException(e);
                }
  
            }

        }

        private bool AllRequestsDone(List<UnityWebRequestAsyncOperation> requests)
        {
            return requests.All(r => r.isDone);
        }
        private void HandleAllRequestsWhenFinished(List<UnityWebRequestAsyncOperation> requests, SearchResult[] searchResult)
        {
            for (var i = 0; i < requests.Count; i++)
            {
                var www = requests[i].webRequest;
                if (CheckWebRequest.IsSuccess(www))
                {
                    // Else if successful
                    Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                    searchResult[i].Thumbnail = myTexture;

                }
                else
                {
                    // If failed
                    searchResult[i].ResultHasThumbnail = false;
                }
            }
        }




        private string FixJson(string value)
        {
            value = "{\"Items\":" + value + "}";
            return value;
        }

        #region Creation

        /// <summary>
        /// Creates <see cref="AnythingQueueObject"/> from parameters and adds it to the <c>anythingQueue</c>.
        /// </summary>
        private void AddToQueue(string objName, bool autoLayout, bool hasBehaviour, bool hasCollider, bool positionGlobally, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, Transform parentTrans, GameObject objRef)
        {
            if (anythingQueue == null)
            {
                Init();
            }

            AnythingQueueObject aQObject = new AnythingQueueObject(objName, autoLayout, hasBehaviour, hasCollider, positionGlobally, objectPos, objectRot, objectScale, parentTrans, objRef);
            anythingQueue.Enqueue(aQObject);



            if (makeObjectRoutineRunning == false)
            {
                //If MakeObjectProcess is not currently running make it
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    if (makeObjectEditorRoutine != null)
                        EditorCoroutineUtility.StopCoroutine(makeObjectEditorRoutine);
                    makeObjectEditorRoutine = EditorCoroutineUtility.StartCoroutine(MakeObjectProcess(), this);
#endif
                }
                else
                {

                    if (makeObjectRoutine != null)
                        StopCoroutine(makeObjectRoutine);
                    makeObjectRoutine = StartCoroutine(MakeObjectProcess());
                }

            }
        }

        private IEnumerator MakeObjectProcess()
        {
            makeObjectRoutineRunning = true;
            AnythingSetup.Instance.ShowLoading(true);
            while (anythingQueue.Count > 0)
            {

                AnythingQueueObject objectParameters = anythingQueue.Dequeue() as AnythingQueueObject;
                string nameLower = objectParameters.objName.ToLower();
                GameObject thingGameObject = null;
                AWObj thingAWObj = null;


                if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Dequed \"{objectParameters.objName}\" creation request. Attempting make process.");

                GameObject clonableObject = GetClonableObjectFromDictionary(objectParameters.objName);


                if (clonableObject == null)
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Clonable object is null for {objectParameters.objName}");
                    if (Application.isEditor && !Application.isPlaying)
                    {
#if UNITY_EDITOR
                        EditorCoroutineUtility.StartCoroutine(MakeNewObject(objectParameters, thingAWObj, thingGameObject), this);
#endif
                    }
                    else
                    {

                        StartCoroutine(MakeNewObject(objectParameters, thingAWObj, thingGameObject));
                    }
                }
                else
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Clonable object is present for {objectParameters.objName}");
                    if (Application.isEditor && !Application.isPlaying)
                    {
#if UNITY_EDITOR
                        EditorCoroutineUtility.StartCoroutine(CloneObject(objectParameters, thingAWObj, thingGameObject, clonableObject), this);
#endif
                    }
                    else
                    {
                        StartCoroutine(CloneObject(objectParameters, thingAWObj, thingGameObject, clonableObject));
                    }

                }

                totalObjects++;


                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(0.01f);
#endif
                }
                else
                {
                    yield return new WaitForEndOfFrame();
                }
            }
            AnythingSetup.Instance.ShowLoading(false);
            makeObjectRoutineRunning = false;
        }





        private IEnumerator MakeNewObject(AnythingQueueObject objectParameters, AWObj thingAWObj, GameObject thingGameObject)
        {
            if (objectParameters.gameObjectReference != null)
            {
                thingGameObject = objectParameters.gameObjectReference;
                thingAWObj = thingGameObject.GetComponent<AWObj>();
            }
            else
            {
                thingGameObject = new GameObject("Loading Object");
                thingAWObj = thingGameObject.AddComponent<AWObj>();
            }
            thingGameObject.name = objectParameters.objName;
            thingAWObj.MakeAWObj(objectParameters.objName, objectParameters.hasBehaviour, objectParameters.hasCollider);
            float startupTime;
            float timeout = 40f;
#if UNITY_EDITOR
            startupTime = (float)EditorApplication.timeSinceStartup;
#else
                startupTime = Time.realtimeSinceStartup;
#endif
            while (!thingAWObj.ObjMade && !thingAWObj.objHasError)
            {
                float timeHanging = 0;
#if UNITY_EDITOR
                timeHanging = (float)EditorApplication.timeSinceStartup - startupTime;
#else
                    timeHanging = Time.realtimeSinceStartup - startupTime;
#endif
                if (timeHanging > timeout)
                {
                    if (thingAWObj != null)
                    {
                        thingAWObj.StopAllCoroutines();
                        AnythingSafeDestroy.SafeDestroy(thingGameObject);
                    }

                    Debug.LogError("Timeout waiting for AWObj to finish making in AWC!");
                    yield break;
                }


                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(0.01f);
#endif
                }
                else
                {
                    yield return new WaitForEndOfFrame();
                }
            }


            if (thingAWObj.objHasError)
            {
                if (thingAWObj.objError != null)
                {
                    Debug.LogWarning($"Cancelled make process early for \"{thingAWObj.objName}\" due to error. \n {thingAWObj.objError.code}: {thingAWObj.objError.message}");
                }
                else
                {
                    Debug.LogWarning("Error: Make object process canceled early.");
                }



                try
                {
                    AnythingSafeDestroy.SafeDestroy(thingGameObject);
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"Error destroying failed object {thingAWObj.objName}: {e.Message}");
                    Debug.LogException(e);
                }
                yield break;
            }
            category = CategoryMap.GetCategory(objectParameters.objName.ToLower());
            behaviour = BehaviourMap.GetBehaviour(objectParameters.objName.ToLower());


            if (GroupMap.ThingHasAGroup(behaviour, category))
            {
                if (objectParameters.parentTransform != null)
                {
                    CreateAWGroup(thingAWObj, objectParameters.objName, objectParameters.parentTransform);
                    AnythingSafeDestroy.SafeDestroy(thingGameObject);
                }
                else
                {
                    CreateAWGroup(thingAWObj, objectParameters.objName);
                }
            }
            else
            {
                if (objectParameters.autoLayout)
                {
                    objectParameters.objectPos = GetGridPosition(thingAWObj, thingAWObj.ObjectScale);
                }
                if (objectParameters.parentTransform != null)
                {
                    thingGameObject.transform.parent = objectParameters.parentTransform;
                }
                PositionAndScaleObject(thingGameObject, objectParameters);
                if (madeThings.TryGetValue(objectParameters.objName, out List<GameObject> val))
                {
                    madeThings[objectParameters.objName].Add(thingGameObject);

                }
                else
                {
                    List<GameObject> newList = new List<GameObject>();
                    newList.Add(thingGameObject);
                    madeThings.Add(objectParameters.objName, newList);
                }
            }
        }
        private IEnumerator CloneObject(AnythingQueueObject objectParameters, AWObj thingAWObj, GameObject thingGameObject, GameObject clonableObject)
        {
            if (objectParameters.gameObjectReference != null)
            {
                AnythingSafeDestroy.SafeDestroy(objectParameters.gameObjectReference);
            }
            AWObj AWObjToClone = clonableObject.GetComponent<AWObj>();

            if (AWObjToClone != null)
            {
                if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Found AWObj to clone for {objectParameters.objName}");
            }
            else
            {
                yield return MakeNewObject(objectParameters, thingAWObj, thingGameObject);
                yield break;
            }
            thingGameObject = Instantiate(clonableObject);
            thingGameObject.name = clonableObject.name;
            AWObj clonedAWObj = thingGameObject.GetComponent<AWObj>();
            if (clonedAWObj != null)
            {
                thingAWObj = clonedAWObj;
            }
            if (objectParameters.parentTransform != null)
            {
                thingGameObject.transform.parent = objectParameters.parentTransform;
            }

            if (objectParameters.autoLayout == true)
            {
                //Debug.Log("autolayoutting");
                objectParameters.objectPos = GetGridPosition(AWObjToClone, AWObjToClone.ObjectScale);
                if (objectParameters.objectPos != null)
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Auto layouting to {objectParameters.objectPos}");
                    try
                    {
                        thingGameObject.transform.position = objectParameters.objectPos;
                    }
                    catch
                    {
                        Debug.Log($"Error setting position for {thingGameObject.name}");
                    }


                }
                else
                {
                    if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Auto layout could not be found");
                }

            }
            else
            {
                PositionAndScaleObject(thingGameObject, objectParameters);
            }
            thingGameObject.transform.localScale = objectParameters.objectScale;
            FlockMember currFlockMember = thingGameObject.GetComponentInChildren<FlockMember>();
            if (currFlockMember != null)
            {
                if (groupController != null)
                {
                    AddCloneToGroup(currFlockMember.gameObject);
                }
                else
                {
                    if (objectParameters.parentTransform != null)
                    {
                        CreateAWGroup(thingAWObj, objectParameters.objName, objectParameters.parentTransform);
                        AnythingSafeDestroy.SafeDestroy(thingGameObject);
                    }
                    else
                    {
                        CreateAWGroup(thingAWObj, objectParameters.objName);
                    }
                }

                AddToMadeObjects(objectParameters.objName, currFlockMember.gameObject);
            }
            else
            {
                AddToMadeObjects(objectParameters.objName, thingGameObject);
            }

            if (thingAWObj != null)
            {
                thingAWObj.ObjMade = true;
            }

            yield return new WaitForSeconds(0.1f);
            yield return null;
        }

        public void ValidateMadeObjectLists()
        {
            List<string> keysToDelete = new List<string>();

            foreach (KeyValuePair<string, List<GameObject>> kvp in madeThings)
            {
                List<GameObject> toDelete = new List<GameObject>();
                foreach (GameObject obj in kvp.Value)
                {
                    if (obj == null)
                    {
                        toDelete.Add(obj);
                    }
                }
                foreach (GameObject deleteObj in toDelete)
                {
                    kvp.Value.Remove(deleteObj);
                }
                if (kvp.Value.Count == 0)
                {
                    keysToDelete.Add(kvp.Key);
                }
            }
        }

        private GameObject CheckMadeObjects(string key)
        {
            ValidateMadeObjectLists();
            if (madeThings.TryGetValue(key, out List<GameObject> val))
            {
                foreach (GameObject obj in val)
                {
                    if (obj != null)
                    {
                        return obj;
                    }
                }
            }
            return null;
        }
        private GameObject GetClonableObjectFromDictionary(string key)
        {
            ValidateMadeObjectLists();
            if (madeThings.TryGetValue(key, out List<GameObject> val))
            {
                foreach (GameObject obj in val)
                {
                    if (obj != null && obj.GetComponent<AWObj>())
                    {
                        return obj;
                    }
                }
            }
            return null;
        }

        private void PositionAndScaleObject(GameObject gameObject, AnythingQueueObject queueObject)
        {

            if (queueObject.positionGlobally == true)
            {
                gameObject.transform.position = queueObject.objectPos;
                gameObject.transform.rotation = queueObject.objectRot;

            }
            else
            {

                gameObject.transform.parent = queueObject.parentTransform;
                gameObject.transform.localPosition = queueObject.objectPos;
                gameObject.transform.localRotation = queueObject.objectRot;
            }
            gameObject.transform.localScale = queueObject.objectScale;
        }
        private void AddToMadeObjects(string key, GameObject obj)
        {

            if (madeThings.TryGetValue(key, out List<GameObject> val))
            {
                madeThings[key].Add(obj);
            }
            else
            {
                List<GameObject> objs = new List<GameObject>();
                objs.Add(obj);
                madeThings.Add(key, objs);
            }
        }

        /// <summary>
        /// Search scene ledger for object of matching name.
        /// </summary>
        /// <param name="name">String to find matching object name to.</param>
        /// <returns>Matching <see cref="ThingGroup"/> object holding instances of the specific game object.</returns>
        private ThingGroup SearchSceneLedger(string name)
        {
            foreach (ThingGroup group in SceneLedger.ThingGroups.ToArray())
            {
                if (group.objectName == name)
                {
                    return group;
                }
            }

            return SceneLedger.NewThingGroup(name);
        }

        /// <summary>
        /// Create flock of AW objects, with the specified number of members (1 by default).
        /// </summary>
        private void CreateAWGroup(AWObj thing, string objName, Transform parent = null, int memberNum = 1)
        {
            GameObject groupObj = GroupMap.GroupPrefab(behaviour);
            if (parent == null)
            {
                groupControllerObject = Instantiate(groupObj) as GameObject;
            }
            else
            {
                groupControllerObject = Instantiate(groupObj, parent);
            }
            groupController = groupControllerObject.GetComponent<FlockManager>();

            FlockMember currFlockMember = thing.GetComponentInChildren<FlockMember>(true);
            currFlockMember.enabled = true;
            currFlockMember.FlockManager = groupController;
            groupController.flockPrefabs = new GameObject[memberNum];

            // add as many members as needed, having the first original object and copies of the first one as the next members (to exist independently)
            for (int i = 0; i < memberNum; i++)
            {
                groupController.flockPrefabs[i] = i == 0 ? currFlockMember.gameObject : Instantiate(currFlockMember.gameObject);
                groupController.flockPrefabs[i].name = currFlockMember.gameObject.name;
                AddCloneToGroup(groupController.flockPrefabs[i]);
            }

            // we have already added our original object to group, so can safely destroy AWObj gameobject now
            if (parent == null)
            {
                AnythingSafeDestroy.SafeDestroy(thing.gameObject);
            }

            AddToMadeObjects(objName, currFlockMember.gameObject);
        }


        private void AddCloneToGroup(GameObject groupClone)
        {

            FlockMember fishChild = groupClone.GetComponentInChildren<FlockMember>();

            // TODO: careful, this will pick up any Flock
            if (groupController == null)
            {
                if (FindObjectOfType<FlockManager>())
                {
                    groupController = FindObjectOfType<FlockManager>();
                }
                else
                {
                    Debug.LogError("Group controller not instantiated for " + groupClone.name);
                    return;
                }
            }
            groupController.AddMember(fishChild);
        }

        public GameObject QueryMadeObjects(string query)
        {
            string lowerQuery = query.ToLower();
            if (madeThings == null)
            {
                if (AnythingSettings.Instance.showDebugMessages) Debug.Log("No made things");
                return null;
            }

            if (madeThings.ContainsKey(lowerQuery))
            {
                if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Checking made items for ");
                List<GameObject> matchingObjects = madeThings[lowerQuery];
                if (matchingObjects.Count > 0)
                {
                    return matchingObjects[0];
                }
            }
            return null;
        }

        #endregion

        #region Grid Layout
        protected Vector3 GetGridPosition(AWObj gridObj, float gridScale)
        {
            float yOffset = gridObj.BoundsYOffset;
            try
            {
                Vector3 gridPos = LayoutGrid.GetNextAvailablePos(gridScale);
                gridPos.y = yOffset;
                return gridPos;
            }
            catch
            {
                LayoutGrid = new GridUtility(10,10);
                Vector3 gridPos = LayoutGrid.GetNextAvailablePos(gridScale);
                gridPos.y = yOffset;
                return gridPos;
            }
        }
        public void ResetAutoLayout(bool resetPositions = true)
        {
            layoutGrid = null;
            //It through each group in the scene ledger and place each item in group on the grid.
            foreach (ThingGroup group in SceneLedger.ThingGroups.ToArray())
            {
                foreach (var awCreature in group.awInstances)
                {
                    if (awCreature != null)
                    {
                        if (resetPositions)
                        {
                            awCreature.awThing.transform.position = GetGridPosition(awCreature, awCreature.ObjectScale);
                        }
                        else
                        {
                            GetGridPosition(awCreature, awCreature.ObjectScale);
                        }

                    }
                }
            }
        }
        #endregion
        public string GetDisplayName(string _name)
        {
            if (_name != null)
            {
                string disp = Regex.Replace(_name, @"\d", "");
                char[] dispArr = disp.ToCharArray();
                dispArr[0] = char.ToUpper(dispArr[0]);
                return new string(dispArr);
            }
            else
            {
                return null;
            }
        }
    }


}