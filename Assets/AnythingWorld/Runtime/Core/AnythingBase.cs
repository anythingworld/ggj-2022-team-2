﻿using AnythingWorld.Animation;
using AnythingWorld.Habitat;
using AnythingWorld.Utilities;
using System;
using UnityEngine;

namespace AnythingWorld
{
    public class AnythingBase : MonoBehaviour
    {
        #region Fields
        protected string category;
        protected int totalObjects;
        private static string setupPrefabDir = "Prefabs/SetupPrefabs/AnythingSetup";
        public static AnythingSettings Settings
        {
            get
            {
                if (AnythingSettings.Instance == null)
                {
                    AnythingSettings.CreateInstance<AnythingSettings>();
                }
                return AnythingSettings.Instance;
            }
        }
        #endregion

        #region Protected Methods

        protected void ResetEverything()
        {
            AWObj[] sceneAWObjs = FindObjectsOfType<AWObj>();
            foreach (AWObj awObj in sceneAWObjs)
            {
                AnythingSafeDestroy.SafeDestroy(awObj.gameObject);
            }
            AWHabitat[] sceneAWHabitats = FindObjectsOfType<AWHabitat>();
            foreach (AWHabitat awHab in sceneAWHabitats)
            {
                AnythingSafeDestroy.SafeDestroy(awHab.gameObject);
            }
            // remove groups too
            FlockManager[] sceneFlocks = FindObjectsOfType<FlockManager>();
            foreach (FlockManager flockObj in sceneFlocks)
            {
                AnythingSafeDestroy.SafeDestroy(flockObj.gameObject);
            }
        }
        #endregion


        public delegate void OpenAPIGenWindowDelegate();
        public static OpenAPIGenWindowDelegate openAPIWindowDelegate;

        public static bool CheckAWSetup()
        {
            if (FindObjectOfType<AnythingSetup>())
            {
                return true;
            }
            else
            {

                try
                {
                    GameObject setup = Instantiate(Resources.Load(setupPrefabDir)) as GameObject;
                    setup.name = "AnythingSetup";
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError($"Error instantiating setup prefab from {setupPrefabDir}: {e.Message}");
                    return false;
                }

            }


        }
    }
}

