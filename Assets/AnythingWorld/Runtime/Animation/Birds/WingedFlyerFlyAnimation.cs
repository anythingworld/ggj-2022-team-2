﻿using System.Collections.Generic;
using UnityEngine;


namespace AnythingWorld.Animation
{
    /// <summary>
    /// Animation controller for the WingedFlyer fly animation.
    /// </summary>
    public class WingedFlyerFlyAnimation : WalkAnimation
    {
        #region Fields
        // Animation settings
        public float rightWingMovementSpeed = 4;
        public Vector3 rightWingMovementRotateTo = new Vector3(10, -20, 0);
        public float leftWingMovementSpeed = 4;
        public Vector3 leftWingMovementRotateTo = new Vector3(10, -20, 0);

        // Autos
        public string Descriptor { get; set; } = "Fly";

        /// ParameterController object used for monitoring parameters' changes
        ParameterController paramControl;
        #endregion

        #region Unity Callbacks
        void Start()
        {
            Initialization("winged_flyer__fly", "fly");

            // Initialize list of parameters used in this animation script
            parametersList = new List<Parameter>()
        {
            new Parameter("wing_right", "rightWingMovementSpeed","AnythingWingRotationAnimationComponent", "RotationSpeed",rightWingMovementSpeed),
            new Parameter("wing_right", "rightWingMovementRotateTo","AnythingWingRotationAnimationComponent","RotateTo",0,rightWingMovementRotateTo),
            new Parameter("wing_left", "leftWingMovementSpeed","AnythingWingRotationAnimationComponent","RotationSpeed",leftWingMovementSpeed),
            new Parameter("wing_left", "leftWingMovementRotateTo","AnythingWingRotationAnimationComponent","RotateTo",0,leftWingMovementRotateTo)
        };

            // Initialize paramControl and _prefabToScript variables
            paramControl = new ParameterController(parametersList);



            // TODO: careful! reliant on parent object script
            controllingAWObj = transform.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
                if (controllingAWObj == null)
                {
                    Debug.LogError($"No AWObj found for {gameObject.name}");
                    // TODO: remove this workaround - birds have no controlling awobj - GM
                    AddAnimationScripts();
                    ActivateShader();
                    return;
                }
            }

            StartCoroutine(WaitForAWObjCompletion());

        }
        void Update()
        {
            if (scriptSetupComplete)
                UpdateParameters();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for updating the speed of feet movement in the animation
        /// </summary>
        /// <param name="speed"></param>
        public override void SetMovementSpeed(float speed)
        {
        }
        public override void UpdateMovementSizeScale(float scale)
        {
            throw new System.NotImplementedException();
        }
        public override void ActivateShader()
        {
        }
        public override void DeactivateShader()
        {
        }
        #endregion

        #region Private Methods

        #endregion

        #region Protected Methods
        /// <summary>
        /// Method used for updating all parameters for the animation
        /// </summary>
        protected override void UpdateParameters()
        {
            // Check which parameters were modified
            List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float, Vector3)>() {
            ("rightWingMovementSpeed",rightWingMovementSpeed, new Vector3()),
            ("rightWingMovementRotateTo",0,rightWingMovementRotateTo),
            ("leftWingMovementSpeed",leftWingMovementSpeed,new Vector3()),
            ("leftWingMovementRotateTo",0,leftWingMovementRotateTo)
            });

            // Update parmeters value in the proper script
            foreach (var param in modifiedParameters)
            {
                prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
            }
        }
        #endregion

    }
}

