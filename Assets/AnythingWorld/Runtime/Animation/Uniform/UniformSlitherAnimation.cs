﻿using System.Collections.Generic;
using UnityEngine;
using AnythingWorld;

namespace AnythingWorld.Animation
{
    /// <summary>
    /// Animation controller for the slither animation for "uniform" type prefabs.
    /// </summary>
    public class UniformSlitherAnimation : AnythingAnimationController
    {
        #region Fields
        private float WobbleDistance = 15;
        private float WobbleSpeed = 5;
        /// ParameterController object used for monitoring parameters' changes
        ParameterController paramController;
        #endregion

        #region Unity Callbacks
        void Start()
        {
            Initialization("uniform__slither", "Move");

            // Initialize list of parameters used in this behaviour script
            parametersList = new List<Parameter>();

            // Initialize paramControl variable
            paramController = new ParameterController(parametersList);



            // TODO: careful! reliant on parent object script
            controllingAWObj = transform.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
                if (controllingAWObj == null)
                {
                    Debug.LogError($"No AWObj found for {gameObject.name}");
                    return;
                }
            }

            StartCoroutine(WaitForAWObjCompletion());

        }
        void Update()
        {
            if (scriptSetupComplete)
                UpdateParameters();
        }
        #endregion

        #region Public Methods
        public override void ActivateShader()
        {
            var shaders = gameObject.GetComponentsInChildren<Renderer>();
            foreach (var shader in shaders)
            {
                shader.sharedMaterial.SetFloat("WobbleDistance", WobbleDistance);
                shader.sharedMaterial.SetFloat("WobbleSpeed", WobbleSpeed);
            }
        }

        public override void DeactivateShader()
        {
            var shaders = gameObject.GetComponentsInChildren<Renderer>();
            foreach (var shader in shaders)
            {
                shader.sharedMaterial.SetFloat("WobbleDistance", 0);
                shader.sharedMaterial.SetFloat("WobbleSpeed", 0);
            }
        }

        public override void SetMovementSpeed(float speed)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateMovementSizeScale(float scale)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Method used for updating all parameters for the behaviour
        /// </summary>
        protected override void UpdateParameters()
        {
            // Check which parameters were modified
            List<Parameter> modifiedParameters = paramController.CheckParameters(new List<(string, float)>());

            // Update parmeters value in the proper script
            foreach (var param in modifiedParameters)
            {
                prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
            }
        }
        #endregion
    }

}
