﻿using AnythingWorld;
using AnythingWorld.Animation;
using AnythingWorld.Behaviours;
using UnityEngine;
public class HoppingMovement : AWBehaviour
{
    #region Fields
    public float movementSpeed = 6f;
    public float turnSpeed = 5f;
    public float jumpAmount = 10f;
    public float jumpTime = 1f;
    private Rigidbody rBody;
    private float forceMultiplier = 1000f;
    private float startY;
    private Quaternion targetRotation;
    private Vector3 jumpForce;

    [SerializeField]
    [HideInInspector]
    private bool inputsMultiplied = false;
    private Vector3 targetPosition;
    private GroundDetect groundDetect;
    private bool initialized = false;
    private bool initStarted = false;
    protected override string[] targetAnimationType { get; set; } = { "hop", "default" };
    public bool InitStarted { get => initStarted; set => initStarted = value; }

    //private bool _isJumping = true;
    #endregion
    #region Unity Callbacks

    private void Init()
    {
        InitStarted = true;


        try
        {
            AWObjController = gameObject.GetComponentInParent<AWObj>();
        }
        catch
        {
            Debug.LogError("Error finding AWObj from behaviour");
            InitStarted = false;
            return;
        }


        try
        {
            if (!inputsMultiplied)
            {
                MultiplyInputs();
            }
            jumpForce = Vector3.zero;
            rBody = AWThingTransform.GetComponent<Rigidbody>();
            startY = AWThingTransform.localPosition.y;
            groundDetect = AWThingTransform.gameObject.AddComponent<GroundDetect>();
            UpdateTarget();
            groundDetect.hitsGroundDelegate = AddJumpForce;
            rBody.AddForce(AWThingTransform.forward);
            initialized = true;
        }
        catch
        {
            Debug.LogError("Error initializing HoppingMovement");
            InitStarted = false;
            return;
        }

        InitStarted = false;
    }
    public void FixedUpdate()
    {
        if (initialized)
        {
            Vector3 lookRotation = targetPosition;
            lookRotation.y = AWThingTransform.position.y;

            Quaternion targRot = Quaternion.Lerp(AWThingTransform.rotation, Quaternion.LookRotation(lookRotation - AWThingTransform.position), turnSpeed * Time.deltaTime);

            targetRotation = targRot;

            AWThingTransform.rotation = targetRotation;

            float targSqrMag = Vector3.SqrMagnitude(AWThingTransform.position - targetPosition);

            if (targSqrMag < 50f)
            {
                UpdateTarget();
            }
            Transform BodyTransform = AWThingTransform.Find("body");
            if (BodyTransform)
            {
                BodyTransform.position = AWThingTransform.position;
                BodyTransform.rotation = AWThingTransform.rotation;
            }

            //Debug.Log("Forward" + AWThingTransform.forward);
            rBody.velocity = new Vector3(0f, rBody.velocity.y, 0f) + (AWThingTransform.forward * movementSpeed);

            //Debug.Log("Velocity:"+rBody.velocity);


        }

    }
    public void AddJumpForce()
    {
        Vector3 hopperForce = Vector3.zero;
        GetJumpForce();

        if (jumpForce != Vector3.zero)
        {
            hopperForce += jumpForce;

            jumpForce = Vector3.zero;
        }

        rBody.AddForce(hopperForce, ForceMode.Force);





    }
    public void Start()
    {
        Init();
    }


    #endregion

    #region Private Methods

    private void MultiplyInputs()
    {
        //MovementSpeed *= delaMultiplier;
        jumpAmount *= forceMultiplier / 10;
        inputsMultiplied = true;
    }
    private void GetJumpForce()
    {
        jumpForce = Vector3.up * jumpAmount;
    }
    private void UpdateTarget()
    {
        targetPosition = new Vector3(Random.Range(-50f, 50f), AWThingTransform.position.y, Random.Range(-50f, 50f));
    }

    #endregion
    #region Public Methods
    public override void SetDefaultParametersValues()
    {
        string prefabType = gameObject.GetComponentInParent<AWObj>().GetObjCatBehaviour();
        string behaviour = "HoppingMovement";
        PrefabAnimationsDictionary settings = ScriptableObject.CreateInstance<PrefabAnimationsDictionary>();
        movementSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "MovementSpeed");
        turnSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "TurnSpeed");
        jumpAmount = settings.GetDefaultParameterValue(prefabType, behaviour, "JumpAmount");
        jumpTime = settings.GetDefaultParameterValue(prefabType, behaviour, "JumpTime");

        
    }
    #endregion


}
