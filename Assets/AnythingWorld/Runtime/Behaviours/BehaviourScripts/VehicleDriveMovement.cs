﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnythingWorld;
using AnythingWorld.Utilities;
using AnythingWorld.Animation;
using AnythingWorld.Behaviours;
public class VehicleDriveMovement : AWBehaviour
{
    #region Fields
    private new string parentPrefabType;
    public float enginePower;
    public float rotationSpeed;
    public float power;
    public float brake;
    public float steer;
    public float maxSteer;
    public Vector3 VehicleCenterOfMass;

    [SerializeField]
    public List<Transform> wheels;
    public List<Transform> wheelsVisual;
    [SerializeField]
    public List<WheelCollider> _wheelColliders;
    [SerializeField]
    private List<WheelCollider> _steerWheelColliders;
    [SerializeField]
    private List<WheelCollider> _driveWheelColliders;

    private Rigidbody _rBody;
    private Vector3 targetPosition;
    private AWObj controllingAWObj;
    [SerializeField]
    private bool wheelsSet = false;
    public bool _readyToGo = false;
    protected override string[] targetAnimationType { get; set; } = { "drive", "default" };
    #endregion

    #region Unity Callbacks
    override public void Awake()
    {
        _readyToGo = false;
        base.Awake();
    }
    public void Start()
    {
        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        AWObjController = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            Debug.LogError($"No AWObj found for {gameObject.name}");
            return;
        }

        if (!wheelsSet)
        {
            StartCoroutine(WaitForAWObjCompletion());
            wheelsSet = true;
        }
        else
        {
            _readyToGo = true;
        }
        
    }
    public void LateUpdate()
    {
        if (!_readyToGo)
        {
            CollectWheels();
        }

        for (int i = 0; i < wheels.Count; i++)
        {
            Transform wheel = wheels[i];
            RotateWheelVisuals(wheel.GetComponent<WheelCollider>(), i);
            //ApplyLocalPositionToVisuals(wheel.GetComponent<WheelCollider>(), i);
        }
    }
    public void FixedUpdate()
    {
        if (!baseInitialised)
        {
            InitializeBehaviour();
            return;
        }


        Vector3 relativePos = AWThing.transform.position - targetPosition;
        Quaternion targetRotation = Quaternion.LookRotation(relativePos);

        float transY = AWThingTransform.eulerAngles.y;
        float DeltaAngle = Mathf.DeltaAngle(transY, targetRotation.eulerAngles.y) * -1;

        float accel = 0.6f;


        Vector3 toTarget = relativePos.normalized;

        // reverse if target behind     
        if (Vector3.Dot(toTarget, AWThingTransform.forward) < 0)
        {
            accel *= -1;
        }

        power = (accel * enginePower * Time.deltaTime * 250.0f) * -1;
        power = Mathf.Clamp(power, -5000, 5000);
        float targSteer = Mathf.Clamp(DeltaAngle, -maxSteer, maxSteer);

        if (steer != targSteer)
        {
            steer = Mathf.Lerp(steer, targSteer, 0.01f);
        }

        if (brake > 0f)
        {
            ChangeWheels(steer, brake, 0f);
        }
        else if (accel == 0f)
        {
            ChangeWheels(steer, 1f, 0f);
        }
        else
        {
            ChangeWheels(steer, 0f, power);
        }
    }
    #endregion

    #region Private Methods
    private IEnumerator WaitForAWObjCompletion()
    {
        while (!controllingAWObj.ObjMade)
            yield return new WaitForEndOfFrame();
        StartCoroutine(CollectWheels());
    }
    private IEnumerator CollectWheels()
    {
        StartCoroutine(RandomiseTarget());
        _rBody = AWThingTransform.GetComponent<Rigidbody>(); // TODO: get this working in editor
        _rBody.centerOfMass = VehicleCenterOfMass;

        wheels = new List<Transform>();
        wheelsVisual = new List<Transform>();

        _wheelColliders = new List<WheelCollider>();
        _steerWheelColliders = new List<WheelCollider>();
        _driveWheelColliders = new List<WheelCollider>();

        foreach (Transform t in AWThingTransform.GetComponentsInChildren<Transform>())
        {
            string tName = t.name.ToLower();
            if (t != AWThingTransform)
            {
                if (tName.IndexOf("wheel_collider") != -1)
                {
                    WheelCollider wCollider = t.GetComponent<WheelCollider>();
                    if (t.GetComponent<WheelCollider>() == null)
                    {
                        wCollider = t.gameObject.AddComponent<WheelCollider>();

                    }
                    wheels.Add(t);
                    _wheelColliders.Add(wCollider);
                }
                else if (tName.IndexOf("wheel") != -1)
                {

                    if (t.childCount > 0)
                    {
                        Transform visualWheel = t.GetChild(0);

                        wheelsVisual.Add(visualWheel);
                    }
                }

                if (tName.IndexOf("steer") != -1)
                {
                    _steerWheelColliders.Add(t.GetComponent<WheelCollider>());
                }
                if (tName.IndexOf("drive") != -1)
                {
                    _driveWheelColliders.Add(t.GetComponent<WheelCollider>());
                }
            }
        }


        for (int i = 0; i < wheels.Count; i++)
        {
            wheels[i].position = wheelsVisual[i].parent.GetComponent<CenterMeshPivot>().PivotCenter;

            var t = wheels[i].GetComponent<WheelCollider>().radius = wheelsVisual[i].parent.GetComponent<CenterMeshPivot>().Radius;
        }

        _readyToGo = true;
        yield return new WaitForEndOfFrame();


    }
    private void ChangeWheels(float steerAmount, float brakeAmount, float motorPower)
    {
        // steer
        foreach (WheelCollider sCollider in _steerWheelColliders)
        {
            sCollider.steerAngle = steerAmount;
        }
        // brake
        foreach (WheelCollider wCollider in _wheelColliders)
        {
            wCollider.brakeTorque = brakeAmount;
        }
        // motor torque
        foreach (WheelCollider dCollider in _driveWheelColliders)
        {
            dCollider.motorTorque = motorPower;
        }
    }
    private IEnumerator RandomiseTarget()
    {
        while (AWThingTransform != null)
        {
            targetPosition = new Vector3(Random.Range(-50f, 50f), AWThingTransform.position.y, Random.Range(-50f, 50f));
            yield return new WaitForSeconds(5);
        }
        yield return null;
    }
    #endregion

    #region Public Methods

    public void RotateWheelVisuals(WheelCollider wheelCollider, int wheelIndex)
    {
        //get loadedobject transform of wheel
        Transform visualWheel = wheelsVisual[wheelIndex];
        Transform wheelPivot = visualWheel.GetChild(1);
        //get world position and rotation of wheelCollider;

        wheelCollider.GetWorldPose(out Vector3 position, out Quaternion rotation);

        wheelPivot.position = position;
        wheelPivot.transform.rotation = rotation;

    }
    public void ApplyLocalPositionToVisuals(WheelCollider collider, int wheelIndex)
    {
        Transform visualWheel = wheelsVisual[wheelIndex];
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
        var wheelMeshTransform = visualWheel.GetComponentInChildren<MeshRenderer>().transform;
        visualWheel.transform.position = position;

        //visualWheel.transform.rotation = rotation;
    }
    public override void RemoveAWAnimator()
    {
        AnythingSafeDestroy.SafeDestroy(animator);
    }
    public override void SetDefaultParametersValues()
    {
        string prefabType = gameObject.GetComponentInParent<AWObj>().GetObjCatBehaviour();
        string behaviour = "VehicleDriveMovement";

        PrefabAnimationsDictionary settings = ScriptableObject.CreateInstance<PrefabAnimationsDictionary>();

        enginePower = settings.GetDefaultParameterValue(prefabType, behaviour, "enginePower");
        rotationSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "rotationSpeed");
        power = settings.GetDefaultParameterValue(prefabType, behaviour, "power");
        brake = settings.GetDefaultParameterValue(prefabType, behaviour, "brake");
        steer = settings.GetDefaultParameterValue(prefabType, behaviour, "steer");
        maxSteer = settings.GetDefaultParameterValue(prefabType, behaviour, "maxSteer");
        VehicleCenterOfMass = settings.GetDefaultParameterVector3Value(prefabType, behaviour, "VehicleCenterOfMass");
    }
    #endregion

}
