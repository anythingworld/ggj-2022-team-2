﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AnythingWorld.Animation
{
    public class FlockMember : MonoBehaviour
    {
        public enum MemberType
        {
            bird = 0,
            fish
        }

        public MemberType flockMemberType = MemberType.bird;
        public float minSpeed;
        public float maxSpeed;
        private float speed;
        Vector3 averageHeading;
        Vector3 averagePosition;

        float neighborDistance = 3.0f;

        bool turning = false;
        public FlockManager FlockManager
        {
            get
            {
                return flockManager;
            }
            set
            {
                flockManager = value;
            }
        }
        private FlockManager flockManager;
        private Transform _rotationTransform;
        private bool _delayTurning;

        public void OnEnable()
        {
            speed = Random.Range(minSpeed, maxSpeed);

        }

        public void Update()
        {
            if (flockManager == null)
            {
                if (transform.parent.TryGetComponent<FlockManager>(out flockManager)) { }
                else
                {
                    return;
                }

            }



            ApplyTankBoundary();

            if (_rotationTransform == null)
            {
                if (flockMemberType == MemberType.fish && gameObject.GetComponentInChildren<MeshRenderer>() != null)
                {
                    _rotationTransform = gameObject.GetComponentInChildren<MeshRenderer>().transform;
                }
                else
                {
                    _rotationTransform = transform.GetChild(0);
                }
            }

            if (turning)
            {
                Vector3 direction = flockManager.transform.position - transform.position;
                Quaternion targetRotation = Quaternion.Slerp(_rotationTransform.rotation,
                    Quaternion.LookRotation(direction),
                    TurnSpeed() * Time.deltaTime);

                _rotationTransform.rotation = targetRotation;

                bool isAtRotation = isApproximate(targetRotation, transform.rotation, 0.1f);

                speed = Random.Range(minSpeed, maxSpeed);
            }
            else
            {
                if (Random.Range(0, 5) < 1)
                    ApplyRules();
            }

            if (_rotationTransform != null)
            {
                Vector3 translateAmnt = _rotationTransform.forward * Time.deltaTime * speed;
                transform.Translate(translateAmnt);
            }
        }

        public static bool isApproximate(Quaternion q1, Quaternion q2, float precision)
        {
            float prec = Mathf.Abs(Quaternion.Dot(q1, q2));
            float thresh = 1f - precision;
            return prec >= thresh;
        }

        private void ApplyTankBoundary()
        {
            if (!_delayTurning)
            {
                if (Vector3.Distance(transform.position, flockManager.transform.position) >= flockManager.flockSize)
                {
                    turning = true;
                }
                else
                {
                    turning = false;
                }
            }
        }

        private IEnumerator DelayTurning()
        {
            _delayTurning = true;
            yield return new WaitForSeconds(5f);
            _delayTurning = false;
        }

        private void ApplyRules()
        {

            if (flockManager == null)
                flockManager = FindObjectOfType<FlockManager>();

            if (flockManager == null)
                return;


            List<GameObject> gos = flockManager.GetAllMembers();

            if (gos == null)
                return;

            Vector3 vCenter = flockManager.transform.position;
            Vector3 vAvoid = Vector3.zero;
            float gSpeed = 0.1f;

            Vector3 goalPos = flockManager.goalPos;

            float dist;
            int groupSize = 0;

            foreach (GameObject go in gos)
            {
                if (go != null)
                {
                    if (go != this.gameObject)
                    {
                        dist = Vector3.Distance(go.transform.position, this.transform.position);
                        if (dist <= neighborDistance)
                        {
                            vCenter += go.transform.position;
                            groupSize++;

                            if (dist < 0.75f)
                            {
                                vAvoid = vAvoid + (this.transform.position - go.transform.position);
                            }

                            FlockMember anotherFish = go.GetComponent<FlockMember>();
                            gSpeed += anotherFish.speed;
                        }

                    }
                }
            }

            if (groupSize > 0)
            {
                vCenter = vCenter / groupSize + (goalPos - this.transform.position);
                speed = gSpeed / groupSize;

                Vector3 direction = (vCenter + vAvoid) - flockManager.transform.position;
                if (direction != Vector3.zero)
                {
                    _rotationTransform.rotation = Quaternion.Slerp(_rotationTransform.rotation,
                        Quaternion.LookRotation(direction),
                        TurnSpeed() * Time.deltaTime);
                }
            }

        }

        float TurnSpeed()
        {
            return Random.Range(minSpeed / 2f, maxSpeed / 2f);
        }
    }
}

