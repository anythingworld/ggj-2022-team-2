﻿using AnythingWorld.Animation;
using AnythingWorld.Utilities;
using UnityEngine;
using UnityEngine.Assertions;

namespace AnythingWorld.Behaviours
{
    /// <summary>
    /// Base class from which AWBehaviours are derived.
    /// </summary>
    public abstract class AWBehaviour : MonoBehaviour
    {
        #region Fields
        protected abstract string[] targetAnimationType { get; set; }

        public string parentPrefabType { get; set; }
        protected PrefabAnimationsDictionary settings;
        [SerializeField, HideInInspector]
        protected Component animatorComponent;
        [SerializeField, HideInInspector]
        protected AnythingAnimationController animator;
        [SerializeField, HideInInspector]
        public bool baseInitialised = false;
        [SerializeField, HideInInspector]
        public bool ReadyToGo { get; set; } = false;
        public GameObject AWThing
        {
            get
            {
                if (AWObjController == null)
                {
                    if (AWObjController == null) AWObjController = GetComponentInParent<AWObj>();

                    return AWObjController.awThing;
                }
                else
                {
                    return AWObjController.awThing;
                }
            }
        }


        [SerializeField]
        public AWObj AWObjController { get; set; } = null;


        public Transform AWThingTransform
        {
            get
            {
                if (AWThing == null)
                    return null;
                else
                    return AWThing.transform;
            }
        }
        // inherit behaviours from this class to make sure they do not run prematurely when created at runtime!
        #endregion

        #region Unity Callbacks
        public virtual void Reset()
        {
            if (!baseInitialised)
            {
                InitializeBehaviour();
            }
        }
        public virtual void OnEnable()
        {
            if (!baseInitialised)
            {
                InitializeBehaviour();
            }
        }
        public virtual void Awake()
        {
            if (!baseInitialised)
            {
                InitializeBehaviour();
            }
        }

        private void Start()
        {

        }

        #endregion

        #region Virtual Functions
        public virtual void SetDefaultParametersValues()
        {

        }
        public virtual void InitializeBehaviour()
        {
            if (ReadyToGo && !baseInitialised)
            {
                if (AWObjController == null) AWObjController = GetComponentInParent<AWObj>();

                if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"Initalizing behaviour for {AWObjController.objName}");
                AddAWAnimator();
                //Debug.Log("gets out of adding AW Animator");
                SetDefaultParametersValues();

                if (AWObjController != null)
                {
                    baseInitialised = true;
                    //Debug.Log("Base behaviour initialised");
                }
            }

        }

        /// <summary>
        /// Get the awThing object that behaviours act on.
        /// </summary>
        private void GetAWThing()
        {
            AWObjController = gameObject.GetComponentInParent<AWObj>();
            if (AWObjController == null)
            {
                Debug.Log("ParentAWObj not found in GetAWThing");
                return;
            }
        }



        public virtual void AddAWAnimator()
        {
            // special cases for flock objects, where AWObj is removed on creation
            if (AWObjController == null)
            {
                if(AnythingSettings.Instance.showDebugMessages) Debug.LogError("No parentAWObj found");
                return;
            }
            parentPrefabType = AWObjController.GetObjCatBehaviour();
            //Debug.Log(parentPrefabType);

            //Query required animator for our prefab type
            if (parentPrefabType != null)
            {
                if (targetAnimationType != null)
                {
                    settings = ScriptableObject.CreateInstance<AnythingWorld.Animation.PrefabAnimationsDictionary>();
                    if (settings != null)
                    {
                        string animatorScriptstring = null;
                        System.Type animatorScript = null;


                        foreach (string animationType in targetAnimationType)
                        {
                            animatorScriptstring = settings.GetAnimationScriptName(parentPrefabType, animationType);
                            if (animatorScriptstring != null)
                            {
                                animatorScriptstring = "AnythingWorld.Animation." + animatorScriptstring;
                                animatorScript = System.Type.GetType(animatorScriptstring);
                                break;
                            }
                        }

                        //Check if animator type exists
                        if (animatorScript != null)
                        {
                            animatorComponent = AWThing.AddComponent(animatorScript);
                            animator = AWThing.GetComponent<AnythingWorld.Animation.AnythingAnimationController>();
                            if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Animator " + animator + " added to " + AWObjController.objName);
                        }
                        else
                        {
                            if (AnythingSettings.DebugEnabled) Debug.LogWarning($"Animator script for target animation type not found");
                        }
                    }
                    else
                    {
                        Debug.LogWarning("Animation map settings could not be created.");
                        return;
                    }
                }
                else
                {
                    Debug.Log("No animation type provided");
                    return;
                }
            }
            else
            {
                Debug.Log("No parent prefab part found");
                return;
            }
            return;
        }
        public virtual void RemoveAWAnimator()
        {
            if (animator != null)
            {
                animator.DestroySubscripts();
                AnythingSafeDestroy.SafeDestroy(animator);
            }
            else
            {
                //Debug.Log($"Error: No animator found");
            }
        }
        public void OnDestroy()
        {
            RemoveAWAnimator();
        }

        #endregion
    }

}
