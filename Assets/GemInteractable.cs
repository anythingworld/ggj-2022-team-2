using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemInteractable : MonoBehaviour, IInteractable
{
    public bool disableGem = true;
    public PortalController pControl;
    public AudioClip activatedSound;
    public void Awake()
    {
        if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>().playOnAwake = false;
        if (disableGem)
        {
            transform.gameObject.SetActive(false);
        }

    }
    public bool CanBePickedUp()
    {
        return false;
    }

    public void Interact()
    {
        if (TryGetComponent<AudioSource>(out var audioSource))
        {
            audioSource.PlayOneShot(activatedSound);
        }
        transform.gameObject.SetActive(false);
        pControl.ActivatePortal();
    }

    public void Interact(GameObject interactingObject)
    {
        if(TryGetComponent<AudioSource>(out var audioSource))
        {
            audioSource.PlayOneShot(activatedSound);
        }
        transform.gameObject.SetActive(false);
        pControl.ActivatePortal();
    }

    public void PickUp()
    {  }

    public void PickUp(Transform newParent)
    {
    }

    public void PutDown()
    {
    }

    public void PutDown(Vector3 position)
    {
    }
}
